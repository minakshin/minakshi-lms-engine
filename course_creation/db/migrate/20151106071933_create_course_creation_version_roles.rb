class CreateCourseCreationVersionRoles < ActiveRecord::Migration
  def change
    create_table :course_creation_version_roles do |t|
      t.integer :version_id
      t.integer :role_id
      t.timestamps null: false
    end
    add_foreign_key :course_creation_version_roles, :course_creation_versions,
                    column: :version_id
    add_index :course_creation_version_roles, :version_id
    add_foreign_key :course_creation_version_roles, :course_creation_roles,
                    column: :role_id
    add_index :course_creation_version_roles, :role_id
  end
end
