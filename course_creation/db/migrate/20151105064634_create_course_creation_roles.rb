class CreateCourseCreationRoles < ActiveRecord::Migration
  def change
    create_table :course_creation_roles do |t|
      t.string :name
      t.timestamps null: false
    end

    %w(commissioner coach official
       parent player).each do |categoryname|
      CourseCreation::Role.create!(name: categoryname)
    end
  end
end
