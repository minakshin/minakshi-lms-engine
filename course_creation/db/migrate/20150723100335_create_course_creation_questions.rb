# create table for questions
class CreateCourseCreationQuestions < ActiveRecord::Migration
  def change
    create_table :course_creation_questions do |t|
      t.text :title
      t.text :test_reference
      t.text :additional_text
      t.integer :question_category_id
      t.timestamps null: false
    end
    add_foreign_key :course_creation_questions,
                    :course_creation_question_categories,
                    column: :question_category_id
    add_index :course_creation_questions, :question_category_id
  end
end
