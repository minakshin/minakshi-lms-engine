# Add is_assessment column to course sections
class AddColumnIsAssessmentToCourseCreationCourseSections < ActiveRecord::Migration
  def change
    add_column :course_creation_course_sections, :is_assessment,
               :boolean, default: false
  end
end
