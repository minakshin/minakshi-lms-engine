# Slide content table
class CreateCourseCreationSlideContents < ActiveRecord::Migration
  def change
    create_table :course_creation_slide_contents do |t|
      t.integer :slide_id
      t.text :content
      t.string :orientation
      t.timestamps null: false
    end
    add_foreign_key :course_creation_slide_contents,
                    :course_creation_slides,
                    column: :slide_id
    add_index :course_creation_slide_contents, :slide_id
  end
end
