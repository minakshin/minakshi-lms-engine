# Interactive slide
class CreateCourseCreationInteractiveSlides < ActiveRecord::Migration
  def change
    create_table :course_creation_interactive_slides do |t|
      t.string :name
      t.text :description
      t.integer :version_id
      t.integer :section_id
      t.timestamps null: false
    end
    add_foreign_key :course_creation_interactive_slides,
                    :course_creation_courses, column: :version_id
    add_foreign_key :course_creation_interactive_slides,
                    :course_creation_courses, column: :section_id
    add_index :course_creation_interactive_slides, :version_id
    add_index :course_creation_interactive_slides, :section_id
  end
end
