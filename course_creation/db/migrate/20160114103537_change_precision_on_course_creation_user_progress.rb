# Increase precision valuse of progress_percentage
class ChangePrecisionOnCourseCreationUserProgress < ActiveRecord::Migration
  def change
    change_column :course_creation_user_courses, :progress_percentage,
                  :decimal, precision: 5, scale: 2
  end
end
