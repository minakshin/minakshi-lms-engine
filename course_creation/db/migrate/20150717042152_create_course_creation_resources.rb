# Resources table
class CreateCourseCreationResources < ActiveRecord::Migration
  def change
    create_table :course_creation_resources do |t|
      t.string :title
      t.string :file
      t.string :type
      t.integer :course_section_id, null: false
      t.timestamps null: false
    end
    add_foreign_key :course_creation_resources,
                    :course_creation_course_sections,
                    column: :course_section_id
    add_index :course_creation_resources, :course_section_id
  end
end
