class RemoveNullConstraintFromResourcesCourseSectionId < ActiveRecord::Migration
  def change
    change_column_null(:course_creation_resources, :course_section_id, true)
  end
end
