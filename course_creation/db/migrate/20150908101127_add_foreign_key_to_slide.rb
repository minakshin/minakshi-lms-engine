class AddForeignKeyToSlide < ActiveRecord::Migration
  def change
    add_foreign_key :course_creation_slides,
                    :course_creation_presentations,
                    column: :presentation_id
    add_index :course_creation_slides, :presentation_id
  end
end
