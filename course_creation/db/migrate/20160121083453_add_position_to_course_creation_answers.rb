class AddPositionToCourseCreationAnswers < ActiveRecord::Migration
  def change
    add_column :course_creation_answers, :position, :integer
  end
end
