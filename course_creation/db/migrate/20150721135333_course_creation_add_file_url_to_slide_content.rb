# Add file url column
class CourseCreationAddFileUrlToSlideContent < ActiveRecord::Migration
  def change
    add_column :course_creation_slide_contents, :file_url, :string
  end
end
