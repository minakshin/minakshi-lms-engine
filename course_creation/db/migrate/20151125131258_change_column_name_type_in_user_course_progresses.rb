# Change colum name for user_course_progress table
class ChangeColumnNameTypeInUserCourseProgresses < ActiveRecord::Migration
  def change
    rename_column :course_creation_user_course_progresses, :type,
                  :content_type
  end
end
