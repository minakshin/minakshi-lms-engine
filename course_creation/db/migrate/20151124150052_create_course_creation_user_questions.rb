# User question tables
class CreateCourseCreationUserQuestions < ActiveRecord::Migration
  def change
    create_table :course_creation_user_questions do |t|
      t.integer :assessment_id
      t.integer :user_course_id
      t.integer :question_id
      t.integer :passing_status
      t.integer :attempt_no
      t.integer :question_type_id
      t.string :assessment_type
      t.timestamps null: false
    end
    add_foreign_key :course_creation_user_questions,
                    :course_creation_user_courses,
                    column: :user_course_id
    add_index :course_creation_user_questions, :user_course_id
    add_foreign_key :course_creation_user_questions,
                    :course_creation_assessments,
                    column: :assessment_id
    add_index :course_creation_user_questions, :assessment_id
    add_foreign_key :course_creation_user_questions,
                    :course_creation_questions,
                    column: :question_id
    add_index :course_creation_user_questions, :question_id
    add_foreign_key :course_creation_user_questions,
                    :course_creation_question_categories,
                    column: :question_type_id
    add_index :course_creation_user_questions, :question_type_id
  end
end
