# add slide_type on create of interactive slide
class AddColumnForInteractiveSlides < ActiveRecord::Migration
  def change
    add_column :course_creation_interactive_slides, :interactive_slide_type,
               :string
  end
end
