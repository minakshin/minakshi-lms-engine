# add null validation to course_creation_versions table
class AddVaidationsCourseCreationVersions < ActiveRecord::Migration
  def change
    change_column :course_creation_versions, :description, :text, null: false
    change_column :course_creation_versions, :expiry, :integer, null: false
    change_column :course_creation_versions, :price, :boolean, null: false
  end
end
