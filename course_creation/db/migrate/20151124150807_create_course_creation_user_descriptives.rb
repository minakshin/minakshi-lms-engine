# User descriptive table
class CreateCourseCreationUserDescriptives < ActiveRecord::Migration
  def change
    create_table :course_creation_user_descriptives do |t|
      t.text :answer
      t.integer :user_question_id
      t.integer :question_id
      t.integer :user_id
      t.boolean :is_correct_answer
      t.timestamps null: false
    end
    add_foreign_key :course_creation_user_descriptives,
                    :course_creation_questions,
                    column: :question_id
    add_index :course_creation_user_descriptives, :question_id
    add_foreign_key :course_creation_user_descriptives,
                    :course_creation_user_questions,
                    column: :user_question_id
    add_index :course_creation_user_descriptives, :user_question_id
  end
end
