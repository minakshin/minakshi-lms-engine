class AddContentToCourseCreationCourseSections < ActiveRecord::Migration
  def change
    add_column :course_creation_course_sections, :content, :boolean, default: false
  end
end
