# remove test_reference column
class RemoveColumnFromCourseCreationQuestions < ActiveRecord::Migration
  def change
    remove_column :course_creation_questions, :test_reference
  end
end
