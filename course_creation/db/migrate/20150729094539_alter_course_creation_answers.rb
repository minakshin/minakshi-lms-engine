# alter course creation answers
class AlterCourseCreationAnswers < ActiveRecord::Migration
  def change
    add_column :course_creation_answers, :correct_answer, :boolean,
               default: false
    add_column :course_creation_answers, :question_id, :integer
    add_foreign_key :course_creation_answers, :course_creation_questions,
                    column: :question_id
    add_index :course_creation_answers, :question_id
  end
end
