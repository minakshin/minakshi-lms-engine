# Interactive slide information
class CreateCourseCreationInteractiveSlidesInformations < ActiveRecord::Migration
  def change
    create_table :course_creation_interactive_slides_informations do |t|
      t.string :image
      t.string :title
      t.text :description
      t.integer :interactive_slide_id
      t.timestamps null: false
    end
    add_foreign_key :course_creation_interactive_slides_informations,
                    :course_creation_interactive_slides, column:
                    :interactive_slide_id
    add_index :course_creation_interactive_slides_informations,
              :interactive_slide_id, name: :interactive_slide_id
  end
end
