class AddCertificateIdToCourseCreationCourses < ActiveRecord::Migration
  def change
    add_column :course_creation_courses, :certificate_id, :integer
    add_foreign_key :course_creation_courses,
                    :course_creation_certificates,
                    column: :certificate_id
    add_index :course_creation_courses, :certificate_id
  end
end
