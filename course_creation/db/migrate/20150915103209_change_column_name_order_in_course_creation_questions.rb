# rename column order to questions_order in questions table.
class ChangeColumnNameOrderInCourseCreationQuestions < ActiveRecord::Migration
  def change
    rename_column :course_creation_questions, :order, :question_order
  end
end
