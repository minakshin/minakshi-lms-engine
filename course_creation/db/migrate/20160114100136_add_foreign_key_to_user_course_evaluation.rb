# AddForeignKeyToUserCourseEvaluation
class AddForeignKeyToUserCourseEvaluation < ActiveRecord::Migration
  def change
    add_foreign_key :course_creation_user_course_evaluations,
                    :course_creation_user_courses,
                    column: :user_course_id
    add_index :course_creation_user_course_evaluations, :user_course_id
  end
end
