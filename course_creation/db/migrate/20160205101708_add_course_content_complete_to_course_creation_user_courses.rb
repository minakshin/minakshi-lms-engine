# Add course content complete key to user course
class AddCourseContentCompleteToCourseCreationUserCourses < ActiveRecord::Migration
  def change
    add_column :course_creation_user_courses, :content_complete, :boolean,
               default: false
  end
end
