# add type column to slide contents
class AddTypeColumnToCourseCreationSlideContents < ActiveRecord::Migration
  def change
    add_column :course_creation_slide_contents, :type, :string
  end
end
