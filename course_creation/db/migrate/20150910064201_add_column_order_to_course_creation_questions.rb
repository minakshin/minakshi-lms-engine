# Add order column to questions
class AddColumnOrderToCourseCreationQuestions < ActiveRecord::Migration
  def change
    add_column :course_creation_questions, :order, :integer
  end
end
