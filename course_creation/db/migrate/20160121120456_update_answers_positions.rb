# Update Answers Positions
class UpdateAnswersPositions < ActiveRecord::Migration
  def change
    questions = CourseCreation::Question.includes(:answers)
    questions.each do |question|
      question.answers.order(:id).each_with_index do |answer, index|
        answer.set_list_position(index + 1)
      end
    end
  end
end
