# UserCourseEvaluations migraion
class CreateCourseCreationUserCourseEvaluations < ActiveRecord::Migration
  def change
    create_table :course_creation_user_course_evaluations do |t|
      t.integer :version_id
      t.integer :user_course_id
    end
  end
end
