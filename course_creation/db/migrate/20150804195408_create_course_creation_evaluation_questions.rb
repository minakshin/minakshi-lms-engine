class CreateCourseCreationEvaluationQuestions < ActiveRecord::Migration
  def change
    create_table :course_creation_evaluation_questions do |t|
      t.string :content
      t.boolean :active

      t.timestamps null: false
    end

    add_reference :course_creation_evaluation_questions, :version,
                  references: :course_creation_versions
  end
end
