class RemoveForeignKeyFromInteractiveSlides < ActiveRecord::Migration
  def change
    remove_foreign_key :course_creation_interactive_slides,
                    column: :course_section_id
    remove_foreign_key :course_creation_interactive_slides,
                    column: :version_id
    add_foreign_key :course_creation_interactive_slides, :course_creation_course_sections,
                    column: :course_section_id
    add_foreign_key :course_creation_interactive_slides, :course_creation_versions,
                    column: :version_id
  end
end
