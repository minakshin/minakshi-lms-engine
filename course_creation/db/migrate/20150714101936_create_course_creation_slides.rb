# slides table
class CreateCourseCreationSlides < ActiveRecord::Migration
  def change
    create_table :course_creation_slides do |t|
      t.string :title
      t.integer :presentation_id
      t.integer :number_of_columns, limit: 1
      t.timestamps null: false
    end
  end
end
