# AddIndexToUserCourseEvaluationQuestions
class AddIndexToUserCourseEvaluationQuestions < ActiveRecord::Migration
  def change
    add_index :course_creation_user_course_evaluation_questions,
              :user_course_evaluation_id, name: :evaluation_id
  end
end
