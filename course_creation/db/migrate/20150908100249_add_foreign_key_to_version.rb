class AddForeignKeyToVersion < ActiveRecord::Migration
  def change
    add_foreign_key :course_creation_versions,
                    :course_creation_categories,
                    column: :category_id
    add_foreign_key :course_creation_versions,
                    :course_creation_courses,
                    column: :course_id
    add_index :course_creation_versions, :category_id
    add_index :course_creation_versions, :course_id
  end
end
