# Slide background settings
class CreateCourseCreationSlideSettings < ActiveRecord::Migration
  def change
    create_table :course_creation_slide_settings do |t|
      t.string :background_color, default: '#ffffff'
      t.string :background_img
      t.string :transition
      t.integer :slide_id, null: false
      t.timestamps null: false
    end
    add_foreign_key :course_creation_slide_settings,
                    :course_creation_slides,
                    column: :slide_id
    add_index :course_creation_slide_settings, :slide_id
  end
end
