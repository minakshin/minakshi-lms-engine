class SetDefaultValueToVersions < ActiveRecord::Migration
  def change
    change_column_default :course_creation_versions, :version, 0
  end
end
