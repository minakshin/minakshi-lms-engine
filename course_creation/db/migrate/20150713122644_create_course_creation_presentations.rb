# Presentation table
class CreateCourseCreationPresentations < ActiveRecord::Migration
  def change
    create_table :course_creation_presentations do |t|
      t.string :title
      t.integer :course_section_id
      t.timestamps null: false
    end
  end
end
