class CreateCourseCreationCustomContents < ActiveRecord::Migration
  def change
    create_table :course_creation_custom_contents do |t|
      t.string :name
      t.text :description
      t.string :zip
      t.integer :content_id

      t.timestamps null: false
    end
  end
end
