# User objective question answers
class CreateCourseCreationUserObjectiveQuestionAnswers < ActiveRecord::Migration
  def change
    create_table :course_creation_user_objective_question_answers do |t|
      t.integer :answer_id
      t.integer :user_question_id
      t.integer :question_id
      t.integer :user_id
      t.boolean :is_correct_answer
      t.timestamps null: false
    end
    add_foreign_key :course_creation_user_objective_question_answers,
                    :course_creation_questions,
                    column: :question_id
    add_foreign_key :course_creation_user_objective_question_answers,
                    :course_creation_user_questions,
                    column: :user_question_id
  end
end
