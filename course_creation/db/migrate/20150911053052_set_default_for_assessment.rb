# adding default value as 0 to passing percentage in assessment
class SetDefaultForAssessment < ActiveRecord::Migration
  def change
    change_column :course_creation_assessments,
                  :passing_percentage, :float, default: 0
  end
end
