class AddForeignKeyToEvaluationQuestion < ActiveRecord::Migration
  def change
    add_foreign_key :course_creation_evaluation_questions,
                    :course_creation_versions,
                    column: :version_id
    add_index :course_creation_evaluation_questions, :version_id
  end
end
