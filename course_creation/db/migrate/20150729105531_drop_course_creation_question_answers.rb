# drop question_answers table
class DropCourseCreationQuestionAnswers < ActiveRecord::Migration
  def change
    if ActiveRecord::Base.connection.table_exists? :course_creation_question_answers
      drop_table :course_creation_question_answers
    end
  end
end
