# add published column to course_creation_versions
class AddPublishedToCourseCreationVersions < ActiveRecord::Migration
  def change
    add_column :course_creation_versions, :published, :boolean, default:  false
  end
end
