class AddTypeToCourseCreationInteractiveSlidesInformation < ActiveRecord::Migration
  def change
    add_column :course_creation_interactive_slides_informations, :type, :string, null: false
  end
end
