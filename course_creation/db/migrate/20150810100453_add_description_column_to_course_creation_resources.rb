# add description column to resources table
class AddDescriptionColumnToCourseCreationResources < ActiveRecord::Migration
  def change
    add_column :course_creation_resources, :description, :string
  end
end
