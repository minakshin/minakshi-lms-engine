# create table for question answers
class CreateCourseCreationQuestionAnswers < ActiveRecord::Migration
  def change
    create_table :course_creation_question_answers do |t|
      t.boolean :correct_answer
      t.integer :question_id
      t.integer :answer_id
      t.timestamps null: false
    end
    add_foreign_key :course_creation_question_answers,
                    :course_creation_questions,
                    column: :question_id
    add_foreign_key :course_creation_question_answers,
                    :course_creation_answers,
                    column: :answer_id
    add_index :course_creation_question_answers, :question_id
    add_index :course_creation_question_answers, :answer_id
  end
end
