# Change column type of passing_criteria in assessments
class ChangeColumnPassingCriteriaFromStringToBooleanInAssessments <
  ActiveRecord::Migration
  def up
    change_column :course_creation_assessments, :passing_criteria,
                  'boolean USING (CASE passing_criteria WHEN \'yes\' THEN
                    \'t\'::boolean ELSE \'f\'::boolean END)'
  end

  def down
    change_column :course_creation_assessments, :passing_criteria,
                  'varchar USING (CASE passing_criteria WHEN \'t\'::boolean THEN
                   \'yes\' ELSE \'no\' END)'
  end
end
