class AddChapterOrderToCourseCreationResources < ActiveRecord::Migration
  def change
    add_column :course_creation_resources, :chapter_order, :integer
  end
end
