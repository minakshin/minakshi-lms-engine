class AddAssessmentRefCreateCourseCreationQuestion < ActiveRecord::Migration
  def change
    add_column :course_creation_questions, :assessment_id, :integer
    add_foreign_key :course_creation_questions, :course_creation_assessments,
                    column: :assessment_id
    add_index :course_creation_questions, :assessment_id
  end
end
