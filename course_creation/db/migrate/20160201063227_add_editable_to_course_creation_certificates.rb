# Add editable field to certificate table
class AddEditableToCourseCreationCertificates < ActiveRecord::Migration
  def change
    add_column :course_creation_certificates, :editable, :boolean,
               default: true
  end
end
