class CreateCourseCreationCourseSections < ActiveRecord::Migration
  def change
    create_table :course_creation_course_sections do |t|
      t.integer :parent_id
      t.string :name
      t.integer :version_id

      t.timestamps null: false
    end
  end
end
