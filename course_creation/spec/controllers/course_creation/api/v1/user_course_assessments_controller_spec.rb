require 'spec_helper'
module CourseCreation
  module API
    # User course assessment controller
    module V1
      describe UserCourseAssessmentsController do
        # Test case for user course assessment details api
        let(:user_course_assessment) do
          FactoryGirl.create(:course_creation_user_course_assessments)
        end
        describe 'GET Chapter', type: :request do
          it 'validate end point' do
            get api_v1_user_course_user_course_assessment_path(
              user_course_assessment.user_course_id, user_course_assessment.assessment.course_section_id)
            expect(response).to be_success
          end

          it 'get details of valid user course assessment' do
            get api_v1_user_course_user_course_assessment_path(
              user_course_assessment.user_course_id, user_course_assessment.assessment.course_section_id)
            status = JSON.parse(response.body)['status']['code']
            expect(status).to eq(200)
          end

          it 'Invalid user course assessment' do
            get api_v1_user_course_user_course_assessment_path(1, -222)
            status = JSON.parse(response.body)['status']['code']
            expect(status).to eq(404)
          end

          it 'Validate Json Format' do
            get api_v1_user_course_user_course_assessment_path(
              user_course_assessment.user_course_id, user_course_assessment.assessment.course_section_id)
            parsed_response = JSON.parse(
              response.body)['data']
            expect(parsed_response['id']).to eq(user_course_assessment.assessment.course_section_id)
            expect(parsed_response['name']).to eq(
              user_course_assessment.assessment.course_section.name)
            expect(parsed_response['content_details']['id']).to eq(
              user_course_assessment.assessment_id)
            expect(parsed_response['content_details']['assessment_type']).to eq(
              user_course_assessment.assessment.assessment_type)
            expect(response).to be_success
          end
        end
      end
      # End of  user course assessment details api
    end
  end
end
