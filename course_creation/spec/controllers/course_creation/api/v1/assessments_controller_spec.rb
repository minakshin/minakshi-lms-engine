require 'spec_helper'
module CourseCreation
  module API
    # Course section controller test cases
    module V1
      describe AssessmentsController do
        let(:assessment) { FactoryGirl.create(:course_creation_assessment) }
        let(:question) do
          FactoryGirl.create(:course_creation_assessment_question,
                             assessment_id: assessment.id)
        end
        describe 'GET #show', type: :request do
          it 'get details of assessment' do
            get api_v1_assessment_path(assessment.id)
            expect(response).to be_success
          end

          it 'invalid assesment object' do
            get api_v1_assessment_path(-1)
            status = JSON.parse(response.body)['status']['code']
            expect(status).to eq(404)
          end

          it 'should return data in valid format' do
            get api_v1_assessment_path(assessment.id)
            parsed_response = JSON.parse(response.body)['data']
            expect(parsed_response['id']).to eq(assessment.id)
            expect(parsed_response['name']).to eq(assessment.name)
          end

          it 'should return question and answers related to assesment' do
            FactoryGirl.create_list(:course_creation_answer,
                                    5, question_id: question.id)
            get api_v1_assessment_path(question.assessment.id)
            parsed_response = JSON.parse(response.body)['data']
            expect(parsed_response['id']).to eq(question.assessment.id)
            expect(parsed_response['content_data'][0]['answers'].count).to eq(
              question.answers.count)
          end
        end
      end
    end
  end
end
