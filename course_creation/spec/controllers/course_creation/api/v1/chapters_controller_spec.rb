require 'spec_helper'
module CourseCreation
  module API
    # Course section controller test cases
    module V1
      describe ChaptersController do
        # Test case for chapter details api
        let(:chapter) { FactoryGirl.create(:course_creation_course_section) }
        let(:custom_content) do
          FactoryGirl.create(:course_creation_customcontent,
                             course_section_id: chapter.id)
        end
        let(:video) do
          FactoryGirl.create(:video, course_section_id: chapter.id)
        end
        let(:document) do
          FactoryGirl.create(:document, course_section_id: chapter.id)
        end
        let(:interactive_slide) do
          FactoryGirl.create(:course_creation_interactive_slide)
        end
        let(:presentation) do
          FactoryGirl.create(:presentation)
        end
        describe 'GET Chapter', type: :request do
          it 'validate end point' do
            get api_v1_chapter_path(chapter.id)
            expect(response).to be_success
          end

          it 'get details of valid chapter' do
            get api_v1_chapter_path(chapter.id)
            status = JSON.parse(response.body)['status']['code']
            expect(status).to eq(200)
          end

          it 'Invalid Chapter' do
            get api_v1_chapter_path(-222)
            status = JSON.parse(response.body)['status']['code']
            expect(status).to eq(404)
          end

          it 'Validate Json Format' do
            FactoryGirl.create_list(:course_creation_course_section, 3,
                                    parent_id: chapter.id)
            get api_v1_chapter_path(chapter.id)
            parsed_response = JSON.parse(response.body)['data']
            expect(parsed_response['id']).to eq(chapter.id)
            expect(parsed_response['contents'].count
                  ).to eq(chapter.contents.count)
          end
          # for course_section show method for all types of content
          it 'get details of course_section' do
            get api_v1_course_section_path(chapter.id)
            expect(response).to be_success
          end

          it 'invalid course_section object' do
            get api_v1_course_section_path(-1)
            status = JSON.parse(response.body)['status']['code']
            expect(status).to eq(404)
          end

          it 'should return data in valid format' do
            get api_v1_course_section_path(chapter.id)
            parsed_response = JSON.parse(response.body)['data']
            expect(parsed_response['id']).to eq(chapter.id)
            expect(parsed_response['name']).to eq(chapter.name)
          end

          it 'should return custom_content related to course_section' do
            get api_v1_course_section_path(custom_content.course_section_id)
            parsed_response = JSON.parse(response.body)['data']
            expect(parsed_response['id']).to eq(
              custom_content.course_section_id)
          end

          it 'should return video related to course_section' do
            get api_v1_course_section_path(video.course_section_id)
            parsed_response = JSON.parse(response.body)['data']
            expect(parsed_response['id']).to eq(video.course_section_id)
          end

          it 'should return document related to course_section' do
            get api_v1_course_section_path(document.course_section_id)
            parsed_response = JSON.parse(response.body)['data']
            expect(parsed_response['id']).to eq(document.course_section_id)
          end

          it 'should return interactive_slide related to course_section' do
            get api_v1_course_section_path(interactive_slide.course_section_id)
            parsed_response = JSON.parse(response.body)['data']
            expect(parsed_response['id']).to eq(
              interactive_slide.course_section.id)
          end
          it 'should return presentation related to course_section' do
            get api_v1_course_section_path(presentation.course_section_id)
            parsed_response = JSON.parse(response.body)['data']
            expect(parsed_response['id']).to eq(presentation.course_section.id)
          end

          it 'Validate resources api' do
            FactoryGirl.create_list(:document, 5, course_section_id: chapter.id)
            FactoryGirl.create_list(:video, 5, course_section_id: chapter.id)
            video_count = chapter.resources.where(
              type: 'CourseCreation::Video').count
            document_count = chapter.resources.where(
              type: 'CourseCreation::Document').count
            get api_v1_chapter_resource_path(chapter.id)
            parsed_response = JSON.parse(response.body)['data']
            expect(parsed_response['id']).to eq(chapter.id)
            expect(parsed_response['name']).to eq(chapter.name)
            expect(parsed_response['resources'].count).to eq(2)
            expect(parsed_response['resources']['videos'].count).to eq(
              video_count)
            expect(parsed_response['resources']['documents'].count).to eq(
              document_count)
          end
        end
      end
    end
  end
end
