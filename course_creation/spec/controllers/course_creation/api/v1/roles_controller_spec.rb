require 'spec_helper'
module CourseCreation
  module API
    # Role controller test cases
    module V1
      describe RolesController do
        describe 'GET Course list', type: :request do
          let(:version_role) do
            FactoryGirl.create(:course_creation_version_roles)
          end
          it 'get course list' do
            get versions_api_v1_role_path(version_role.role_id)
            expect(response).to be_success
          end

          it 'should return 404 for invalid role id' do
            get versions_api_v1_role_path(-1)
            status = JSON.parse(response.body)['status']['code']
            expect(status).to eq(404)
          end

          it 'should retun valid json format' do
            get versions_api_v1_role_path(version_role.role_id)
            parsed_response = JSON.parse(response.body)['courses']
            expect(parsed_response.first['id']).to eq(version_role.version_id)
          end
        end
      end
    end
  end
end
