require 'spec_helper'
module CourseCreation
  module API
    # UserCourseProgresses controller test cases
    module V1
      describe UserCourseProgressesController do
        describe 'POST #create', type: :request do
          let(:course_progress) do
            FactoryGirl.create(:course_creation_course_progress)
          end

          it 'should return 402 for parameter missing' do
            post api_v1_user_course_progresses_path,
                 user_course_id: course_progress.user_course_id,
                 content_id: course_progress.content_id,
                 content_type: course_progress.content_type
            expect(response).to be_success
          end

          it 'should return 402 if data not saved successfully' do
            post api_v1_user_course_progresses_path,
                 user_course_id: course_progress.user_course_id,
                 content_id: course_progress.content_id
            status = JSON.parse(response.body)['status']['code']
            expect(status).to eq(402)
          end
        end

        describe 'GET #show', type: :request do
          let(:course_progress) { create(:course_creation_course_progress) }

          it 'should return success for valid data' do
            get api_v1_user_course_progress_path(course_progress.id)
            expect(response).to be_success
          end

          it 'should return 404 for invalid data' do
            get api_v1_user_course_progress_path(-1)
            status = JSON.parse(response.body)['status']['code']
            expect(status).to eq(404)
          end

          it 'should return valid data' do
            get api_v1_user_course_progress_path(course_progress.user_course_id)
            parsed_response = JSON.parse(response.body)['data']
            expect(parsed_response['id']).to eq course_progress.id
            expect(parsed_response['content_id']
                  ).to eq course_progress.content_id
          end
        end
      end
    end
  end
end
