require 'spec_helper'
module CourseCreation
  module API
    # Resource test cases
    module V1
      describe ResourcesController do
        describe 'GET #show', type: :request do
          let(:resource) { FactoryGirl.create(:resource) }
          it 'get details of resource' do
            get api_v1_resource_path(resource.id)
            expect(response).to be_success
          end

          it 'should return 404 for invalid resource' do
            get api_v1_resource_path(-1)
            status = JSON.parse(response.body)['status']['code']
            expect(status).to eq(404)
          end

          it 'should return valid data of resource type' do
            get api_v1_resource_path(resource.id)
            parsed_response = JSON.parse(response.body)['data']
            expect(parsed_response['id']).to eq resource.id
            expect(parsed_response['title']).to eq resource.title
            expect(parsed_response['description']).to eq resource.description
          end
        end
      end
    end
  end
end
