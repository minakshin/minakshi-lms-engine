require 'spec_helper'
module CourseCreation
  module API
    # Versions controller test cases
    module V1
      describe VersionsController do
        describe 'GET version', type: :request do
          let(:version) { FactoryGirl.create(:version) }
          it 'should validate end point' do
            get api_v1_version_path(version.id)
            expect(response).to be_success
          end

          it 'should return 200 for valid version id' do
            get api_v1_version_path(version.id)
            status = JSON.parse(response.body)['status']['code']
            expect(status).to eq(200)
          end

          it 'should return 404 for invalid version id' do
            get api_v1_version_path(-1)
            status = JSON.parse(response.body)['status']['code']
            expect(status).to eq(404)
          end

          it 'should return valid data of version type' do
            FactoryGirl.create_list(:course_creation_course_section,
                                    1, version_id: version.id)
            get api_v1_version_path(version.id)
            parsed_response = JSON.parse(response.body)['data']
            expect(parsed_response['id']).to eq version.id
            expect(parsed_response['description']).to eq version.description
            expect(parsed_response['chapters'].first['id']
                  ).to eq version.chapters.first.id
            expect(parsed_response['chapters'].first['name']
                  ).to eq version.chapters.first.name
            expect(parsed_response['chapters'].first['version_id']
                  ).to eq version.chapters.first.version_id
          end
        end
      end
    end
  end
end
