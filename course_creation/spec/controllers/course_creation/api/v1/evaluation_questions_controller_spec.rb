require 'spec_helper'
module CourseCreation
  module API
    # UserCourses controller test cases
    module V1
      describe EvaluationQuestionsController do
        let(:couse_evaluation) do
          FactoryGirl.create(:course_creation_user_course_evaluation)
        end

        let(:user_course) do
          FactoryGirl.create(:course_creation_user_courses)
        end

        let(:eval_question) do
          FactoryGirl.create(:course_creation_course_evaluation_question)
        end
        describe 'POST #create', type: :request do
          it 'should return success for evaluation questions valid data' do
            post api_v1_evaluation_questions_path, user_course_evaluation: {
              user_course_id: user_course.id,
              evaluation_questions_attributes:
                [answer_id: eval_question.answer_id,
                 evaluation_question_id: eval_question.user_course_evaluation_id]
            }
            expect(response).to be_success
          end

          it 'should return 404 for parameter missing' do
            post api_v1_evaluation_questions_path, user_course_evaluation: {
              evaluation_questions_attributes:
                [answer_id: eval_question.answer_id]
            }
            status = JSON.parse(response.body)['status']['code']
            expect(status).to eq(404)
          end
        end
      end
    end
  end
end
