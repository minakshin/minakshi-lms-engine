require File.expand_path('../../../rails_helper', __FILE__)
require 'spec_helper'
# Presentation Controller
module CourseCreation
  describe PresentationsController do
    let(:course_section) { create :course_creation_course_section }
    describe 'POST #create', type: :request do
      context 'with valid attributes' do
        attributes = FactoryGirl.attributes_for(
          :presentation,
          slides_attributes: { '0' => FactoryGirl.attributes_for(:slide) })
        it 'create new presentation' do
          expect do
            post version_course_section_presentations_path(
              course_section.version_id, course_section),
                 presentation: attributes
          end.to change(Presentation, :count).by(1)
        end
        it 'redirects to edit slides' do
          post version_course_section_presentations_path(
            course_section.version_id, course_section),
               presentation: attributes
          expect(response).to redirect_to(
            edit_version_course_section_presentation_slide_path(
              course_section.version_id, course_section,
              Presentation.last.id, Presentation.last.slides.first))
        end
      end
      context 'with invalid attributes' do
        attributes = FactoryGirl.attributes_for(:invalid_presentation)
        it 'does not create new presentation' do
          expect do
            post version_course_section_presentations_path(
              course_section.version_id, course_section),
                 presentation: attributes
          end.to_not change(Presentation, :count)
        end
        it 'render new view' do
          post version_course_section_presentations_path(
            course_section.version_id, course_section),
               presentation: attributes
          expect(response).to render_template :new
        end
      end
    end
  end
end
