require File.expand_path('../../../rails_helper', __FILE__)
require 'spec_helper'
# course creation module
module CourseCreation
  describe CoursesController do

    routes { CourseCreation::Engine.routes }

    describe 'GET #index' do
      let(:course) { FactoryGirl.create(:course_creation_course) }
      it 'renders the :index view' do
        get :index
        response.should render_template :index
      end
      it 'populates an array of courses' do
        get :index
        expect(:course).not_to be_empty
      end
    end

    describe 'GET #show' do
      let(:course) { FactoryGirl.create(:course_creation_course) }
      it 'renders the :show template' do
        get :show, id: course.id
        response.should render_template :show
      end
      it 'assigns the requested course to @course' do
        get :show, id: course.id
      end
    end

    describe 'GET #new' do
      it 'assigns a new Course to @course' do
        get :new
      end
    end

    describe 'POST #create' do
      context 'with valid attributes' do
        it 'creates a new course' do
          expect do
            post :create, course:
                 attributes_for(:course_creation_course).merge(versions_attributes: [ attributes_for(:course_creation_version) ])
          end.to change(Course, :count).by(1)
        end
        it 'redirects to the new courses' do
          post :create, course:
               attributes_for(:course_creation_course).merge(versions_attributes: [ attributes_for(:course_creation_version) ])
          response.should redirect_to syllabus_version_path(Course.last.versions.first)
        end
      end
      context 'with invalid attributes' do
        it 'does not save the new category' do
          expect do
            post :create, course: attributes_for(:invalid_course_creation_course).merge( versions_attributes: [attributes_for(:course_creation_version)])
          end.to_not change(Course, :count)
        end
        it 're-renders the :new template' do
          post :create, course: attributes_for(:invalid_course_creation_course).merge( versions_attributes: [attributes_for(:course_creation_version)])
          response.should render_template :new
        end
      end
    end
    describe 'DELETE destroy' do
      let!(:course) { FactoryGirl.create(:course_creation_course) }
      it 'redirects to course#index' do
        get :index
        response.should render_template :index
      end
      it 'deletes the course' do
        expect do
          delete :destroy, id: course
        end.to change(Course, :count).by(-1)
      end
    end

    describe 'PUT update' do
      let!(:course) { create :course_creation_course_versions }
      it 'update the requested @courses' do
        new_title = Faker::Name.name
        put :update, id: course, course: { title: new_title }
        expect(course.reload.title).to eq(new_title)
      end
    end
  end
end
