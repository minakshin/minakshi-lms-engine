require File.expand_path('../../../rails_helper', __FILE__)
require 'spec_helper'
# course creation module
module CourseCreation
  describe CourseSectionsController do
    let(:version) { create :course_creation_version }

    def this_version_cs
      create :course_creation_course_section, version_id: version.id
    end

    describe 'POST #create', type: :request do
      it 'creates a new course section' do
        attributes = FactoryGirl.attributes_for(:course_creation_course_section)
        expect do
          post "/course_creation/versions/#{version.id}/course_sections",
               course_section: attributes,
               format: :js
        end.to change(CourseSection, :count).by(1)
      end
    end

    describe 'PUT #update', type: :request do
      it 'edits existing course section' do
        cs = this_version_cs
        old_name = cs.name

        put "/course_creation/versions/#{version.id}/course_sections/#{cs.id}",
            course_section: { name: 'new_name' },
            format: :js
        cs.reload.name.should_not eq(old_name)
        cs.name.should eq('new_name')
      end
    end

    describe 'DELETE destroy', type: :request do
      it 'deletes the course section' do
        cs = this_version_cs
        expect do
          delete "/course_creation/versions/#{version.id}/course_sections/#{cs.id}",
                 format: :js
        end.to change(CourseSection, :count).by(-1)
      end
    end
  end
end
