require File.expand_path('../../../rails_helper', __FILE__)
require 'spec_helper'

# course creation module
module CourseCreation
  describe ResourcesController do

    routes { CourseCreation::Engine.routes }

    let(:version) { create :course_creation_version }
    let(:course_section) { create :course_creation_course_section, version_id: version.id }

    describe 'POST #create', type: :request do
      it 'creates new uploaded resources' do
        %w(video document).each do |rsc|
          attributes = FactoryGirl.attributes_for(rsc.to_sym)
          attributes[:course_section_id] = course_section.id
          expect do
            post "/course_creation/versions/#{version.id}/course_sections/#{course_section.id}/#{rsc}s",
                 Hash[rsc, attributes]
          end.to change("CourseCreation::#{rsc.capitalize}".constantize, :count).by(1)
        end
      end

      it 'creates new sections uploaded resources that are content' do
        %w(video document).each do |rsc|
          attributes = FactoryGirl.attributes_for(rsc.to_sym)
          attributes[:course_section_id] = course_section.id
          expect do
            post "/course_creation/versions/#{version.id}/course_sections/#{course_section.id}/#{rsc}s",
                 Hash[rsc, attributes, :syllabus, true]
          end.to change(CourseCreation::CourseSection, :count).by(1)
          expect(CourseSection.last.content).to be true
          expect(CourseSection.last.content_resource.title).to be_eql(attributes[:title])
        end
      end

      it 'creates new text resources', type: :request do
        %w(quote note).each do |rsc|
          attributes = FactoryGirl.attributes_for(rsc.to_sym)
          attributes[:version_id] = version.id
          expect do
            post "/course_creation/versions/#{version.id}/#{rsc}s",
                 Hash[rsc, attributes]
          end.to change("CourseCreation::#{rsc.capitalize}".constantize, :count).by(1)
        end
      end
    end
  end
end
