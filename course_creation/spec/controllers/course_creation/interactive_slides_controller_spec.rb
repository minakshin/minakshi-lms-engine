require File.expand_path('../../../rails_helper', __FILE__)
require 'spec_helper'
# InteractiveSlide Controller
module CourseCreation
  describe InteractiveSlidesController do
    describe 'POST #create', type: :request do
      context 'with invalid attributes' do
        attributes = FactoryGirl.attributes_for(
          :course_creation_interactive_slide)
        it 'does not create new interactive_slide' do
          expect do
          end.to change(InteractiveSlide, :count).by(0)
        end
      end
     end

   describe 'POST #create', type: :request do
     let(:course_section) do
       FactoryGirl.create(:course_creation_course_section)
     end
     let(:interactive_slide) do
       FactoryGirl.create(:course_creation_interactive_slide,
                          course_section_id: course_section.id)
     end
     it 'creates a new interactive_slide' do
       expect do
         new_version_course_section_interactive_slide_text_slide_path(course_section.id,
         interactive_slide.id, course_section.version_id)
       end.to change(InteractiveSlide, :count).by(1)
     end
   end
  end

  describe 'PUT update' do
    before :each do
      @course_section = FactoryGirl.create(:course_creation_course_section)
      @interactive_slide = FactoryGirl.attributes_for(:course_creation_interactive_slide,
                         course_section_id: @course_section.id)
      context 'valid attributes' do
        it 'located the requested @interactive_slide'do
          put :update, id: @interactive_slide
          assigns(:course_creation_interactive_slide).should eq(@interactiveslide)
        end
      end
    end
  end
end
