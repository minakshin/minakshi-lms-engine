require 'rails_helper'

module CourseCreation
  RSpec.describe EvaluationQuestionsController, type: :controller do
    routes { CourseCreation::Engine.routes }
    let(:version) { create(:course_creation_version)}

    describe 'GET #index' do
      it 'renders the :index view' do
        get :index, version_id: version.id
        expect(response).to render_template :index
      end
    end

    describe 'POST #create' do
      it 'creates a new course' do
        expect do
          post :create, { version_id: version.id, evaluation_question:
              attributes_for(:course_creation_evaluation_question) }
        end.to change(EvaluationQuestion, :count).by(1)
      end
      it 'redirects to index' do
        post :create, { version_id: version.id, evaluation_question:
            attributes_for(:course_creation_evaluation_question) }
        expect(response).to redirect_to version_evaluation_questions_path(version)
      end
    end

    describe 'DELETE destroy' do
      let(:question) { create(:course_creation_evaluation_question, version_id: version.id) }
      it 'deletes the question' do
        id = question.id
        expect do
          delete :destroy, { version_id: version.id, id: id }
        end.to change(EvaluationQuestion, :count).by(-1)
      end
    end
  end
end
