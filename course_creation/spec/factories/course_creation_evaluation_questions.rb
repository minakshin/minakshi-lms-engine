FactoryGirl.define do
  factory :course_creation_evaluation_question, class: 'CourseCreation::EvaluationQuestion' do
    content Faker::Lorem.paragraph
    active false
  end
end
