FactoryGirl.define do
  factory :course_creation_interactive_slides_information, class: 'CourseCreation::InteractiveSlidesInformation' do
    name Faker::Name.name
    description Faker::Lorem.sentence
    association :interactive_slide,
                 factory: :course_creation_interactive_slide,
                 strategy: :create
    association :version,
                factory: :course_creation_version,
                strategy: :create
    association :course_section,
                factory: :course_creation_course_section,
                strategy: :create
  end
end
