FactoryGirl.define do
  factory :course_creation_certificate, class: 'CourseCreation::Certificate' do
    title Faker::Name.name
    description Faker::Lorem.sentence
    file { Rack::Test::UploadedFile.new(File.join(CourseCreation::Engine.root, '/spec/fixtures/test.pdf')) }
  end
end
