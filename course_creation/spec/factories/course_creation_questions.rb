FactoryGirl.define do
  factory :course_creation_question, class: 'CourseCreation::Question' do
    title Faker::Name.name
    question_category_id Faker::Number.between(1, 9)
  end

  factory :course_creation_assessment_question,
          class: 'CourseCreation::Question' do
    association :assessment,
                factory: :course_creation_assessment,
                strategy: :create
    association :question_category,
                factory: :course_creation_question_category,
                strategy: :create
    title Faker::Name.name
  end
end
