# spec/factories/categories.rb
 FactoryGirl.define do
   factory :category, class: CourseCreation::Category do
     sequence(:name) { |n| "#{Faker::Name.name}#{n}" }
   end
   factory :invalid_category, parent: :category do |f|
     f.name nil
   end
 end
