FactoryGirl.define do
  factory :course_creation_course_evaluation_question, class: 'CourseCreation::CourseTaking::UserCourseEvaluationQuestion' do |q|
    q.answer_id Faker::Number.between(1, 5)

    association :user_course_evaluation,
                factory: :course_creation_user_course_evaluation,
                strategy: :create
  end
end
