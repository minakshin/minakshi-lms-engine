FactoryGirl.define do
  factory :course_creation_version, class: 'CourseCreation::Version' do
    association :course,
                factory: :course_creation_course,
                strategy: :create
    description Faker::Lorem.sentence
    expiry Faker::Number.number(3)
    price true
  end

  factory :invalid_course_creation_version, parent: :course_creation_version do
    description nil
    expiry nil
    price nil
  end

  factory :unpublish_version, parent: :course_creation_version do
    published false
  end

  factory :publish_version, parent: :course_creation_version do
    published true
  end

  factory :version, parent: :course_creation_version do
    published false
    association :course,
                factory: :course_creation_course,
                strategy: :create
  end
end
