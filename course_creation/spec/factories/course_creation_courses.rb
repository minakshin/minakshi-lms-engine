FactoryGirl.define do
  factory :course_creation_course, class: 'CourseCreation::Course' do
    sequence(:title) { |n| "#{Faker::Name.name}#{n}" }
    factory :course_creation_course_versions do
      after(:create) do |course_creation_course|
        create(:course_creation_version,
               course_id: course_creation_course.id)
      end
    end
  end
  factory :invalid_course_creation_course, class:
            'courseCreation::Course' do
    title nil
    factory :invalid_course_creation_course_versions do
      after(:create) do |invalid_course_creation_course|
        create(:invalid_course_creation_course,
               invalid_course_creation_course:
               invalid_course_creation_course)
      end
    end
  end
end
