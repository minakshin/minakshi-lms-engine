FactoryGirl.define do
  factory :course_creation_user_question, class: 'CourseCreation::CourseTaking::UserQuestion' do
    assessment_type 'test'
    association :user_course,
                factory: :course_creation_user_courses,
                strategy: :create
    association :assessment,
                factory: :course_creation_assessment,
                strategy: :create
    association :question,
                factory: :course_creation_assessment_question,
                strategy: :create
    question_type_id Faker::Number.between(1,3)
  end
end
