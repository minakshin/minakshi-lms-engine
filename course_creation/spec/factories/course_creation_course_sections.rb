FactoryGirl.define do
  factory :course_creation_course_section,
          class: 'CourseCreation::CourseSection' do
    name Faker::Company.catch_phrase
    content true
    association :version,
                factory: :course_creation_version,
                strategy: :create
  end

  factory :version_chapters, class: 'CourseCreation::CourseSection' do
    sequence(:name) { |n| "chapter-#{n}" }
  end
end
