FactoryGirl.define do
  factory :course_creation_role, class: 'CourseCreation::Role' do |r|
    r.name Faker::Name.name
  end
end
