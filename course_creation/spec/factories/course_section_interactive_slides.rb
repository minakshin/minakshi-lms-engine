FactoryGirl.define do
  factory :course_creation_interactive_slide, class: 'CourseCreation::InteractiveSlide' do
    name Faker::Name.name
    description Faker::Lorem.sentence
    interactive_slide_type CourseCreation::InteractiveSlide::TEXTSLIDE
    association :course_section,
                factory: :course_creation_course_section,
                strategy: :create
  end
end
