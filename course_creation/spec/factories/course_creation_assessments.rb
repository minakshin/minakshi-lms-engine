FactoryGirl.define do
  factory :course_creation_practice_assessment, class:
           'CourseCreation::Assessment' do
    association :course_section,
                factory: :course_creation_course_section,
                strategy: :create
    name Faker::Name.name
    description Faker::Lorem.sentence
    assessment_type CourseCreation::Assessment::TYPES[:practice]
  end

  factory :course_creation_assessment,
          class: 'CourseCreation::Assessment' do
    association :course_section,
                factory: :course_creation_course_section,
                strategy: :create
    name Faker::Name.name
    description Faker::Lorem.sentence
    assessment_type CourseCreation::Assessment::TYPES[:quiz]
  end

  factory :course_creation_quiz_assessment, class:
           'CourseCreation::Assessment' do
    association :course_section,
                factory: :course_creation_course_section,
                strategy: :create
    name Faker::Name.name
    description Faker::Lorem.sentence
    assessment_type CourseCreation::Assessment::TYPES[:quiz]
  end

  factory :course_creation_no_test_assessment, class:
           'CourseCreation::Assessment' do
    association :course_section,
                factory: :course_creation_course_section,
                strategy: :create
    name Faker::Name.name
    description Faker::Lorem.sentence
    assessment_type CourseCreation::Assessment::TYPES[:quiz]
    passing_criteria CourseCreation::Assessment::PASSING_CRITERIA_NO
  end

  factory :course_creation_yes_test_assessment, class:
          'CourseCreation::Assessment' do
    association :course_section,
                factory: :course_creation_course_section,
                strategy: :create
    name Faker::Name.name
    description Faker::Lorem.sentence
    assessment_type CourseCreation::Assessment::TYPES[:quiz]
    passing_criteria CourseCreation::Assessment::PASSING_CRITERIA_YES
    passing_percentage Faker::Number.between(45, 100)
    no_of_displayed_questions Faker::Number.between(1, 10)
  end

  factory :course_creation_invalid_assessment, class:
           'CourseCreation::Assessment' do
    association :course_section,
                factory: :course_creation_course_section,
                strategy: :create
    name ''
    description Faker::Lorem.sentence
    assessment_type CourseCreation::Assessment::TYPES[:test]
    passing_criteria CourseCreation::Assessment::PASSING_CRITERIA_YES
  end
end
