FactoryGirl.define do
  factory :course_creation_user_course_assessments,
          class: 'CourseCreation::CourseTaking::UserCourseAssessment' do
    status 'Complete'
    assessment_percentage Faker::Number.between(0, 100)
    assessment_type 'test'

    association :user_course,
                factory: :course_creation_user_courses,
                strategy: :create
    association :assessment,
                factory: :course_creation_assessment,
                strategy: :create
  end
end
