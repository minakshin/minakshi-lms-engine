FactoryGirl.define do
  factory :course_creation_course_progress,
    class: 'CourseCreation::CourseTaking::UserCourseProgress' do
    association :user_course,
                factory: :course_creation_user_courses,
                strategy: :create
    association :section,
                factory: :course_creation_course_section,
                strategy: :create
    content_type Faker::Name.title
  end
end
