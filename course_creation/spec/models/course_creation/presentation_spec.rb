require File.expand_path('../../../rails_helper', __FILE__)
require 'spec_helper'
# module course creation for presentation model
module CourseCreation
  describe Presentation do
    it 'has a valid factory' do
      FactoryGirl.create(:presentation).should be_valid
    end
    it 'is invalid without title' do
      FactoryGirl.build(:invalid_presentation).should_not be_valid
    end
  end
end
