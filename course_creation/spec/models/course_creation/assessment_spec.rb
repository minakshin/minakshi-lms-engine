require 'rails_helper'
# Assessment spec
module CourseCreation
  describe Assessment do
    it 'is valid with quiz assessment attributes' do
      FactoryGirl.build(:course_creation_quiz_assessment).should be_valid
    end

    it 'is valid with no passing criteria test assessment attributes' do
      FactoryGirl.build(:course_creation_no_test_assessment).should be_valid
    end

    it 'is valid with passing criteria test assessment attributes' do
      FactoryGirl.build(:course_creation_yes_test_assessment).should be_valid
    end

    it 'is not valid with no name' do
      FactoryGirl.build(:course_creation_invalid_assessment).should_not be_valid
    end
  end
end
