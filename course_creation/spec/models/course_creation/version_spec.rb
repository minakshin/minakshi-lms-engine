require File.expand_path('../../../rails_helper', __FILE__)
require 'spec_helper'
# module course creation for course model
module CourseCreation
  describe 'ActiveRecord associations' do
    it { expect { belongs_to(:course_creation_course) } }
    it { expect { belongs_to(:course_creation_category) } }
    it { expect { has_many(:course_creation_version_roles) } }
  end

  describe Version do
    it 'is invalid without a description' do
      FactoryGirl.build(:invalid_course_creation_version, description: nil
                       ).should_not be_valid
    end
    it 'returns a version\'s description as a string' do
      FactoryGirl.create(:course_creation_version
                        ).should be_valid
    end
  end
end
