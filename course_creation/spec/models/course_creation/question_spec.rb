require File.expand_path('../../../rails_helper', __FILE__)
require 'spec_helper'
# module course creation for course model
module CourseCreation
  describe 'ActiveRecord associations' do
    it { expect { have_many(:course_creation_answer) } }
  end
end
