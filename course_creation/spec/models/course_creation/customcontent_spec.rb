require 'rails_helper'
# Customcontent spec
module CourseCreation
  describe CustomContent do
    it 'is not valid with no name and other than .zip extension' do
      invalid = FactoryGirl.build(:invalid_course_creation_customcontent)
      invalid.should_not be_valid
    end

    it 'is not valid with empty attributes submit' do
      nozip = FactoryGirl.build(:nozip_course_creation_customcontent)
      nozip.should_not be_valid
    end
  end
end
