require File.expand_path('../../../rails_helper', __FILE__)
require 'spec_helper'
# module course creation for course model
module CourseCreation
  describe 'ActiveRecord associations' do
    it { expect { have_many(:course_creation_version) } }

    it 'association with version' do
      expect do
        accept_nested_attributes_for(:course_creation_version
                                    ).allow_destroy(true)
      end
    end
  end
  describe Course do
    it 'is invalid without a name' do
      FactoryGirl.build(:course_creation_course, title: nil).should_not be_valid
    end
    it 'returns a course\'s full name as a string' do
      FactoryGirl.create(:course_creation_course, title: 'test'
                        ).title.should == 'test'
    end
  end
end
