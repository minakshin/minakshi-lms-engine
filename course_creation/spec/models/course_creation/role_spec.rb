require File.expand_path('../../../rails_helper', __FILE__)
require 'spec_helper'
# module course creation for course model
module CourseCreation
  describe 'ActiveRecord associations' do
    it { expect { has_many(:course_creation_role) } }
    it { expect { has_many(:course_creation_version) } }
  end
end
