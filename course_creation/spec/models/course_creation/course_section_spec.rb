require File.expand_path('../../../rails_helper', __FILE__)
require 'spec_helper'
# course creation module for category
module CourseCreation
  describe CourseSection do
    it 'is invalid without a name' do
      build(:course_creation_course_section, name: nil).should_not be_valid
    end

    it 'is invalid with too long of a name' do
      build(:course_creation_course_section,
            name: 'This name is way too long to be a valid name for a course
                  section'
            ).should_not be_valid
    end
  end
end
