module CourseCreation
  # User course section serialiazer
  class UserSectionSerializer < ActiveModel::Serializer
    attributes :id, :name, :status, :is_section, :has_contents, :is_content

    def name
      return object.name.split(':').last if object.is_assessment
      object.name
    end

    def status
      section = context[:course_progress].where(content_id: id).last
      section.blank? ? ApiStatuses::STATUS[0] : section.status
    end

    def is_section
      object.content? ? false : true
    end

    def has_contents
      object.contents.present?
    end

    def is_content
      object.name.split(':').first.eql?('Assessment') && !has_contents
    end
  end
end
