module CourseCreation
  # User purchased course chapter details
  class UserChapterStateSerializer < ActiveModel::Serializer
    attributes :id, :chapter_name, :status, :contents

    def status
      chapter = user_course_progress.where(content_id: section.id).last
      chapter.blank? ? ApiStatuses::STATUS[0] : chapter.status
    end

    def chapter_name
      section.name
    end

    def contents
      if section.is_assessment
        course_section_serializer(section)
      else
        sections =
          section.parent_id.blank? ? chapter_contents : section.contents
        sections.rank(:chapter_order).map do |content|
          course_section_serializer(content)
        end
      end
    end

    def course_section_serializer(section)
      CourseSectionSerializer.new(
        section, root: false,
                 context: { course_progress: user_course_progress,
                            user_course: object })
    end

    def user_course_progress
      object.course_progresses
    end

    def section
      serialization_options[:section]
    end

    def chapter_contents
      section.contents.where(content: true)
    end
  end
end
