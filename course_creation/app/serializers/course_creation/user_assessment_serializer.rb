module CourseCreation
  # UserCourseAssessment Serializer
  class UserAssessmentSerializer < ActiveModel::Serializer
    attributes :id, :total_question_count, :correct_questions,
               :user_question, :passing_status, :assessment_percentage

    def user_question
      user_assessment.map do |question|
        UserAssessmentQuestionSerializer.new(
          question, scope: scope, root: false)
      end
    end

    def user_assessment
      CourseTaking::UserQuestion.where(
        'assessment_id = ? AND user_course_id = ?',
        object.assessment_id, object.user_course_id)
    end

    def total_question_count
      user_assessment.count
    end

    def correct_questions
      user_assessment.where(passing_status: 1).count
    end

    def passing_status
      object.status
    end
  end
end
