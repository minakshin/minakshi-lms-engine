module CourseCreation
  # User purchased course chapter details
  class UserSectionStateSerializer < ActiveModel::Serializer
    attributes :id, :name, :status, :contents

    def status
      section = context[:course_progress].where(content_id: id).last
      section.blank? ? ApiStatuses::STATUS[0] : section.status
    end

    def contents
      object.contents.rank(:chapter_order).map do |content|
        UserSectionStateSerializer.new(content, root: false)
      end
    end
  end
end
