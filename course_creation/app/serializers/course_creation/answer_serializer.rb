module CourseCreation
  # Answer Serializer
  class AnswerSerializer < ActiveModel::Serializer
    attributes :id, :option, :image
    def image
      object.image.present? && object.image.url
    end
  end
end
