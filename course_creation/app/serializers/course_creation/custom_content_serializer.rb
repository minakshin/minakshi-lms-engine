module CourseCreation
  # Slide serializer
  class CustomContentSerializer < ActiveModel::Serializer
    attributes :id, :name, :zip, :course_section_id, :unzip

    def zip
      object.zip.present? && object.zip.url
    end

    def common_path
      zip_name = "#{object.zip_identifier[0..-5].gsub(/\W/, '_')}"
      "#{object.bucket_path}/#{zip_name}"
    end

    def unzip
      if object.zip.present?
        unzip_folder = File.basename(object.zip.path)[0..-5].gsub(/\W/, '_')
        "#{File.dirname(object.zip.url)}/#{unzip_folder}/index.html"
      else
        "https://#{S3_BUCKET}.s3.amazonaws.com/#{common_path}/index.html"
      end
    end
  end
end
