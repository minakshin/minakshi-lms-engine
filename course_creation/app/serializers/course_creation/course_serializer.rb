module CourseCreation
  # Course Serializer
  class CourseSerializer < ActiveModel::Serializer
    attributes :id, :title
    has_many :versions
  end
end
