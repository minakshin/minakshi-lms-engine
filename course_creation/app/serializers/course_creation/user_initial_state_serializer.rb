module CourseCreation
  # User initial state for course progress
  class UserInitialStateSerializer < ActiveModel::Serializer
    attributes :chapter, :section, :content

    def chapter
      object.id
    end

    def section
      return nil if chapter_first_content.first.present? ||
                    chapter_first_section.blank?
      chapter_first_section.first.content ? nil : chapter_first_section.first.id
    end

    def content
      return chapter_first_content.first.id if chapter_first_content.present?
      chapter_first_section.first.contents.first.id if
        chapter_first_section.present? &&
        chapter_first_section.first.contents.present?
    end

    def chapter_first_section
      object.contents.rank(:chapter_order).where(content: false)
    end

    def chapter_first_content
      object.contents.rank(:chapter_order).where(content: true)
    end
  end
end
