module CourseCreation
  # UserAnswerSerializer Serializer
  class UserAnswerSerializer < ActiveModel::Serializer
    attributes :answer_id, :user_ans_text, :user_ans_image, :user_ans_position

    def user_ans_text
      user_answer.option if user_answer.option.present?
    end

    def user_ans_image
      user_answer.image.url if user_answer.image.url.present?
    end

    def user_answer
      Answer.find(object.answer_id)
    end

    def user_ans_position
      object.position
    end
  end
end
