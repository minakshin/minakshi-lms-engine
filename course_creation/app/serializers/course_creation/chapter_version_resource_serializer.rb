module CourseCreation
  # Resource Serializer
  class ChapterVersionResourceSerializer < ActiveModel::Serializer
    attributes :name, :resources

    def resources
      object.resources.where(context).rank(:chapter_order).map do |document|
        VideoSerializer.new(document, scope: scope, root: false)
      end
    end
  end
end
