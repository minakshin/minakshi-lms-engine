module CourseCreation
  # UserCourseAssessment Serializer
  class UserResultAssessmentSerializer < ActiveModel::Serializer
    attributes :id, :total_questions, :correct_questions,
               :user_question, :passing_status, :name, :assessment_type,
               :no_of_displayed_questions, :details_page, :upfront,
               :description, :is_result, :assessment_percentage

    def name
      object.assessment.name
    end

    def id
      object.assessment.id
    end

    def assessment_type
      object.assessment.assessment_type
    end

    def upfront
      object.assessment.upfront
    end

    def description
      object.assessment.description
    end

    def details_page
      object.assessment.details_page
    end

    def no_of_displayed_questions
      object.assessment.no_of_displayed_questions
    end

    def user_question
      user_assessment.map do |question|
        UserAssessmentQuestionSerializer.new(
          question, scope: scope, root: false)
      end
    end

    def user_assessment
      CourseTaking::UserQuestion.where(
        'assessment_id = ? AND user_course_id = ?',
        assessment_id, user_course_id)
    end

    def assessment_id
      object.assessment.id
    end

    def user_course_id
      context[:user_course].id if context[:user_course].present?
    end

    def total_questions
      no_of_displayed_questions || user_assessment.count
    end

    def correct_questions
      user_assessment.where(passing_status: 1).count
    end

    def passing_status
      context[:assessment].status if context[:assessment].present?
    end

    def is_result
      true
    end

    def assessment_percentage
      context[:assessment].assessment_percentage
    end
  end
end
