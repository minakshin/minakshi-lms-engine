module CourseCreation
  # Version Serializer
  class VersionSerializer < ActiveModel::Serializer
    attributes :id, :course_id, :description, :title, :resources
    has_many :chapters, serializer: CourseStructureSerializer

    def chapters
      object.chapters.rank(:course_order)
    end

    def title
      object.course.title
    end

    def course_id
      object.course_id
    end

    def resources
      VersionResourcesSerializer.new(object, scope: scope, root: false)
    end
  end
end
