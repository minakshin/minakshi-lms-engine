module CourseCreation
  # ContentResource Serializer
  class ContentResourceSerializer < ActiveModel::Serializer
    attributes :id, :title, :file, :description
    def file
      object.file.present? && object.file.url
    end
  end
end
