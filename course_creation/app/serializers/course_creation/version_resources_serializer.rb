module CourseCreation
  # Version resources serializer
  class VersionResourcesSerializer < ActiveModel::Serializer
    attributes :notes, :quotes, :documents, :videos

    def notes
      object.notes.map do |note|
        NoteResourceSerializer.new(note, scope: scope, root: false)
      end
    end

    def quotes
      object.quotes.map do |note|
        NoteResourceSerializer.new(note, scope: scope, root: false)
      end
    end

    def documents
      object.chapters.map do |document|
        ChapterVersionResourceSerializer.new(
          document, scope: scope, root: false,
                    context: { type: 'CourseCreation::Document' })
      end
    end

    def videos
      object.chapters.map do |video|
        ChapterVersionResourceSerializer.new(
          video, scope: scope, root: false,
                 context: { type: 'CourseCreation::Video' })
      end
    end
  end
end
