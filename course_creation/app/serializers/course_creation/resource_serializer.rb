module CourseCreation
  # Resource Serializer
  class ResourceSerializer < ActiveModel::Serializer
    attributes :id, :title, :file, :description

    def file
      object.file.present? && object.file.url
    end
  end
end
