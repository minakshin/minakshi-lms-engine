module CourseCreation
  # UserCourse Serializer
  class UserCourseSerializer < ActiveModel::Serializer
    attributes :id, :user_id, :version_id, :title, :progress_percentage,
               :course_status, :chapters, :resources, :user_state,
               :evaluation_questions, :evaluation_completed

    def chapters
      course_progress = { course_progress: user_course_progress }
      object.version.chapters.rank(:course_order).map do |chapter|
        UserChapterSerializer.new(chapter, scope: scope, root: false,
                                           context: course_progress)
      end
    end

    def evaluation_questions
      if evaluation_completed
        object.user_course_evaluation.evaluation_questions.order(:evaluation_question_id).map do |question|
          EvaluationQuestionDetailsSerializer.new(
            question, scope: scope, root: false)
        end
      else
        object.version.evaluation_questions.where(
          active: true).map do |evaluation_question|
          EvaluationQuestionSerializer.new(evaluation_question, scope: scope,
                                                                root: false)
        end
      end
    end

    def evaluation_completed
      object.user_course_evaluation.present?
    end

    def resources
      VersionResourcesSerializer.new(object.version, scope: scope, root: false)
    end

    def user_state
      return nil if object.course_status == ApiStatuses::STATUS[2] || object.content_complete
      state = user_course_progress.where(status: ApiStatuses::STATUS[1])
      return UserStateSerializer.new(state, root: false) unless state.blank?
      UserInitialStateSerializer.new(object.version.chapters.first, root: false)
    end

    def user_course_progress
      object.course_progresses
    end

    def title
      object.version.course.title
    end
  end
end
