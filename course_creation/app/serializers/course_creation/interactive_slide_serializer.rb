module CourseCreation
  # Interactive Slide Serializer
  class InteractiveSlideSerializer < ActiveModel::Serializer
    attributes :id, :name, :description, :version_id, :course_section_id,
               :interactive_slide_type, :content_data

    def content_data
      object.interactive_slides_informations.order(:created_at)
    end
  end
end
