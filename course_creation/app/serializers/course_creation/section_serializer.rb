module CourseCreation
  # Serializer for chapter details API
  class SectionSerializer < ActiveModel::Serializer
    attributes :id, :name, :content
    has_many :contents, serializer: SectionSerializer

    def contents
      object.contents.rank(:chapter_order)
    end
  end
end
