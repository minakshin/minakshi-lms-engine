module CourseCreation
  # Note and quotes serializer
  class NoteResourceSerializer < ActiveModel::Serializer
    attributes :id, :title, :content
  end
end
