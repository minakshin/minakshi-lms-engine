module CourseCreation
  # Document Serializer
  class VideoSerializer < ActiveModel::Serializer
    attributes :id, :title, :file, :description, :thumb

    def file
      object.file.present? && object.file.url
    end

    def thumb
      if object.type.eql? ('CourseCreation::Video')
        return object.file.present? && object.file.thumb.url
      end
    end
  end
end
