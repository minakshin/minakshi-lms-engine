module CourseCreation
  # CorrectAnswerSerializer Serializer
  class CorrectAnswerSerializer < ActiveModel::Serializer
    attributes :id, :correct_ans_text, :correct_ans_image, :correct_ans_position

    def correct_ans_text
      object.option if object.option.present?
    end

    def correct_ans_image
      object.image.url if object.image.url.present?
    end

    def correct_ans_position
      object.position
    end
  end
end
