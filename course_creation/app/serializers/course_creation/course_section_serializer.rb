module CourseCreation
  # Course Section serializer
  class CourseSectionSerializer < ActiveModel::Serializer
    attributes :id, :name, :chapter_name, :status, :content, :content_type,
               :attempt_to
    has_many :contents
    has_one :content_details

    def contents
      object.contents.rank(:chapter_order)
    end

    def wrapped_content
      object.content_resource || content_assessment ||
        object.interactive_slide || object.presentation ||
        object.custom_content
    end

    def content_details
      wrapped_content
    end

    def content_type
      return name.split(':').first.delete(' ') if object.is_assessment
      name.split(':').first.delete(' ') if object.content.present?
    end

    def chapter_name
      return unless object.chapter
      object.chapter.name
    end

    def status
      return ApiStatuses::STATUS[0] if context.blank?
      section = context[:course_progress].where(content_id: id).last
      section.blank? ? ApiStatuses::STATUS[0] : section.status
    end

    def attempt_to
      return ApiStatuses::STATUS[0] if context.blank?
      attempt = context[:course_progress].where(content_id: id).last
      attempt.blank? ? ApiStatuses::STATUS[0] : attempt.attempt_to
    end

    def content_assessment
      return unless object.assessment
      content_statuses = assessment_questions.content_data.map(&:status)
      if content_statuses.include?(ApiStatuses::STATUS[2]) &&
         (content_statuses.uniq.length == 1 || status == ApiStatuses::STATUS[2])
        UserResultAssessmentSerializer.new(
          object, root: false,
                  context: {
                    user_course: context[:user_course],
                    assessment: user_assessment
                  })
      else
        assessment_questions
      end
    end

    def assessment_questions
      AssessmentSerializer.new(
        object.assessment, root: false,
                           context: { user_course: context[:user_course] })
    end

    def user_assessment
      CourseTaking::UserCourseAssessment.where(
        'assessment_id = ? AND user_course_id = ?',
        object.assessment.id, context[:user_course].id).last
    end
  end
end
