module CourseCreation
  # courselisting serializer for course listing api
  class CourseListingSerializer < ActiveModel::Serializer
    attributes :id, :name, :image

    def name
      object.course.title
    end
  end
end
