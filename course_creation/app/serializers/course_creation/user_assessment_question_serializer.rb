module CourseCreation
  # UserAssessmentQuestionSerializer Serializer
  class UserAssessmentQuestionSerializer < ActiveModel::Serializer
    attributes :id, :question_title, :answer_status, :assessment_answer,
               :correct_option

    def question_title
      object.question.title
    end

    def answer_status
      object.passing_status
    end

    def assessment_answer
      object.objective_answers.map do |answer|
        UserAnswerSerializer.new(answer, scope: scope, root: false)
      end
    end

    def correct_option
      object.question.answers.where(correct_answer: true).map do |answer|
        CorrectAnswerSerializer.new(answer, scope: scope, root: false)
      end
    end
  end
end
