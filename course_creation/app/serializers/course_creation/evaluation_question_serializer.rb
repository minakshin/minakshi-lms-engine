module CourseCreation
  # EvaluationQuestion Serializer
  class EvaluationQuestionSerializer < ActiveModel::Serializer
    attributes :id, :content, :answer_id

    def answer_id
      nil
    end
  end
end
