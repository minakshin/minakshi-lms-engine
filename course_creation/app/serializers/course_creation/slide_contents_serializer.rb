module CourseCreation
  # Slide_content serializer
  class SlideContentsSerializer < ActiveModel::Serializer
    attributes :id, :content, :file_url, :content_type

    def file_url
      object.file_url.present? && object.file_url.url
    end

    def content_type
      return 'Text' if object.file_url.blank?
      object.file_url.model.type
    end
  end
end
