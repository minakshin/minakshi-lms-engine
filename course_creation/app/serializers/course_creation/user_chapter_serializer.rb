module CourseCreation
  # User purchased course chapter details
  class UserChapterSerializer < ActiveModel::Serializer
    attributes :id, :name, :status, :contents, :has_contents

    def status
      chapter = context[:course_progress].where(content_id: id).last
      chapter.blank? ? ApiStatuses::STATUS[0] : chapter.status
    end

    def contents
      object.contents.where(content: false).rank(
        :chapter_order).map do |section|
        UserSectionSerializer.new(section, root: false, context: context)
      end
    end

    def has_contents
      object.contents.where(content: true).present?
    end
  end
end
