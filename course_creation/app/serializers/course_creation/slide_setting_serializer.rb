module CourseCreation
  # Slide_setting serializer
  class SlideSettingSerializer < ActiveModel::Serializer
    attributes :bg_type_img, :background, :transition

    def bg_type_img
      object.background_img.present?
    end

    def background
      return object.background_img.url if object.background_img.present?
      object.background_color
    end
  end
end
