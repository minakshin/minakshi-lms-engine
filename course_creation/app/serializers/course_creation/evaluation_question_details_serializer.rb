module CourseCreation
  # EvaluationQuestion Serializer
  class EvaluationQuestionDetailsSerializer < ActiveModel::Serializer
    attributes :id, :content, :answer_id

    def id
      object.evaluation_question_id
    end

    def content
      evaluation_question.content
    end

    def evaluation_question
      EvaluationQuestion.find(object.evaluation_question_id)
    end

    def answer_id
      object.answer_id
    end
  end
end
