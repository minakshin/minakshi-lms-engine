module CourseCreation
  # UserCourseProgress Serializer
  class UserCourseProgressSerializer < ActiveModel::Serializer
    attributes :id, :content_id
  end
end
