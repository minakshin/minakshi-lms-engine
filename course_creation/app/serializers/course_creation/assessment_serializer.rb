module CourseCreation
  # Assessment Serializer
  class AssessmentSerializer < ActiveModel::Serializer
    attributes :id, :name, :description, :content_data, :assessment_type,
               :details_page, :upfront, :no_of_displayed_questions,
               :total_questions, :passing_percentage

    def content_data
      quiz_questions = []
      quiz_questions.push(attempted_questions)
      quiz_questions.push(unattended_questions)
      quiz_questions.flatten!
      quiz_questions.map do |assessment|
        QuestionSerializer.new(
          assessment, scope: scope, root: false,
                      context: { attempted: attempted_questions })
      end
    end

    def attempted_questions
      object.questions.where(id: user_course_questions)
    end

    def unattended_questions
      count = total_questions - user_course_questions.count
      return remaining_questions.sample(count.abs) if object.randomize
      remaining_questions.order(:question_order).limit(count.abs)
    end

    def user_course_questions
      return [] if context.blank?
      context[:user_course].user_questions.where(
        assessment_id: object.id).pluck(:question_id)
    end

    def remaining_questions
      object.questions.where.not(id: user_course_questions)
    end

    def total_questions
      no_of_displayed_questions || object.questions.count
    end
  end
end
