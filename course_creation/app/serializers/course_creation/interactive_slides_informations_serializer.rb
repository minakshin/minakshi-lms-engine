module CourseCreation
  # InteractiveSlidesInformations serializer
  class InteractiveSlidesInformationsSerializer < ActiveModel::Serializer
    attributes :id, :image, :title, :description, :interactive_slide_id, :type

    def image
      object.image.present? && object.image.url
    end
  end
end
