module CourseCreation
  # Serializer for chapter details API
  class ChapterResourceSerializer < ActiveModel::Serializer
    attributes :id, :name, :resources

    def resources
      ChapterResourcesSerializer.new(object, scope: scope, root: false)
    end
  end
end
