require_dependency 'course_creation/application_controller'
module CourseCreation
  # Assessment controller
  class AssessmentsController < CourseCreation::BaseController
    before_action :set_assessment, only: [:show, :edit, :update, :destroy]
    before_action :set_version, except: [:index]
    # GET /assessments
    def index
      @assessments = Assessment.all
    end

    # GET /assessments/new
    def new
      @assessment = Assessment.new
      @types = Assessment::TYPES
    end

    # Post /assessments
    def create
      @assessment = Assessment.new(assessment_params)
      @assessment.course_section_id = params[:course_section_id]
      if @assessment.save
        if params[:section_type].present?
          @assessment.sectionize_assessment
        else
          @assessment.sectionize_content
        end
        set_flash_message :success, :success
        redirect_to assessment_questions_path(@assessment)
      else
        render :new
      end
    end

    # GET /assessments/1
    def show
    end

    # GET /assessments/1/edit
    def edit
    end

    # PATCH/PUT /assessments/1
    def update
      if @assessment.update(assessment_params)
        @assessment.update_section
        set_flash_message :success, :update
        redirect_to assessment_questions_path(@assessment)
      else
        render :edit
      end
    end

    # DELETE /assessments/1
    def destroy
      if @assessment.destroy
        set_flash_message :success, :destroy
      else
        set_flash_message :danger, :error
      end
      redirect_to syllabus_version_path(@version)
    end

    private

    # Use callbacks to share common setup or constraints between actions.
    def set_assessment
      @assessment = Assessment.find(params[:id])
    end

    def set_version
      @version = Version.find(params[:version_id])
    end

    # Only allow a trusted parameter "white list" through.
    def assessment_params
      params.require(:assessment).permit(
        :name, :description, :assessment_type,
        :passing_criteria, :passing_percentage,
        :no_of_displayed_questions, :course_section_id, :upfront,
        :additional_text, :details_page, :randomize)
    end
  end
end
