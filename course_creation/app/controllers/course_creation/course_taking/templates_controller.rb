require_dependency 'course_creation/application_controller'

module CourseCreation
  module CourseTaking
    # TemplatesController
    class TemplatesController < CourseTaking::UserBaseController
      before_filter :user_redirection_url
      def index
        render layout: false
      end

      def show
        render params[:id], layout: false
      end

      private

      def user_redirection_url
        @logout_url = "#{main_app.root_url}#{CourseCreation.logout_url}"
        @dashboard_url = CourseCreation.dashboard_url
      end
    end
  end
end
