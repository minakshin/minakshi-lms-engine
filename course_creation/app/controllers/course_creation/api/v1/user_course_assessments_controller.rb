module CourseCreation
  module API
    module V1
      # User course assessments Controller
      class UserCourseAssessmentsController < API::V1::BaseController
        before_action :user_course, only: :show

        def create
          user_assessment =
            CourseTaking::UserCourseAssessment.new(assessment_params)
          if user_assessment.save
            render json: {
                    status:
                      success(t('course_creation.user_assessment.success')) }
          elsif user_assessment.invalid?
            render json: { status: params_missing }
          else
            render json: { status: failure }
          end
        end

        def show
          assessment_details =
            @user_course.course_assessments.find_by_assessment_id(assessment_id)
          assessment_details.delete_questions unless assessment_details.nil?
          render json: assessment_details,
                 serializer: UserCourseAssessmentSerializer,
                 'status' => success, meta_key: 'status'
        end

        private

        def assessment_id
          CourseSection.find(user_assessment_params).assessment.id
        end

        def assessment_params
          params.require(:user_course_assessment).permit(:assessment_id,
                                                         :user_course_id,
                                                         :assessment_percentage,
                                                         :status)
        end

        def user_assessment_params
          params.require(:id)
        end

        def user_course_params
          params.require(:user_course_id)
        end

        def user_course
          @user_course = CourseTaking::UserCourse.find(user_course_params)
        end
      end
    end
  end
end
