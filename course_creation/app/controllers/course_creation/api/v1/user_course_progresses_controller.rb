module CourseCreation
  module API
    module V1
      # UserCourseProgresses Controller
      class UserCourseProgressesController < API::V1::BaseController
        include ApiStatuses
        before_action :current_content_progress, only: :create
        before_action :next_content_progress, only: :create
        before_action :course_content_completed, only: :create

        def show
          @user_course_progress =
            CourseCreation::CourseTaking::UserCourseProgress.where(
              user_course_id: course_progress_params).last
          if @user_course_progress
            render json: @user_course_progress,
                   serializer: UserCourseProgressSerializer,
                   'status' => success, meta_key: 'status'
          else
            render json: { status: record_not_found }
          end
        end

        def create
          if @user_course_progress.present?
            if @user_course_progress.save
              render json: { status: success(
                'User course progress saved successfully') }
            else
              render json: { status: failure }
            end
          else
            render json: { status: success }
          end
        end

        private

        def current_content_params
          params.require(:current_state).permit(
            :content_id, :content_type, :chapter_id, :section_id)
        end

        def next_content_params
          params.require(:next_state).permit(
            :content_id, :content_type, :chapter_id, :section_id)
        end

        def course_params
          params.require(:user_course_id)
        end

        def last_chapter_flag
          params.require(:is_last_for_chapter)
        end

        def last_section_flag
          params.require(:is_last_for_section)
        end

        def user_course
          CourseTaking::UserCourse.find(course_params)
        end

        def course_content_completed
          return unless params[:is_course_completed].present?
          user_course.update_course_content
        end

        def current_content_progress
          return if params[:current_state].nil?
          update_current_section_chapter_progress
          @user_course_progress =
            CourseTaking::UserCourseProgress.find_or_initialize_by(
              user_course_id: course_params,
              content_id: current_content_params[:content_id],
              content_type: current_content_params[:content_type]
            )
          @user_course_progress.save_status(ApiStatuses::STATUS[2])
          @user_course_progress.user_attempts
        end

        def update_current_section_chapter_progress
          if current_content_params[:section_id].present?
            CourseTaking::UserCourseProgress.update_course_progress(
              course_params, current_content_params[:section_id], nil,
              section_status)
          end
          return unless current_content_params[:chapter_id].present?
          content_id = current_content_params[:chapter_id]
          CourseTaking::UserCourseProgress.update_course_progress(
            course_params, content_id, nil, chapter_status)
        end

        def update_next_section_chapter_progress
          if next_content_params[:section_id].present?
            CourseTaking::UserCourseProgress.update_course_progress(
              course_params, next_content_params[:section_id], nil,
              ApiStatuses::STATUS[1])
          end
          return unless next_content_params[:chapter_id].present?
          CourseTaking::UserCourseProgress.update_course_progress(
            course_params, next_content_params[:chapter_id], nil,
            ApiStatuses::STATUS[1])
        end

        def next_content_progress
          return if params[:next_state].nil?
          update_next_section_chapter_progress
          return unless next_content_params[:content_id].present?
          CourseTaking::UserCourseProgress.update_course_progress(
            course_params, next_content_params[:content_id],
            next_content_params[:content_type], ApiStatuses::STATUS[1])
        end

        def section_status
          last_section_flag ? ApiStatuses::STATUS[2] : ApiStatuses::STATUS[1]
        end

        def chapter_status
          last_chapter_flag ? ApiStatuses::STATUS[2] : ApiStatuses::STATUS[1]
        end

        def course_progress_params
          params.require(:id)
        end
      end
    end
  end
end
