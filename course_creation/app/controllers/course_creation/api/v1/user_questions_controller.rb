module CourseCreation
  module API
    module V1
      # user_questions controller for APi
      class UserQuestionsController < API::V1::BaseController
        def create
          @questions = CourseTaking::UserQuestion.new(question_params)
          if @questions.save
            if params[:question][:last]
              create_user_course_assessment
              render json:  @user_assessment,
                     serializer: UserAssessmentSerializer,
                     'status' => success('Quiz Result Saved Successfully'),
                     meta_key: 'status'
              @user_assessment.remove_assessment if params[:question][:preview]
            else
              render json: { 'status' => success }
            end
          else
            render json: { 'status' => failure }, status: 400
          end
        end

        private

        def user_objective_questions_params
          params.require(:question).permit(
            :question_id, :question_type_id, :passing_status, :assessment_id,
            :assessment_type, :user_course_id, objective_answers_attributes:
                                              [:answer_id, :is_correct_answer])
        end

        def user_descriptive_questions_params
          params.require(:question).permit(
            :question_id, :question_type_id, :passing_status, :assessment_id,
            :assessment_type, :user_course_id, user_descriptives_attributes:
                                             [:answer, :is_correct_answer])
        end

        def question_params
          question_category =
            QuestionCategory.find(question_type_params[:question_type_id])
          if OBJECTIVE_QUESTION_TYPE.include? question_category.name
            user_objective_questions_params
          else
            user_descriptive_questions_params
          end
        end

        def create_user_course_assessment
          @user_assessment = CourseTaking::UserCourseAssessment.create(
            user_assessment_params)
        end

        def user_assessment_params
          params.require(:question).permit(
            :assessment_id, :user_course_id)
        end

        def question_type_params
          params.require(:question).permit(:question_type_id)
        end
      end
    end
  end
end
