module CourseCreation
  module API
    module V1
      # Resource Controller
      class UserCoursesController < API::V1::BaseController
        before_action :user_course

        def show
          if @user_course
            render json: @user_course,
                   serializer: UserCourseSerializer,
                   'status' => success, meta_key: 'status'
          else
            render json: { status: record_not_found }
          end
        end

        def assessments
          render json:  @user_course,
                 serializer: UserCourseAssessmentsSerializer,
                 'status' => success, meta_key: 'status'
        end

        def sections
          section = CourseCreation::CourseSection.find(section_params)
          render json: @user_course,
                 serializer: UserChapterStateSerializer, section: section,
                 'status' => success, meta_key: 'status'
        end

        private

        def user_course_params
          if params[:user_course_id].blank?
            params.require(:id)
          else
            params.require(:user_course_id)
          end
        end

        def section_params
          params.require(:id)
        end

        def user_course
          @user_course = CourseTaking::UserCourse.find(user_course_params)
        end
      end
    end
  end
end
