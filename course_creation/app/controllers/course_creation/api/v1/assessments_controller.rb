module CourseCreation
  module API
    module V1
      # Assessment controller API
      class AssessmentsController < API::V1::BaseController
        before_action :set_assessment, only: :show

        def show
          render json: @assessment,
                 serializer: AssessmentSerializer,
                 'status' => success, meta_key: 'status'
        end

        def set_assessment
          @assessment = Assessment.find(assessment_params)
        end

        def assessment_params
          params.require(:id)
        end
      end
    end
  end
end
