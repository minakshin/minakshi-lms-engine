module CourseCreation
  module API
    module V1
      # Course Sections API controller
      class CourseSectionsController < API::V1::BaseController
        before_action :set_course_section, only: :show
        def show
          render json: @course_section,
                 'status' => success, meta_key: 'status'
        end

        private

        def set_course_section
          @course_section = CourseSection.find(course_section_params)
        end

        def course_section_params
          params.require(:id)
        end
      end
    end
  end
end
