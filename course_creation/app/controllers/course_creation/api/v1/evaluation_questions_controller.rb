module CourseCreation
  module API
    module V1
      # EvaluationQuestions Controller for API
      class EvaluationQuestionsController < API::V1::BaseController
        before_action :user_course, only: :create
        before_action :user_evaluation, only: :create

        def create
          if @user_evaluation.save
            render json: { status: success('Data saved successfully') }
          else
            render json: { 'status' => failure }
          end
        end

        private

        def user_evaluation
          @user_evaluation =
            if @user_course.user_course_evaluation.present?
              @user_course.user_course_evaluation.assign_attributes(
                evaluation_question_params)
              @user_course.user_course_evaluation
            else
              CourseTaking::UserCourseEvaluation.create(
                evaluation_question_params)
            end
          @user_evaluation.assign_attributes(
            version_id: @user_course.version_id)
        end

        def evaluation_question_params
          params.require(:user_course_evaluation).permit(
            :user_course_id,
            evaluation_questions_attributes: [:answer_id,
                                              :evaluation_question_id])
        end

        def user_course
          @user_course = CourseTaking::UserCourse.find(
            params[:user_course_evaluation][:user_course_id])
        end
      end
    end
  end
end
