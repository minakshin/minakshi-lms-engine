module CourseCreation
  module API
    module V1
      # API Base Controller
      class BaseController < ApplicationController
        include CourseCreation::ApiStatuses
        rescue_from ActiveRecord::RecordNotFound, with: :respond_with_not_found
        rescue_from ActionController::ParameterMissing,
                    with: :respond_with_parameter_missing
        rescue_from ActiveRecord::InvalidForeignKey,
                    with: :respond_with_foreign_key_constraint

        private

        def respond_with_not_found
          render json: { status: record_not_found }, status: 404
        end

        def respond_with_parameter_missing
          render json: { status: params_missing }, status: 402
        end

        def respond_with_foreign_key_constraint
          render json: { status: foreign_key_constraint }
        end

        def default_serializer_options
          { root: 'data' }
        end
      end
    end
  end
end
