module CourseCreation
  module API
    module V1
      # Course controller API
      class VersionsController < API::V1::BaseController
        before_action :version, only: :show

        def show
          render json: @version,
                 serializer: VersionSerializer,
                 'status' => success, meta_key: 'status'
        end

        private

        def version
          @version = Version.find(version_params)
        end

        def version_params
          params.require(:id)
        end
      end
    end
  end
end
