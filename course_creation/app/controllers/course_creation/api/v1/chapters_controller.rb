module CourseCreation
  module API
    module V1
      # Course Sections API controller
      class ChaptersController < API::V1::BaseController
        before_action :chapter
        def show
          if @chapter && @chapter.parent_id.nil?
            render json: @chapter,
                   serializer: serializer,
                   'status' => success, meta_key: 'status'
          else
            render json: { status: record_not_found }
          end
        end

        private

        def chapter_params
          params.require(:id)
        end

        def chapter
          @chapter = CourseSection.find(chapter_params)
        end

        def serializer
          return ChapterSerializer unless params[:type].present?
          ChapterResourceSerializer
        end
      end
    end
  end
end
