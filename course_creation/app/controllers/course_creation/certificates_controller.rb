require_dependency 'course_creation/application_controller'

module CourseCreation
  # Certificate controller
  class CertificatesController < ApplicationController
    layout 'course_creation/fullpage'
    before_action :find_certificate, except: [:index, :new, :create]
    before_action :find_published_courses, except: [:index, :show, :destroy]

    def index
      @certificates = Certificate.all
    end

    def new
      @certificate = Certificate.new
    end

    def create
      @certificate = Certificate.new(certificate_params)
      if @certificate.save
        redirect_to certificates_path
      else
        render :new
      end
    end

    def edit
    end

    def update
      # Disassociate certificate with courses
      @certificate.courses.update_all(certificate_id: nil)
      @certificate.reload
      if @certificate.update_attributes(certificate_params)
        redirect_to certificates_path
      else
        render :edit
      end
    end

    def destroy
      if @certificate.course_ids.present? && @certificate.certification_taken?
        redirect_to certificates_path
        set_flash_message :danger, :message
      else
        @certificate.destroy
        redirect_to certificates_path
        set_flash_message :success, :delete_certificate
      end
    end

    private

    def find_certificate
      @certificate = Certificate.find(params[:id])
    end

    def find_published_courses
      @courses = Course.published_versions
    end

    def certificate_params
      params.require(:certificate).permit(:title, :description, :file,
                                          course_ids: [])
    end
  end
end
