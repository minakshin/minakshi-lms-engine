require_dependency 'course_creation/application_controller'
module CourseCreation
  # Courses controller
  class CoursesController < CourseCreation::BaseController
    before_action :set_course, only: [:show, :edit, :update, :destroy]
    before_action :course_categories, only: [:edit, :new, :create]
    before_action :version_roles, only: [:edit, :new, :create]

    # GET /courses
    def index
      @courses = Course.all
    end

    # GET /courses/1
    def show
    end

    # GET /courses/new
    def new
      @course = Course.new
      @version = @course.versions.build
    end

    # GET /courses/1/edit
    def edit
      @version = @course.versions.last
      @version_roles = @version.version_roles
    end

    # POST /courses
    def create
      @course = Course.new(course_params)
      # version_user_roles_params hash contains version id
      if @course.save && VersionRole.create(version_user_roles_params)
        set_flash_message :success, :success
        redirect_to syllabus_version_path(@course.versions.first)
      else
        @user_role_ids = role_params
        respond_to do |format|
          format.json do
            render json: { status: false,
                           notice: @course.errors.messages[:title].first }
          end
          format.html { render :new }
        end
      end
    end

    # PATCH/PUT /courses/1
    def update
      version = @course.versions.last
      if @course.update(course_params) && version.update_roles(
        version_user_roles_params)
        set_flash_message :notice, :update
        redirect_to syllabus_version_path(@course.versions.last)
      else
        redirect_to edit_course_path
      end
    end

    # DELETE /courses/1
    def destroy
      if @course
        @course.destroy
        set_flash_message :notice, :destroy
      else
        set_flash_message :notice, :failure
      end
      redirect_to courses_url
    end

    private

    # Use callbacks to share common setup or constraints between actions.
    def set_course
      @course = Course.find(params[:id])
    end

    # Only allow a trusted parameter "white list" through.
    def course_params
      params.require(:course).permit(:id, :title, versions_attributes: [
        :id, :description, :image, :video, :category_id, :default_image,
        :expiry, :price])
    end

    # fetch categories for courses
    def course_categories
      @categories = Category.order('name')
    end

    # fetch roles for courses
    def version_roles
      @roles = Role.order('name')
    end

    # create hash of user roles for version
    def version_user_roles_params
      version_user_roles = []
      if params[:role_id].present?
        params[:role_id].each do |role_id|
          version_roles_hash = { role_id: role_id,
                                 version_id: @course.versions.last.id }
          version_user_roles.push(version_roles_hash)
        end
      end
      version_user_roles
    end

    def role_params
      params.fetch(:role_id, '')
    end
  end
end
