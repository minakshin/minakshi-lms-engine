require_dependency 'course_creation/application_controller'
module CourseCreation
  # resource controller
  class ResourcesController < CourseCreation::BaseController
    before_action :set_resource, except: [:index, :new, :create]
    before_action :set_type
    before_action :set_version
    before_action :set_section, if: :upload?, except: :index
    before_action :set_chapters, only: [:index, :create, :update]

    def index
    end

    def new
      @resource = resource_class.new(section_params)
      respond_to do |format|
        format.js { render file: '/course_creation/resources/new_content.js.coffee' }
        format.html
      end
    end

    def create
      @resource = resource_class.new(resource_params)
      if @resource.save
        @resource.sectionize_content(
          foreign_key: :content_section_id) if syllabus?
        respond_to do |format|
          format.json do
            render json: { status: true,
                           version_id: params[:version_id] }
          end
          format.js { render file: '/course_creation/course_sections/syllabus.js.coffee' }
          set_flash_message :success, :create
          format.html { redirect_to resources_redirect_path }
        end
      elsif syllabus?
        render_notice(@resource.errors.full_messages.join(', '))
      else
        respond_to do |format|
          format.json do
            render json: { status: false,
                           error: @resource.errors.messages[:base] }
          end
          format.html { render :new }
        end
      end
    end

    def edit
      respond_to do |format|
        format.js { render file: '/course_creation/resources/new_content.js.coffee' }
        format.html
      end
    end

    def update
      if @resource.update_attributes(resource_params)
        @resource.update_section if syllabus?
        respond_to do |format|
          format.json { render json: { resource: @resource, status: 200 } }
          set_flash_message :success, :update
          format.js { render file: '/course_creation/course_sections/syllabus.js.coffee' }
          format.html { redirect_to resources_redirect_path }
        end
      elsif syllabus?
        render_notice(@resource.errors.full_messages.join(', '))
      else
        respond_to do |format|
          format.json { render json: { status: 400 } }
          format.html { render :edit, notice: :error }
        end
      end
    end

    # update resource details on ajax call.
    def update_details
      if @resource.update_attributes(update_params)
        render json: { resource: @resource, status: 200 }
      else
        render json: { status: 400 }
      end
    end

    def destroy
      if @resource.destroy
        set_flash_message :success, :success_delete
        status = 200
      else
        set_flash_message :danger, :error_delete
        status = 400
      end
      respond_to do |format|
        format.json { render json: { status: status } }
        format.html { redirect_to resources_redirect_path }
      end
    end

    private

    def set_type
      @type = Resource.valid_type?(params[:type]) ? params[:type] : 'Resource'
    end

    def resource_class
      ("CourseCreation::#{@type}").constantize
    end

    def resource_params
      params.require(set_type.downcase).permit(:file, :title,
                                               :type, :content,
                                               :version_id,
                                               :chapter_order_position,
                                               :version_order_position,
                                               :description,
                                               :course_section_id)
    end

    def update_params
      params.require(set_type.downcase).permit(:file, :title, :type, :content,
                                               :version_id, :course_section_id)
    end

    def section_params
      params.permit(:course_section_id)
    end

    def set_version
      @version = Version.find(params[:version_id])
    end

    def set_section
      @section = CourseSection.find(params[:course_section_id])
    end

    def set_resource
      @resource = Resource.find(params[:id])
    end

    def set_chapters
      @chapters = @version.chapters
    end

    def upload?
      Resource.upload?(params[:type])
    end

    def syllabus?
      params[:syllabus] || @resource.try(:content_section_id)
    end

    def resources_redirect_path
      return syllabus_version_path(@version) if syllabus?
      send("version_#{@type.downcase.pluralize}_path", @version)
    end
  end
end
