require_dependency 'course_creation/application_controller'
module CourseCreation
  # question_answers controller
  class QuestionsController < CourseCreation::BaseController
    before_action :assessment_information, only: [:index, :new, :edit, :create]
    before_action :question_information, only: [:edit, :update]
    before_action :question_categories, only: [:new, :create]
    def index
      @questions = @assessment.questions.includes(:question_category).rank(
        :question_order).page(params[:page]).per(10)
      @total_questions = @assessment.questions.count
    end

    def new
      @question = Question.new
      @answers = @question.answers.build
      @version = @assessment.course_section.version
    end

    # POST /courses
    def create
      @question = Question.new(question_params)
      if @question.save
        set_flash_message :notice, :success
        redirect_to assessment_questions_path(params[:question][:assessment_id])
      else
        render :new
      end
    end

    # DELETE /questions/1
    def destroy
      @question = Question.find(params[:id])
      assessment_id = @question.assessment.id
      if @question
        @question.try(:destroy)
        set_flash_message :notice, :destroy
      else
        set_flash_message :notice, :failure
      end
      redirect_to assessment_questions_path(assessment_id)
    end

    # GET /questions/1/edit
    def edit
      @questioncategories = QuestionCategory.order(:name)
      @answers = @question.answers
      @version = @assessment.course_section.version
    end

    def update_position
      @question = Question.find(params[:question_id])
      if @question && @question.update(position_params)
        render json: { status: true }
      else
        render json: { status: false }
      end
    end

    # PATCH/PUT /questions/1
    def update
      if @question.update(question_params)
        set_flash_message :notice, :update
      else
        set_flash_message :danger, :error
      end
      redirect_to assessment_questions_path(params[:assessment_id])
    end

    private

    def assessment_information
      @assessment = Assessment.includes(:course_section).find_by_id(params[:assessment_id])
      @version = @assessment.course_section.version
    end

    def position_params
      params.require(:question).permit(:question_order_position)
    end

    def question_information
      @question = Question.find(params[:id])
    end

    def question_params
      json_question_type = if params[:action] == 'update'
                             [:match, :sort]
                           else
                             [:match, :sort, :multiple]
                           end
      json_question_type.each do |questiontype|
        if params[:question][questiontype].present?
          params[:question][:answers_attributes] =
            params[:question][questiontype]
        end
      end
      params.require(:question).permit(
        :id, :title, :question_category_id, :assessment_id, answers_attributes:
        [:id, :option, :correct_answer, :image])
    end

    def question_categories
      @questioncategories = QuestionCategory.order(:name)
    end
  end
end
