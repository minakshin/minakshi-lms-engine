require_dependency 'course_creation/application_controller'
module CourseCreation
  # Categories controller
  class CategoriesController < CourseCreation::BaseController
    layout 'course_creation/fullpage'
    before_action :set_category, only: [:show, :edit, :update, :destroy]

    # GET /categories
    def index
      @categories = Category.all
    end

    # GET /categories/1
    def show
    end

    # GET /categories/new
    def new
      @category = Category.new
    end

    # GET /categories/1/edit
    def edit
    end

    def create
      @category = Category.new(category_params)
      if @category.save
        respond_to do |format|
          format.json do
            render json: { status: true, id: @category.id,
                           name: @category.name,
                           notice: t('course_creation.categories.success') }
          end
          format.html do
            redirect_to categories_path, flash: { notice: t(
              'course_creation.categories.success') }
          end
        end
      else
        respond_to do |format|
          format.json do
            render json: { status: false,
                           notice: @category.errors.messages[:name].first }
          end
          format.html { render :new }
        end
      end
    end

    # PATCH/PUT /categories/1
    def update
      if @category.update(category_params)
        set_flash_message :success, :update
      else
        set_flash_message :danger, :save_error
      end
      redirect_to categories_path
    end

    # DELETE /categories/1
    def destroy
      if @category
        @category.destroy
        set_flash_message :notice, :destroy
      else
        set_flash_message :notice, :failure
      end
      redirect_to categories_url
    end

    private

    # Use callbacks to share common setup or constraints between actions.
    def set_category
      @category = Category.find(params[:id])
    end

    # Only allow a trusted parameter "white list" through.
    def category_params
      params.require(:category).permit(:name)
    end

    def translation_scope
      'course_creation.categories'
    end
  end
end
