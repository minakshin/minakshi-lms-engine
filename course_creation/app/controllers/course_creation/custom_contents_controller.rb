require_dependency 'course_creation/application_controller'
require 'zip'
module CourseCreation
  # customcontents controller
  class CustomContentsController < CourseCreation::BaseController
    before_action :set_customcontent, only: [:show, :edit, :update, :destroy]
    before_action :set_version, except: [:show]
    # GET /customcontents
    def index
      @customcontents = CustomContent.all
    end

    # GET /customcontents/new
    def new
      @customcontent = CustomContent.new
    end

    # Post /customcontents
    def create
      @customcontent = CustomContent.new(customcontent_params)
      @customcontent.course_section_id = params[:course_section_id]
      if @customcontent.save && @customcontent.unzip(customcontent_params)
        @customcontent.sectionize_content
        set_flash_message :success, :success
        redirect_to syllabus_version_path(@version)
      else
        FileUtils.rm_rf(@customcontent.zip.path[0..-5])
        @customcontent.destroy!
        set_flash_message :danger, :indexpage
        render :new
      end
    end

    # GET /customcontents/1/edit
    def edit
    end

    # GET /customcontents/1/
    def show
      path = @customcontent.zip.file.file
      send_file "#{path}", type: 'application/zip', x_sendfile: true
    end

    # PATCH/PUT /customcontents/1
    def update
      if @customcontent.checkzip(customcontent_params[:zip])
        @customcontent.update(customcontent_params)
        dest = File.dirname(@customcontent.zip.path)
        S3_BUCKET &&
          @customcontent.delete_aws_directory &&
          @customcontent.upload_aws(dest)
        @customcontent.update_section
        set_flash_message :success, :update
        redirect_to syllabus_version_path(@version)
      else
        set_flash_message :danger, :indexpage
        render :edit
      end
    end

    # DELETE /customcontent/1
    def destroy
      if @customcontent
        @customcontent.destroy!
        set_flash_message :success, :destroy
      else
        set_flash_message :danger, :failure
      end
      redirect_to version_course_section_custom_contents_path
    end

    private

    # Use callbacks to share common setup or constraints between actions.
    def set_customcontent
      @customcontent = CustomContent.find(params[:id])
    end

    # Only allow a trusted parameter "white list" through.
    def customcontent_params
      params.require(:custom_content).permit(:name, :zip)
    end

    def set_version
      @version = Version.find(params[:version_id])
    end
  end
end
