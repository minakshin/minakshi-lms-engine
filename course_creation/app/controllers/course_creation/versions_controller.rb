require_dependency 'course_creation/application_controller'
module CourseCreation
  # versions controller
  class VersionsController < CourseCreation::BaseController
    layout 'course_creation/fullpage',
           only: [:index]
    before_action :set_version, only: [:show, :edit, :update,
                                       :destroy, :publish, :preview]

    # GET /versions
    def index
      @versions = Version.all
      if !params[:search].present?
        @versions = Version.list_courses params[:page]
      else
        @versions = Version.search_courses params[:page], params[:search]
      end
    end

    # GET /versions/1
    def show
      @course = @version.course
      @chapter = CourseSection.new
      @chapters = @version.chapters
    end

    # GET /versions/new
    def new
      @version = Version.new
    end

    # GET /versions/1/edit
    def edit
      @chapters = @version.chapters
    end

    # POST /versions
    def create
      @version = Version.new(version_params)
      if @version.save
        set_flash_message :notice, :success
        redirect_to version_path
      else
        render :new
      end
    end

    # PATCH/PUT /versions/1
    def update
      if @version.update(version_params)
        set_flash_message :notice, :update
        redirect_to version_path
      else
        render :edit
      end
    end

    # DELETE /versions/1
    def destroy
      if @version.published
        set_flash_message :danger, :failure
      else
        @version.try(:destroy) && set_flash_message(:notice, :destroy)
      end
      redirect_to versions_path
    end

    def publish
      publish = @version.published
      publish_result = @version.publishable?
      if publish_result
        publish = !@version.published
        @version.update_attributes(published: publish)
      end
      render json: { validate_publish: publish_result,
                     published: publish }
    end

    def preview
      preview_user_id = { user_id: CourseTaking::UserCourse.preview_user_id }
      preview = @version.user_courses.find_or_create_by(preview_user_id)
      redirect_to course_taking_preview_path(course_id: preview.id)
    end

    private

    # Use callbacks to share common setup or constraints between actions.
    def set_version
      @version = Version.find(params[:id])
    end

    # Only allow a trusted parameter "white list" through.
    def version_params
      params.require(:version).permit(:description, :image, :video,
                                      :expiry, :prerequisite,
                                      :course_id, :category_id)
    end
  end
end
