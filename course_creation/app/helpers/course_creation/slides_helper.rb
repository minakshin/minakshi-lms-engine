module CourseCreation
  # Slides Helper
  module SlidesHelper
    # set active css class to slide.
    def active_slide_class(id)
      @slide.id == id ? 'active-slide' : ''
    end

    # set background style for slide.
    def set_style
      if background_img_exist?
        set_background_img
      else
        set_background_color
      end
    end

    # return background color.
    def set_background_color
      "background-color: #{@slide_setting.background_color}"
    end

    # check background image exist for slide.
    def background_img_exist?
      @slide_setting.background_img.path.present?
    end

    # return background image path.
    def set_background_img
      "background-image: url(#{@slide_setting.background_img})"
    end

    # return background image file name.
    def background_img_file_name
      File.basename(@slide_setting.background_img.path) if background_img_exist?
    end

    # display image title div if img exist
    def bg_img_title_div
      background_img_exist? || 'hidden-area'
    end

    # apply active class for select transition.
    def active_transition(transistion)
      @slide_setting.transition.eql?(transistion) ? 'active-transition' : ''
    end

    # return slide title.
    def slide_title
      @slide.title && @slide.title.html_safe
    end

    # check content exist else display menu
    def content_exist(column)
      column.type.blank? ? '' : 'hidden-area'
    end

    # check image content exist
    def img_content_exist(column)
      column.type.eql?('Image') ? '' : 'hidden-area'
    end

    # check text content exist
    def text_content_exist(column)
      column.type.eql?('Text') ? '' : 'hidden-area'
    end

    # check video content exist
    def video_content_exist(column)
      column.type.eql?('Video') ? '' : 'hidden-area'
    end

    # file path
    def file_path(column)
      column.file_url.url || ''
    end

    # return column content
    def column_content(column)
      column.content || ''
    end

    def slide_title_class
      'updated-slide-title' if slide_title.present?
    end
  end
end
