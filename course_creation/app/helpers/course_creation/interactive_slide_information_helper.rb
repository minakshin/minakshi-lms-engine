module CourseCreation
  # InteractiveSlideInformation Helper
  module InteractiveSlideInformationHelper
    def type_render
      "interactive_#{@type.underscore}"
    end

    def new_action_form
      send("interactive_slide_#{@type.underscore.pluralize}_path")
    end

    def course_name
      @version.course.title
    end

    def section_name
      CourseSection.find_by_id(params[:course_section_id]).chapter.name
    end
  end
end
