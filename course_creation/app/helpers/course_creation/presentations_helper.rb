module CourseCreation
  # Presentations Helper
  module PresentationsHelper
    def chapter_name
      @chapter_name || @course_section.name
    end
  end
end
