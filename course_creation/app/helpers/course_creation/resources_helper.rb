module CourseCreation
  # Resource Helper
  module ResourcesHelper
    # return form action to create resource.
    def create_form_action(type)
      return version_course_section_videos_path if
        type.eql?(Resource::TYPES[:video])
      version_course_section_documents_path
    end

    # return form action to update resource.
    def update_form_action(type)
      return version_course_section_video_path if
        type.eql?(Resource::TYPES[:video])
      version_course_section_document_path
    end

    def resource_partial(chapters, version)
      case params[:type]
      when 'Video', 'Document'
        render partial: 'course_creation/resources/upload_resources',
               locals: { chapters: chapters,
                         resource_type: params[:type].downcase }
      when 'Quote', 'Note'
        render partial: 'course_creation/resources/text_resources',
               locals: { version: version,
                         resource_type: params[:type].downcase }
      end
    end

    # return input field name depending on type of resource.
    def file_field_name(type)
      return 'video[file]' if type.eql?(Resource::TYPES[:video])
      'document[file]'
    end

    # return name of file.
    def resource_file_name(path)
      File.basename(path)
    end

    # return heading name depending on resource type.
    def heading_name(type)
      return t('course_creation.add_video_title') if
        type.eql?(Resource::TYPES[:video])
      t('course_creation.add_pdf_title')
    end

    def resource_cs_label(resource)
      "#{sub_type(resource.type)}: #{resource.title}"
    end

    def supported_formats_msg(type)
      return t('course_creation.video_note') if
        type.eql?(Resource::TYPES[:video])
      t('course_creation.pdf_note')
    end

    def description_label
      return if @type.eql? sub_type(Resource::TYPES[:document])
      label_tag :description, localize(:description_title),
                class: 'chapter-name'
    end

    def description_field
      return :hidden_field if @type.eql? sub_type(Resource::TYPES[:document])
      :text_area
    end

    def description_tag
      return if @type.eql? sub_type(Resource::TYPES[:document])
      text_area_tag :description, '', class: 'form-control tinymce'
    end

    def resource_title
      if @type.eql?(sub_type(Resource::TYPES[:document]))
        localize(:document_title)
      else
        localize(:video_title)
      end
    end

    def text_resources_path
      send("version_#{@type.underscore.pluralize.downcase}_path", @version)
    end

    def resources_error(errors, input)
      return if errors.empty?
      errors.full_messages_for(input).join(', ')
    end

    def chapter_tag(section)
      return if section.section?
      tag(:div, { id: 'chapter-content-form',
                  data: { chapter_id: section.id } } )
    end

    private

    def sub_type(type)
      type.sub('CourseCreation::', '')
    end
  end
end
