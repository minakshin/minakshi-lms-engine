old_content = $('#course_section_' + <%= @course_section.id.to_s %>).html()
$('#course_section_' + <%= @course_section.id.to_s %>).empty().append("<%= j render(partial: 'course_creation/course_sections/form', locals: { version: @version, course_section: @course_section, chapter: false } ) %>")
$('.cancel-edit').click ->
  $('#course_section_' + <%= @course_section.id.to_s %>).empty().append(old_content)
  contentClick('.add-content[data-cs-id="' + <%= @course_section.id.to_s %> + '"]')
