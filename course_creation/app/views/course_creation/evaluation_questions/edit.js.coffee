$(".evaluation-question[data-question-id=#{<%= @question.id.to_s %>}]")
  .empty()
    .append("<%= j render(
                           partial: 'course_creation/evaluation_questions/form',
                           locals: { version: @version, question: @question }
                          )
              %>")
