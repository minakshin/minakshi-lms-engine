// This is a manifest file that'll be compiled into application.js, which will include all the files
// listed below.
//
// Any JavaScript/Coffee file within this directory, lib/assets/javascripts, vendor/assets/javascripts,
// or any plugin's vendor/assets/javascripts directory can be referenced here using a relative path.
//
// It's not advisable to add code directly here, but if you do, it'll appear at the bottom of the
// compiled file.
//
// Read Sprockets README (https://github.com/rails/sprockets#sprockets-directives) for details
// about supported directives.
//
//= require jquery
//= require jquery_ujs
//= require jquery-ui
//= require js-routes
//= require jquery.remotipart
//= require course_creation/jquery.fileupload
//= require course_creation/common
//= require course_creation/jquery.validate.min
//= require course_creation/additional-methods.min
//= require course_creation/lms
//= require course_creation/course_taking/course_taking
//= require_tree .
