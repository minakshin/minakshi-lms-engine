//= require 'course_creation/multiselect.min'
$ ->
  $('#courses').multiselect()

  if  $('.disable_checkbox').length
    $('input[type=checkbox]').attr('disabled', 'disabled')

    $('.ui-helper-reset').on 'click', ->
      return false
