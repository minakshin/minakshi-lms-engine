#= require 'course_creation/jquery.fileupload'
#= require underscore
$ ->
  resource = undefined
  _.templateSettings = interpolate: /\{\{(.+?)\}\}/g
  window.ResourceUpload =
    viewElements:
      formElement: $('#resource_form')
      fileElement: $('#resource_file')
      divElement: $('.videos')
      elementType: $('#resource_form').data('type')
      types: {video: 'CourseCreation::Video',    document: 'CourseCreation::Document'}
      actions: $('.actions')

    init: ->
      resource = @viewElements
      if $('#resource_id').length
        resource.resourceTemplate = _.template $('#resource_id').html()
      @bindDynamicUIActions()
    bindDynamicUIActions: ->
      # resource upload using jquery-fileupload.
      queueFiles = {}
      uploadedFiles = 0
      $('#resource_file').fileupload
        add: (e, data) ->
          if ResourceUpload.resourceValidate(data.files[0], resource)
            if resource.elementType is resource.types['document']
              ResourceUpload.show_error('#invalidPdf')
            else
              ResourceUpload.show_error('#invalidVideo')
          else
            ResourceUpload.hide_error('#invalidPdf')
            ResourceUpload.hide_error('#invalidVideo')
            resource.actions.show()
            count = ResourceUpload.elements().length + 1
            $('.actions').show()
            queueFiles["resource_#{count}"] = data
            html = resource.resourceTemplate({count: count, name: data.files[0].name})
            resource.divElement.append(html)
        # Update resource details
        $('body').on 'click', 'a.add-video-details', (e) ->
          if ResourceUpload.validateTitles()
            ResourceUpload.hide_error('.error')
            loader true
            for id, resourceForm of queueFiles
              resourceForm = ResourceUpload.setAttributes(id, resource, resourceForm)
              resourceForm.submit()
        done: (e, data) ->
          if data.result.status
            route_type = resource.elementType.substring(16)
            uploadedFiles++
            if uploadedFiles == Object.keys(queueFiles).length
              location.href = Routes.course_creation_version_resources_path(data.result.version_id, type: route_type)
          else
            loader false
            $('#error_msg').text(data.result.error[0])
            ResourceUpload.show_error('#error_msg')
      # delete resource
      $('body').on 'click', 'a.remove-video', (e) ->
        removedFile = "resource_#{$(e.target).data('resource')}"
        delete queueFiles[removedFile]
        $("##{removedFile}").remove()
        unless $('.videos .video-detail-wrapper').length
          resource.actions.hide()
    # validate resource type
    resourceValidate: (file, resource) ->
      if resource.elementType is resource.types['document']
        return $.inArray(file.type, ['application/pdf', 'application/msword',
                                      'application/vnd.openxmlformats-officedocument.wordprocessingml.document']) is -1 &&
                                      ResourceUpload.extensionValidate(file, ['pdf', 'doc', 'docx'])
      else
        return ($.inArray(file.type, ['video/mp4']) is -1) || ResourceUpload.extensionValidate(file, ['mp4'])
    extensionValidate:(file, extensions) ->
      return $.inArray(file.name.split('.').pop(), extensions) is -1
    # show error message
    show_error: (el) ->
      $(el).show()
      $('.error').show()
      $('#file-upload #title').focusin ->
        $('#emptyTitle').fadeOut 2000
    # hide error message
    hide_error: (el) ->
      $(el).hide()
    # validates the all the title of resources
    validateTitles: ->
      ResourceUpload.hide_error('.error #emptyTitle')
      valid = true
      for resourceDiv, i in ResourceUpload.elements()
        unless $(resourceDiv).find('#title').val().length
          ResourceUpload.show_error($(resourceDiv).find('#emptyTitle'))
          valid = false
      valid
    # return all the div of uploaded resources
    elements: ->
      $('.videos .video-detail-wrapper')
    # set the title and description to form data.
    setAttributes: (id, resource, formData) ->
      title = $("##{id}").find('#title').val()
      description = $("##{id}").find('#description').val()
      if resource.elementType is resource.types['video']
        formData.form[0].video_title.value = title
        formData.form[0].video_description.value = description
      else
        formData.form[0].document_title.value = title
        formData.form[0].document_description.value = description
      formData
  ResourceUpload.init()

  $('.resource-list').sortable(
    items: '.resource'
    connectWith: '.resource-list'
    dropOnEmpty: true

    update: (e, ui) ->
      resource_id = ui.item.data('resource-id')
      chapter_object = ui.item.parent().parent()
      chapter_id = chapter_object.data('chapter-id')
      version_id = $('#resource-info').data('version-id')
      type = $('#resource-info').data('type')
      position = ui.item.index()
      if type is 'Quote' or type is 'Note'
        resource_url = Routes.course_creation_version_resource_path(version_id, resource_id)
        resource_data = { resource: { version_order_position: position } }
      else
        resource_url = Routes.course_creation_version_course_section_resource_path(version_id, chapter_id, resource_id)
        resource_data = { resource: { course_section_id: chapter_id, chapter_order_position: position } }
      $.ajax(
        type: 'PUT'
        url: resource_url
        dataType: 'json'
        data: resource_data
      )
  )

  $('.edit_document, .edit_video').on 'submit', ->
    loader true
    return

  #Client side validation for custom_resource (video and pdf)
  validateTitle = ->
    required: true
    minlength: 4
    messages: required: $('.submit-resource').data('name_message')

  $('body').on 'click', '.submit-resource', (e) ->
    if ($('body #new_video').length)
      $('body #new_video').validate submitHandler: (form) ->
        loader true
      $('#video_file').rules 'add',
        required: true
        extension: 'mp4'
        messages:
          required: $('.submit-resource').data('upload-mp4')
          extension: $('.submit-resource').data('invalid-content')
      $('#video_title').rules 'add',
        validateTitle()
    else
      $('body .document-content').validate submitHandler: (form) ->
        loader true
      $('#document_file').rules 'add',
        required: true
        extension: 'pdf'
        messages:
          required: $('.submit-resource').data('upload-pdf')
          extension: $('.submit-resource').data('invalid-content')
      $('#document_title').rules 'add',
        validateTitle()
