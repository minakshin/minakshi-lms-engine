'use strict'

angular.module('CourseTakingApp').directive 'ctDisableSeeking', ->
  (scope, ele) ->
    unless  scope.$preview()
      maxPlayPosition = 0.0
      seeking = false
      player = jwplayer(ele.attr('id'))

      player.onTime (e)->
        if !seeking
          maxPlayPosition = e.position

      player.onSeek (e)->
        if e.offset > maxPlayPosition
          seeking = true
          setTimeout((=> player.seek(maxPlayPosition)), 10)
        else
          seeking = false
