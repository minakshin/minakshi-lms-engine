'use strict'

angular.module('CourseTakingApp').directive 'ctCustomRadio', ->
  (scope, ele) ->
    element = angular.element(ele)
    element.click (e) ->
      if !element.next().hasClass('disabled')
        element.parents('.radiobox-main-block').find('.custom-radiobox').removeClass('active')
        element.next().toggleClass('active')
      false
    return
