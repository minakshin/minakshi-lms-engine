'use strict'

angular.module('CourseTakingApp').directive 'ctPdfViewer', ->
  (scope, ele) ->
    pdf = angular.element(".#{ele.attr('class')}")
    pdf.on 'scroll', ->
      if (pdf.scrollTop() + pdf.innerHeight() >= pdf[0].scrollHeight) and (scope.pageNum != scope.pageCount)
        scope.goNext()
        angular.element('#pageNumber').val(scope.pageNum)
        pdf.scrollTop(1)
      if (pdf.scrollTop() == 0) and (scope.pageNum != 1)
        scope.goPrevious()
        angular.element('#pageNumber').val(scope.pageNum)
        pdf.scrollTop(pdf.innerHeight())
