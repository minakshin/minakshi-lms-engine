'use strict'

angular.module('CourseTakingApp').directive 'ctAnimateContent', [
  '$timeout'
  '$rootScope'
  'CONTENT_TYPES'
  'Loader'
  ($timeout, $rootScope, CONTENT_TYPES, Loader) ->
    (scope, ele) ->
      element = angular.element(ele)
      element.click (e) ->
        if !element.hasClass('disabled')
          if scope.currentContent && scope.currentContent.content_type == CONTENT_TYPES.PRESENTATION
            return
          else
            cta.slideOut()
            $timeout (->
              cta.slideIn()
              Loader.hide()
            ), 1500
        else
          Loader.hide()
      return
]
