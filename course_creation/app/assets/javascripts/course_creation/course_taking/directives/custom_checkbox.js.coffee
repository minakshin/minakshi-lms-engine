'use strict'

angular.module('CourseTakingApp').directive 'ctCustomCheckbox', ->
  (scope, ele) ->
    element = angular.element(ele)
    element.click (e) ->
      element.next().toggleClass('active')
      if element.next().hasClass('active')
        angular.element(element).attr('checked', 'checked')
      else
        angular.element(element).removeAttr('checked')
    return
