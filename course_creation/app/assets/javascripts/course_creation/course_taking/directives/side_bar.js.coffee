'use strict'

angular.module('CourseTakingApp').directive 'ctSideBar', ->
  (scope, ele) ->
    element = angular.element(ele)
    sideOuter = element.parents('.sideOuter')
    element.click (e) ->
      isActive = sideOuter.hasClass('active')
      leftNav = sideOuter.hasClass('course-taking-left-nav')
      if angular.element('#video-content').length
        jwplayer('video-content').pause(!isActive)
      sideOuter.toggleClass('active')
      if leftNav
        angular.element('.course-taking-content').toggleClass('slide-right-content')
      false
    return
