'use strict'

angular.module('CourseTakingApp').directive 'ctSubMenu', ->
  (scope, ele) ->
    element = angular.element(ele)
    logOutBlock = element.parents().hasClass('logout-wrap')
    element.click (e) ->
      isActive = if element.hasClass('active') then true else false
      angular.element('.vertical-left-menu').find('.chapter').removeClass('active')
      angular.element('.chapter-menu').slideUp 'fast'
      if !isActive
        element.addClass('active').next().slideToggle 'fast'
      if logOutBlock
        element.removeClass('active')
      false
    return
