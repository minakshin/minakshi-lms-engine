((cta) ->
  'use strict'
  cta.isFisrtTime = true

  cta.setScreenHeight = ->
    windowHeight = angular.element(window).height()
    headerHeight = angular.element('header').outerHeight()
    footerHeight = angular.element('footer').outerHeight()
    leftNavIconHeight = angular.element('#showLeftNav').outerHeight(true)
    contentHeight = angular.element('.course-taking-content').outerHeight()
    marginTopContent = (windowHeight - contentHeight) / 2
    angular.element('.course-taking-content').css 'margin-top', marginTopContent
    angular.element('.ct-footer').show()
    if angular.element(window).width() <= 1024
      angular.element('.sideOuter').removeClass('active')
      angular.element('.course-taking-content').removeClass('slide-right-content')

  cta.iframeHeight = ->
    if screen.width <= 1024
      angular.element('#customContentIframe').load ->
        ifbody = angular.element(this).contents().find('body')
        ifbody.css 'height', 'auto'
        angular.element(this).height ifbody.height()
        return

  # slide animation effect
  cta.slideIn = ->
    angular.element('.course-content-block').css('right','-1500px')
    angular.element('.course-content-block').animate
      right: '0'
      , 1000

  cta.slideOut = ->
    angular.element('.course-content-block').css('right','0')
    angular.element('.course-content-block').animate
      right: '1500px'
      , 1000

  cta.presentationAnimation = (slideEffect, slideNo) ->
    if slideNo == 0 && cta.isFisrtTime
      cta.isFisrtTime = false
      cta.slideIn()
      setTimeout (->
        cta.presentationAnimate(slideEffect)
        return
      ), 1500
    else
      cta.presentationAnimate(slideEffect)

  cta.presentationAnimate = (slideEffect) ->
    switch slideEffect
      when 'slideLeft'
        angular.element('.presentation-wrapper').css('right','844px')
        angular.element('.presentation-wrapper').animate
          right: '0'
          , 1000
      when 'slideRight'
        angular.element('.presentation-wrapper').css('right','-844px')
        angular.element('.presentation-wrapper').animate
          right: '0'
          , 1000
      when 'slideBottom'
        angular.element('.presentation-wrapper').css('top','-525px')
        angular.element('.presentation-wrapper').animate
          top: '0'
          , 1000
      when 'slideTop'
        angular.element('.presentation-wrapper').css('top','525px')
        angular.element('.presentation-wrapper').animate
          top: '0'
          , 1000
      when 'slideBlur'
        angular.element('.presentation-wrapper').hide()
        angular.element('.presentation-wrapper').fadeIn(2000)

  cta.paginateScroll = ->
    paginateItemWidth = angular.element('.pagination').find('li').outerWidth(true)
    listCount = angular.element('.pagination').find('li').length

    if listCount <= 8
      angular.element('.pagination-next, .pagination-prev').addClass('disabled')
      angular.element('.pagination, .pagination ul').css({width: 'auto'})
      angular.element('.pagination ul').css({right: '0'})
    else
      angular.element('.pagination-next, .pagination-prev').removeClass('disabled')
      listWidth = paginateItemWidth * listCount
      angular.element('.pagination').find('ul').css({width: listWidth})
      PaginationlistWidth = angular.element('.pagination ul').outerWidth(true)
      paginateWidth = angular.element('.pagination').outerWidth(true) + angular.element('.pagination').offset().left
      paginateListRight = angular.element('.pagination').find('ul').css('right')
      diff = ( PaginationlistWidth - paginateWidth ) + 1
      angular.element('.pagination').css({width: '305px'})
      if angular.element('.pagination').find('.active').offset()
        activePosition = angular.element('.pagination').find('.active').offset().left
        if (angular.element('.pagination').outerWidth(true) + angular.element('.pagination').offset().left) < activePosition
          angular.element('.pagination').find('ul').animate({right: ((activePosition-(angular.element('.pagination').outerWidth(true) + angular.element('.pagination').offset().left))+paginateItemWidth)})
          if ((parseInt(paginateListRight) + paginateItemWidth) >= diff)
            angular.element('.pagination-next').addClass('disabled')
        else
          angular.element('.pagination-prev').addClass('disabled')
) window.cta = window.cta or {}

