'use strict'
angular.module('CourseTakingApp').constant 'FAILURE_MESSAGES',
  'WRONG': 'Something went wrong. Please try again.'
