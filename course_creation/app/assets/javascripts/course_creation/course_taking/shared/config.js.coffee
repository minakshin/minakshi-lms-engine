'use strict'

angular.module('CourseTakingApp').config [
  'restmodProvider'
  (restmodProvider) ->
    restmodProvider.rebase $config:
      style: 'DefaultPacker'
      isArray: true
    return
]

angular.module('CourseTakingApp').run [
  '$rootScope'
  '$state'
  ($rootScope, $state) ->
    $rootScope.$state = $state
    $rootScope.$preview = () ->
      $state.current.name is 'preview' ? true : false
]
