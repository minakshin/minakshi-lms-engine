'use strict'
angular.module('CourseTakingApp').constant 'ANSWER_VALUE',
  'STRONGLY_DISAGREE': 1
  'DISAGREE': 2
  'SOMEWHERE_AGREE': 3
  'AGREE': 4
  'STRONGLY_AGREE': 5
