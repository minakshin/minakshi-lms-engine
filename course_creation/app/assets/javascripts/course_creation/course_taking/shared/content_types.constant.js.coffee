'use strict'
angular.module('CourseTakingApp').constant 'CONTENT_TYPES',
  'PRESENTATION': 'Presentation'
  'DOCUMENT': 'Document'
  'VIDEO': 'Video'
  'ASSESSMENT': 'Assessment'
  'CUSTOMCONTENT': 'CustomContent'
  'INTERACTIVESLIDE': 'InteractiveSlide'
