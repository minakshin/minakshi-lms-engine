'use strict'
course_views = {
  'menuView': {
    templateUrl: '/course_creation/course_taking/templates/course.html',
    controller: 'CourseCtrl'
  },
  'contentView': {
    templateUrl: '/course_creation/course_taking/templates/course-content.html',
    controller: 'MainContentCtrl'
  },
  'resourceView': {
    templateUrl: '/course_creation/course_taking/templates/resources.html',
    controller: 'ResourcesCtrl'
  }
}

angular.module('CourseTakingApp').config [
  '$stateProvider'
  '$locationProvider'
  ($stateProvider, $locationProvider) ->
    $locationProvider.html5Mode
      enabled: true
      requireBase: false
    $stateProvider.state 'course',
      url: '/course_creation/course_taking/courses/:courseId'
      views: course_views
    .state 'preview',
      url: '/course_creation/course_taking/courses/:courseId/preview'
      views: course_views
]
