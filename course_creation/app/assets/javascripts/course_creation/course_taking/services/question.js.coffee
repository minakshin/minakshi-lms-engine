'use strict'

'use strict'

angular.module('CourseTakingApp').factory 'Question', [
  'restmod'
  'APP_CONSTANTS'
  (restmod, APP_CONSTANTS) ->
    return restmod.model(APP_CONSTANTS.apiURL + '/user_questions')
]
