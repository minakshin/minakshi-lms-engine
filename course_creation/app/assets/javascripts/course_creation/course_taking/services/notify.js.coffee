'use strict'
angular.module('CourseTakingApp').service 'notifyService', [
  'FAILURE_MESSAGES'
  (FAILURE_MESSAGES) ->

    commonStatueCode = [
      500
      502
      422
    ]
    errorStatusObj =
      500: ->
      422: ->
      502: ->
        console.log FAILURE_MESSAGES['WRONG']
        return
      undefined: ->
        console.log FAILURE_MESSAGES['WRONG']
        return
      default: (message) ->
        console.log message
        return
    {
      notifyError: ->
        (error) ->
          if error.$response.data.status
            status = error.$response.data.status
          statusCode = error.$response.status

          if $.inArray(statusCode, commonStatueCode) != -1
            errorStatusObj[statusCode]()
          else if angular.isUndefined(statusCode)
            errorStatusObj['undefined']()
          else
            errorStatusObj['default'] status.message
          return
    }
]
