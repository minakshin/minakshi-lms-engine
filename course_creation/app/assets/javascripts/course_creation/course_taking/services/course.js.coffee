'use strict'

angular.module('CourseTakingApp').factory 'Course', [
  'restmod'
  'APP_CONSTANTS'
  (restmod, APP_CONSTANTS) ->
    restmod.model APP_CONSTANTS.apiURL + '/user_courses'
]
