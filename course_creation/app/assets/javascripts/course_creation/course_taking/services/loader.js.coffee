'use strict'

angular.module('CourseTakingApp').factory 'Loader', ->
  {
    show: () ->
      angular.element('.loader').show()
    hide: () ->
      angular.element('.loader').hide()
  }
