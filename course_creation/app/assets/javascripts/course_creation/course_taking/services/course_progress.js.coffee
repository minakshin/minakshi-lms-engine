'use strict'

angular.module('CourseTakingApp').factory 'CourseProgress', [
  'restmod'
  'APP_CONSTANTS'
  (restmod, APP_CONSTANTS) ->
    return restmod.model APP_CONSTANTS.apiURL + '/user_course_progresses'
]
