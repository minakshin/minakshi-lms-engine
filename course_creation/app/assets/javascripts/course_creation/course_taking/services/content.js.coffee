'use strict'

angular.module('CourseTakingApp').factory 'Content', [
  'restmod'
  'APP_CONSTANTS'
  (restmod, APP_CONSTANTS) ->
    {
      getContentModel: (userCourseId) ->
        return restmod.model(APP_CONSTANTS.apiURL + '/user_courses/' + userCourseId + '/user_course_sections')
    }
]
