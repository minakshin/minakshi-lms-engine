'use strict'

angular.module('CourseTakingApp').factory 'Assessment', [
  'restmod'
  'APP_CONSTANTS'
  (restmod, APP_CONSTANTS) ->
    {
      getAssessmentModel: (userCourseId) ->
        return restmod.model(APP_CONSTANTS.apiURL + '/user_courses/' + userCourseId + '/user_course_assessments')
    }
]

