'use strict'

angular.module('CourseTakingApp').filter 'slideSetting', ->
  (setting) ->
    if setting
      if setting.bg_type_img
        {'background-image': "url(#{setting.background})"}
      else
        {'background': "#{setting.background}"}
