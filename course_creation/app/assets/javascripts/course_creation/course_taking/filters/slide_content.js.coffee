'use strict'

angular.module('CourseTakingApp').filter 'slideContent', ->
  (type) ->
    if angular.equals(type.content_type, 'Text')
      'slideText'
    else if angular.equals(type.content_type, 'Video')
      'slideVideo'
    else if angular.equals(type.content_type, 'Image')
      'slideImage'
