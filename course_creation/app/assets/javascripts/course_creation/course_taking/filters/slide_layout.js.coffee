'use strict'

angular.module('CourseTakingApp').filter 'slideLayout', ->
  (column) ->
    if column == 1
      'oneColumnLayoutPresentation'
    else
      'twoColumnLayoutPresentation'
