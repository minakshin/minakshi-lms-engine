'use strict'

angular.module('CourseTakingApp').controller 'CustomContentCtrl', [
  '$scope'
  '$sce'
  'CONTENT_TYPES'
  '$timeout'
  'Loader'
  ($scope, $sce, CONTENT_TYPES, $timeout, Loader) ->
    cta.iframeHeight()
    $scope.initCustomContent = ->
      if $scope.currentContent.content_details.unzip
        $scope.currentCustomContent = $sce.trustAsResourceUrl($scope.currentContent.content_details.unzip)
        angular.element('.content-next-btn').removeClass('disabled')
        angular.element('.course-content-block').addClass('custom-content-height')
      $timeout ->
        Loader.hide()

    $scope.$on 'changeContent', ->
      angular.element('.course-content-block').removeClass('custom-content-height')
      if $scope.currentContentType == CONTENT_TYPES.CUSTOMCONTENT
        $scope.initCustomContent()
]
