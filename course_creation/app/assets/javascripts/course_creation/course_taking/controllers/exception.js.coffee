'use strict'

angular.module('CourseTakingApp').controller 'ExceptionController', [
  'notifyService'
  '$scope'
  (notifyService, $scope) ->

    $scope.onError = ->
      notifyService.notifyError()

    return
]
