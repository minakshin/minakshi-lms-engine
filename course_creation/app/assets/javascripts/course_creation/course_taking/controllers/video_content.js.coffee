'use strict'

angular.module('CourseTakingApp').controller 'VideoContentCtrl', [
  '$scope'
  '$rootScope'
  'CONTENT_TYPES'
  '$timeout'
  'Loader'
  ($scope, $rootScope, CONTENT_TYPES, $timeout, Loader)->
    $scope.initVideoContent = ->
      if $scope.currentContent.content_details
        options =
          file: $scope.currentContent.content_details.file
          image: $scope.currentContent.content_details.thumb
          height: 462
          width: 718
          autostart: true
          events:
            onComplete: ->
              angular.element('.content-next-btn').removeClass('disabled')
              return

        jwplayer('video-content').setup(options)
      $timeout ->
        Loader.hide()

    $scope.$on 'changeContent', ->
      if $scope.currentContentType == CONTENT_TYPES.VIDEO
        $scope.initVideoContent()
    return
]
