'use strict'

angular.module('CourseTakingApp').controller 'ResourcesCtrl', [
  '$scope'
  'sharedResources'
  '$filter'
  ($scope, sharedResources, $filter)->
    $filter('htmlToPlaintext')
    $scope.selectedResource = 'documents'

    $scope.$on 'sharedResourcesChanged', ->
      if sharedResources.resources.hasOwnProperty('videos')
        videos = sharedResources.resources.videos

        angular.forEach videos, (video, index) ->
          angular.forEach video.resources, (videoResource, index) ->
            option = {}
            option.file = videoResource.file
            option.image = videoResource.thumb
            option.width = 100%
            videoResource.options = option
            return

        $scope.videos = videos
      if sharedResources.resources.hasOwnProperty('documents')
        $scope.documents = sharedResources.resources.documents
      if sharedResources.resources.hasOwnProperty('notes')
        $scope.notes = sharedResources.resources.notes
      if sharedResources.resources.hasOwnProperty('quotes')
        $scope.quotes = sharedResources.resources.quotes
]
