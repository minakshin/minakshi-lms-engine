'use strict'

angular.module('CourseTakingApp').controller 'AssessmentContentCtrl', [
  '$scope'
  'Assessment'
  'Question'
  '$rootScope'
  'CONTENT_STATUS'
  'QUESTION_STATUS'
  'ANSWER_STATUS'
  'PASSING_STATUS'
  'CONTENT_TYPES'
  'Loader'
  '$timeout'
  ($scope, Assessment, Question, $rootScope, CONTENT_STATUS, QUESTION_STATUS, ANSWER_STATUS, PASSING_STATUS, CONTENT_TYPES, Loader, $timeout) ->
    $scope.ANSWER_STATUS = ANSWER_STATUS;

    $scope.initAssessmentContent = ->
      $scope.currentQuestionIndex = 0
      $scope.passingStatus = 0
      $scope.assessmentSlides = []
      $scope.objectiveAnswersAttributes = []
      $scope.processing = false

      if $scope.currentContent.content_details
        contentDetails = $scope.currentContent.content_details
        $scope.assessmentType = contentDetails.assessment_type
        $scope.assessmentId = contentDetails.id
        $scope.noOfQuestions = contentDetails.total_questions
        $scope.contentId = $scope.currentContent.id

        if $scope.currentContent.status == CONTENT_STATUS.COMPLETED || contentDetails.is_result
          $scope.resultAssessment = contentDetails
          $scope.userPercentage = contentDetails.assessment_percentage
          $scope.passingStatus = contentDetails.passing_status
          if $scope.passingStatus == PASSING_STATUS.PASS
            angular.element('.content-next-btn').removeClass('disabled')

        if $scope.currentContent.content_details.content_data
          detailsPage = contentDetails.details_page

          if $scope.currentContent.status == CONTENT_STATUS.IN_PROGESS or $rootScope.$preview()
            if detailsPage
              startPageObj =
                question_category: 'start_assessment'
                title: contentDetails.name
                description: contentDetails.description
                no_of_question: $scope.noOfQuestions
                passing_percentage: contentDetails.passing_percentage
              $scope.assessmentSlides.push(startPageObj)
              if contentDetails.content_data[0] && contentDetails.content_data[0].status == CONTENT_STATUS.COMPLETED
                $scope.currentQuestionIndex = 1

          angular.forEach contentDetails.content_data, (question, index) ->
            if question.status == QUESTION_STATUS.ATTEMPTED
              $scope.currentQuestionIndex++
            question.questionIndex = index + 1
            $scope.assessmentSlides.push(question)

        resultPageObj =
          question_category: 'result_assessment'
          no_of_question: $scope.noOfQuestions
        $scope.assessmentSlides.push(resultPageObj)
        setAssessmentSlide()
      $timeout ->
        Loader.hide()

    setAssessmentSlide = ->
      $scope.slideData = $scope.assessmentSlides[$scope.currentQuestionIndex]

    $scope.nextSlide = ->
      $scope.objectiveAnswersAttributes = []
      $scope.currentQuestionIndex += 1
      if $scope.assessmentSlides.length > $scope.currentQuestionIndex
        setAssessmentSlide()

    $scope.selectSingleAnswer = (answer) ->
      $scope.objectiveAnswersAttributes = []
      selectedAnswerObj =
        answer_id: answer.id
      $scope.objectiveAnswersAttributes.push(selectedAnswerObj)

    $scope.selectMultipleAnswer = (answer, event) ->
      $scope.objectiveAnswersAttributes = _.filter($scope.objectiveAnswersAttributes, (selectedAnswer) ->
        selectedAnswer.answer_id != answer.id
      )
      if angular.element(event.currentTarget).is(':checked')
        selectedAnswerObj =
          answer_id: answer.id
        $scope.objectiveAnswersAttributes.push(selectedAnswerObj)

    $scope.submitAnswer = (question) ->
      if $scope.objectiveAnswersAttributes.length && !$scope.processing
        $scope.processing = true
        isLast = if $scope.noOfQuestions == question.questionIndex then true else false
        question_params = 'question':
          'assessment_id': $scope.assessmentId
          'objective_answers_attributes': $scope.objectiveAnswersAttributes
          'user_course_id': parseInt($rootScope.courseId)
          'question_id': question.id
          'question_type_id': question.question_category_id
          'last': isLast
          'preview': $rootScope.$preview()

        $scope.user_question = Question.$build(question_params)
        $scope.user_question.$save().$then ((response) ->
          $scope.nextSlide()
          if isLast
            $scope.updateCurrentAssessment(response.data)
            $scope.resultAssessment = response.data
            $scope.passingStatus = response.data.passing_status
            $scope.userPercentage = response.data.assessment_percentage
            if $scope.passingStatus == PASSING_STATUS.PASS
              angular.element('.content-next-btn').removeClass('disabled')
          $scope.processing = false
        ), $scope.onError()
      return

    $scope.startAssessment = ->
      $scope.nextSlide()

    $scope.retakeAssessment = ->
      if $scope.contentId
        Assessment.getAssessmentModel($rootScope.courseId).$find($scope.contentId).$then ((response) ->
          $rootScope.$broadcast 'updateCurrentContent', response.data
        ), $scope.onError()

    $scope.getLetter = (index) ->
      return String.fromCharCode(64+index)

    $scope.$on 'changeContent', ->
      if $scope.currentContentType == CONTENT_TYPES.ASSESSMENT
        $scope.initAssessmentContent()
    return
]
