'use strict'

angular.module('CourseTakingApp').controller 'PresentationContentCtrl', [
  '$scope'
  '$rootScope'
  '$timeout'
  'CONTENT_TYPES'
  'Loader'
  ($scope, $rootScope, $timeout, CONTENT_TYPES, Loader) ->
    changePresentationSlide = (isFirstTime) ->
      timeout = 0
      if isFirstTime && cta.isFisrtTime
        timeout = 1500
      $scope.slide = $scope.currentContent.content_details.content_data[$scope.currentSlide]
      if $scope.slide and $scope.slide.settings and $scope.slide.settings.transition
        $rootScope.slideAnimation = $scope.slide.settings.transition
      if $scope.slide
        angular.forEach $scope.slide.contents, (content, index) ->
          if angular.equals(content.content_type, 'Video')
            if $scope.slide.number_of_columns == 2 && index % 2 != 0
              content.option = { file: content.file_url, width: '100%', height: '100%' }
            else
              content.option = { file: content.file_url, width: '100%', height: '100%', autostart: true }
      cta.presentationAnimation($rootScope.slideAnimation, $scope.currentSlide)
      $timeout (->
        Loader.hide()
      ), timeout

    $scope.initPresentationContent = () ->
      $scope.setCurrentSlide(0)
      if $scope.currentContent.content_details.content_data
        $scope.setNoOfSlides($scope.currentContent.content_details.content_data.length)
        changePresentationSlide(true)
        angular.element('.content-next-btn').removeClass('disabled')
      return

    $scope.$on 'changeSlide', ->
      changePresentationSlide()

    $scope.$on 'changeContent', ->
      if $scope.currentContentType == CONTENT_TYPES.PRESENTATION
        $scope.initPresentationContent()
    return
]
