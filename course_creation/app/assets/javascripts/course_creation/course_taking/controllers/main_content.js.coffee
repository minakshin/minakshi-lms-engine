'use strict'

angular.module('CourseTakingApp').controller 'MainContentCtrl', [
  '$scope'
  'Content'
  '$rootScope'
  '$timeout'
  'CONTENT_STATUS'
  'CourseProgress'
  'Loader'
  'PAGINATE_OFFSET'
  'CONTENT_TYPES'
  'sharedResources'
  'CourseEvaluation'
  'ANSWER_VALUE'
  ($scope, Content, $rootScope, $timeout, CONTENT_STATUS, CourseProgress, Loader, PAGINATE_OFFSET, CONTENT_TYPES, sharedResources, CourseEvaluation, ANSWER_VALUE)->
    $scope.ANSWER_VALUE = ANSWER_VALUE
    $scope.currentContentType = null
    $scope.CONTENT_STATUS = CONTENT_STATUS
    $scope.PAGINATE_OFFSET = PAGINATE_OFFSET
    $scope.noOfQuestionAnswered = []

    getStateObject = (chapterId, sectionId, contentId, contentType) ->
      stateObj =
        chapter_id: chapterId
        section_id: sectionId
        content_id: contentId
        content_type: contentType
      return stateObj

    $scope.initMainContent = (isFirstLoad) ->
      $scope.contents = []
      $scope.noOfSlides = 0
      $scope.currentSlide = 0
      $scope.currentIndex = 0
      Loader.show()
      Content.getContentModel($rootScope.courseId).$find($rootScope.currentContainer.id).$then ((response) ->
        if !$rootScope.currentContentId and response.data.contents[0]
          $rootScope.currentContentId = response.data.contents[0].id

        if angular.isArray(response.data.contents)
          angular.forEach response.data.contents, (content, index) ->
            if content.content
              $scope.contents.push(content)

            if content.id == $rootScope.currentContentId && content.content_type
              $scope.currentIndex = index
            else
              angular.forEach content.contents, (sectionContent, key) ->
                if sectionContent.id == $rootScope.currentContentId && sectionContent.content_type
                  $scope.currentIndex = index
            return
        else
          if response.data.contents
            $scope.contents.push(response.data.contents)
            $scope.currentIndex = 0

        $rootScope.currentChapterName = response.data.chapter_name
        currentContent = $scope.contents[$scope.currentIndex]

        if currentContent.status == CONTENT_STATUS.NOT_STARTED && !$rootScope.$preview()
          currentContainer = $rootScope.containers[$rootScope.currentContainerIndex]
          nextState = getStateObject(currentContainer.parentContainerId, currentContainer.containerId, currentContent.id, currentContent.content_type)
          params =
            'user_course_id': $rootScope.courseId
            'current_state': null
            'next_state': nextState
            'is_last_for_chapter': false
            'is_last_for_section': false

          user_progress = CourseProgress.$build(params)
          user_progress.$save().$then ((response) ->
            $scope.contents[$scope.currentIndex].status = CONTENT_STATUS.IN_PROGESS
            $scope.changeContent($scope.currentIndex)
          ), $scope.onError()
        else
          $scope.changeContent($scope.currentIndex)

        $timeout ->
          cta.paginateScroll()
          cta.setScreenHeight()
          if isFirstLoad
            cta.slideIn()
          return
      ), $scope.onError()
      return

    $scope.setNoOfSlides = (noOfSlides) ->
      $scope.noOfSlides = noOfSlides

    $scope.setCurrentSlide = (currentSlide) ->
      $scope.currentSlide = currentSlide

    $scope.updateCurrentContent = (content) ->
      $scope.contents[$scope.currentIndex] = content
      $scope.changeContent($scope.currentIndex)
      Loader.hide()

    $scope.updateCurrentAssessment = (assessmentResult) ->
      if assessmentResult.passing_status
        $scope.currentContent.content_details.passing_status = assessmentResult.passing_status
      $scope.currentContent.content_details.correct_questions = assessmentResult.correct_questions
      if assessmentResult.user_question
        $scope.currentContent.content_details.user_question = assessmentResult.user_question
      if assessmentResult.total_question_count
        $scope.currentContent.content_details.total_questions = assessmentResult.total_question_count
      $scope.currentContent.content_details.is_result = true
      if $scope.currentContent.content_details.content_data
        delete $scope.currentContent.content_details.content_data

    $scope.selectContent = (index) ->
      newContent = $scope.contents[index]
      if newContent.status != CONTENT_STATUS.NOT_STARTED or $rootScope.$preview()
        $scope.changeContent(index)

    $scope.changeContent = (index) ->
      Loader.show()
      $scope.currentSlide = 0
      $scope.noOfSlides = 0
      $scope.currentIndex = index
      $scope.currentContent = $scope.contents[index]
      $scope.currentContentType = $scope.currentContent.content_type
      $rootScope.currentContentName = $scope.currentContent.name
      $rootScope.currentContentId = $scope.currentContent.id
      if $scope.currentContent.status == CONTENT_STATUS.COMPLETED or $rootScope.$preview()
        angular.element('.content-next-btn').removeClass('disabled')
      else
        angular.element('.content-next-btn').addClass('disabled')
      $rootScope.$broadcast 'changeContent'

    $scope.courseEvalution = (offset) ->
      if offset == PAGINATE_OFFSET.PREVIOUS
        $rootScope.courseState = 'InProgress'
        $rootScope.$broadcast 'changeContainer', offset
      else if offset == PAGINATE_OFFSET.NEXT && !angular.element('.evalution-next-btn').hasClass('disabled')
        $rootScope.courseState = 'Congratulation'

    $scope.congratulationProgress = (offset) ->
      if offset == PAGINATE_OFFSET.PREVIOUS
        $rootScope.courseState = 'CourseEvalution'

    $scope.contentProgress = (offset) ->
      currentState = nextState = null
      newIndex = $scope.currentIndex + offset
      newSlide = $scope.currentSlide + offset
      if newIndex >= 0 and newIndex <= $scope.contents.length - 1 or newSlide < $scope.noOfSlides and newSlide >= 0
        $scope.currentSlide = newSlide
        if $scope.currentContentType == CONTENT_TYPES.PRESENTATION && $scope.currentSlide < $scope.noOfSlides && $scope.currentSlide >= 0
          $rootScope.$broadcast 'changeSlide'
        else
          if !angular.element('.content-next-btn').hasClass('disabled') and offset == PAGINATE_OFFSET.NEXT or offset == PAGINATE_OFFSET.PREVIOUS
            if $scope.contents[newIndex].status != $scope.CONTENT_STATUS.NOT_STARTED  or $rootScope.$preview()
              $scope.changeContent(newIndex)
            else
              nextContent = $scope.contents[newIndex]
              currentContainer = $rootScope.containers[$rootScope.currentContainerIndex]
              if angular.equals($rootScope.currentContainer.type, 'chapter')
                currentState = getStateObject(currentContainer.containerId, null, $scope.currentContent.id, $scope.currentContent.content_type)
                nextState = getStateObject(currentContainer.containerId, null, nextContent.id, nextContent.content_type)
              else
                currentState = getStateObject(currentContainer.parentContainerId, currentContainer.containerId, $scope.currentContent.id, $scope.currentContent.content_type)
                nextState = getStateObject(currentContainer.parentContainerId, currentContainer.containerId, nextContent.id, nextContent.content_type)

              params =
                'user_course_id': $rootScope.courseId
                'current_state': currentState
                'next_state': nextState
                'is_last_for_chapter': false
                'is_last_for_section': false

              user_progress = CourseProgress.$build(params)
              user_progress.$save().$then ((response) ->
                $scope.currentContent.status = CONTENT_STATUS.COMPLETED
                $scope.contents[newIndex].status = CONTENT_STATUS.IN_PROGESS
                $scope.changeContent(newIndex)
              ), $scope.onError()
      else
        if offset == PAGINATE_OFFSET.NEXT
          if !angular.element('.content-next-btn').hasClass('disabled')
            currentContainer = $rootScope.containers[$rootScope.currentContainerIndex]
            newContainerId = $rootScope.currentContainerIndex + offset
            if newContainerId >= 0 and newContainerId <= $rootScope.containers.length - 1  and !($rootScope.$preview())
              nextContainer = $rootScope.containers[$rootScope.currentContainerIndex + offset]
              if angular.equals($rootScope.currentContainer.type, 'chapter')
                if $scope.currentContent.status != CONTENT_STATUS.COMPLETED
                  currentState = getStateObject(currentContainer.containerId, null, $scope.currentContent.id, $scope.currentContent.content_type)
                if nextContainer.status == CONTENT_STATUS.NOT_STARTED
                  nextState = getStateObject(nextContainer.containerId, null, null, null)
              else
                if $scope.currentContent.status != CONTENT_STATUS.COMPLETED
                  currentState = getStateObject(currentContainer.parentContainerId, currentContainer.containerId, $scope.currentContent.id, $scope.currentContent.content_type)
                if nextContainer.status == CONTENT_STATUS.NOT_STARTED
                  nextState = getStateObject(nextContainer.parentContainerId, nextContainer.containerId, null, null)

              if currentState or nextState
                params =
                  'user_course_id': $rootScope.courseId
                  'current_state': currentState
                  'next_state': nextState
                  'is_last_for_chapter': currentContainer.isLastSection
                  'is_last_for_section': true

                user_progress = CourseProgress.$build(params)
                user_progress.$save().$then ((response) ->
                  $rootScope.containers[$rootScope.currentContainerIndex].status = CONTENT_STATUS.COMPLETED
                  $rootScope.$broadcast 'changeContainer', offset
                ), $scope.onError()
              else
                $rootScope.$broadcast 'changeContainer', offset
            else if newContainerId >= 0 and newContainerId <= $rootScope.containers.length - 1  and ($rootScope.$preview())
              $rootScope.$broadcast 'changeContainer', offset
            else
              $rootScope.$broadcast 'evalutionCompleted'
        else
          $rootScope.$broadcast 'changeContainer', offset

    $scope.$on 'evalutionCompleted', ->
      if $scope.currentContent.status != CONTENT_STATUS.COMPLETED and !($rootScope.$preview())
        currentContainer = $rootScope.containers[$rootScope.currentContainerIndex]
        currentState = getStateObject(currentContainer.parentContainerId, currentContainer.containerId, $scope.currentContent.id, $scope.currentContent.content_type)
        params =
          'user_course_id': $rootScope.courseId
          'current_state': currentState
          'next_state': null
          'is_last_for_chapter': true
          'is_last_for_section': true
          'is_course_completed': true

        user_progress = CourseProgress.$build(params)
        user_progress.$save().$then ((response) ->
          $rootScope.courseState = 'CourseEvalution'
          $rootScope.courseCompleted = true
          $rootScope.containers[$rootScope.currentContainerIndex].status = CONTENT_STATUS.COMPLETED
          $rootScope.$broadcast 'changeContainer', PAGINATE_OFFSET.NEXT
        ), $scope.onError()
      else
        $rootScope.courseState = 'CourseEvalution'
      $rootScope.currentChapterId = null
      $rootScope.currentSectionId = null

    $scope.isAllAnswerd = (questionId, answerId) ->
      isAllAnswerd = _.filter($scope.noOfQuestionAnswered, (answer) ->
        answer.evaluation_question_id == questionId
      )
      if !isAllAnswerd.length
        obj =
          'evaluation_question_id': questionId
          'answer_id': answerId
        $scope.noOfQuestionAnswered.push(obj)
      if $scope.noOfQuestionAnswered.length == $scope.evaluationQuestionsLength
        angular.element('.evalutionSubmit').removeClass('disabled')
      else
        angular.element('.evalutionSubmit').addClass('disabled')
      return

    $scope.evaluationSubmit = ->
      if !angular.element('.evalutionSubmit').hasClass('disabled')
        if !($rootScope.$preview())
          evaluation_params = 'user_course_evaluation':
            'user_course_id': $rootScope.courseId
            'evaluation_questions_attributes': $scope.noOfQuestionAnswered

          user_progress = CourseEvaluation.$build(evaluation_params)
          user_progress.$save().$then ((response) ->
            $rootScope.evalutionCompleted = true
            angular.element('.evalutionSubmit').addClass('disabled')
          ), $scope.onError()
        else
          $rootScope.evalutionCompleted = true
          angular.element('.evalutionSubmit').addClass('disabled')
      return

    $scope.$on 'sharedResourcesChanged', ->
      $scope.evaluationQuestions = sharedResources.evaluationQuestions
      $scope.evaluationQuestionsLength = sharedResources.evaluationQuestions.length

    $scope.$on 'containerChanged', (evemt, isFirstLoad) ->
      $scope.initMainContent(isFirstLoad)

    $scope.$on 'updateCurrentContent', (event, content) ->
      $scope.updateCurrentContent(content)
    return
]
