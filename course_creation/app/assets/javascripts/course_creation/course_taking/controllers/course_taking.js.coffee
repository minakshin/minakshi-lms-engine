'use strict'

angular.module('CourseTakingApp').controller 'CourceTakingCtrl', [
  '$scope'
  '$controller'
  '$document'
  '$rootScope'
  '$window'
  ($scope, $controller, $document, $rootScope, $window)->
    $rootScope.containers = []
    $rootScope.currentContainerIndex = 0
    $rootScope.currentContainer = {}
    $rootScope.courseId = null
    $rootScope.courseName = null
    $rootScope.currentChapterId = null
    $rootScope.currentSectionId = null
    $rootScope.currentContentId = null
    $rootScope.slideAnimation = null

    angular.element(window).resize ->
      cta.setScreenHeight()
      return

    $scope.dashboardUrl = (url) ->
      $window.location.href = url

    $scope.logout = (url) ->
      $window.location.href = url

    angular.extend this, $controller('ExceptionController', $scope: $scope)
]
