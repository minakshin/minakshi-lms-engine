showReset = () ->
  $('#reset').show()

resetClick = () ->
  $('#search').val('')
  $('.search-form').submit()
$ ->
  if $('#search').val()
    showReset()
  $('#search').keyup (e) ->
    showReset()
  $('#reset').click (e) ->
    resetClick()    
  $('#publish_course').click ->
    publish = $('#publish').val()
    versionId = $('#version_id').val()
    if publish == 'true'
      message = $('#publish_course').attr('data-unpublish-message')
      unpublish_confirm = confirm(message)
      if unpublish_confirm
        publish_course(versionId, publish)
    else
      publish_course(versionId, publish)
  publish_course = (versionId, publish) -> 
    $.ajax
      type: 'put',
      url: Routes.course_creation_publish_version_path(versionId)
      success: (data, published) ->
        if data.validate_publish == true
          loader true
          if data.published == true
            $('#publishSuccessMsg .alert').show().delay(7000).fadeOut()
            $('#publish_course').text($('#publish_course').attr('data-unpublish-text'))
          else
            $('#unpublishSuccessMsg .alert').show().delay(7000).fadeOut()
            $('#publish_course').text($('#publish_course').attr('data-publish-text'))
          $('#publish').val(data.published)
          setInterval (->
            window.location.reload()
            return
          ), 3000
        else
          $('#publishErrorMsg .alert').show().delay(10000).fadeOut()

