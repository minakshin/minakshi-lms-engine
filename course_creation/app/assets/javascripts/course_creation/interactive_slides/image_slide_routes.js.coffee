//= require 'course_creation/interactive_slides/interactive_common'
//=require 'course_creation/interactive_slides/views/image_interactive/image_interactive'
class ImageSlideRouter
  constructor: ->
    _events()

  _events = ->
    commonInteractive = new lms.views.interactive()
    imageSlide = new lms.views.imageInterSlide()
    imageSlide.show_template()
    imageSlide.default_select()

    $('.update-heading h4').on 'click', ->
      commonInteractive.toggleClasses('update-heading', 'edit-heading')
      if $('.update-heading h4').text() == $('.edit-heading.hidden-area input').attr('placeholder')
        $('.edit-heading').find('#image_slide_title').val()
      else
        $('.edit-heading').find('#image_slide_title').val($('.update-heading h4').text())

    # Save title of image based slide
    $('#saveImageSlideTitle').on 'click', ->
      if !!$('.selected').attr('data-id')
        imageSlide.edit_title()
      else
        $('#saveImageSlideTitle').hide()
        imageSlide.save_title()

    # Show description container
    $('.text-area-container .is-slide-desc').on 'click', ->
      if $(@).text() == $('#image_slide_description').attr('placeholder')
        tinyMCE.activeEditor.setContent('')
      else
        $(@).hide().next('.edit-slide-content').show()

    $('#title-block').on 'click', '.cancel', ->
      imageSlide.cancel_title()

    # Save description for image based interactive slide
    $('#saveImageSlideDesc').on 'click', ->
      imageSlide.edit_data()

    $('#title-block').on 'click', '.cancel', ->
      commonInteractive.reverseToogleClasses('edit-slide-content', 'text-area-container .is-slide-desc')

    # Save description for text image based interactive slide
    $(document).on 'click', '.edit-slide-content.hidden-area #SavedDescriptionImageBased', ->
      imageSlide.edit_data()

    # Navidation of images
    $('body').on 'click', '.add-section', ->
      imageSlide.navigation(@)

    # update image
    $(document).on 'click', '.updateImageSlide', ->
      if confirm $('.deleteSlideItem').data('message')
        image_slide_id = $('.selected').attr('data-id')
        $(this).parent().attr('data-id', image_slide_id)
        imageSlide.cancel_image()
        loader true
        if $('#deleteImageSlide').length
          imageTemplate = _.template $('#deleteImageSlide').html()
          $(this).parent().html imageTemplate
          loader false
        $(this).addClass('selected')

    # Delete data of particular element
    $(document).on 'click', '.deleteSlideItem', ->
      image_slide_id = $('.selected').attr('data-id')
      if !!image_slide_id
        if confirm $('.deleteSlideItem').data('message')
          loader true
          imageSlide.delete_data()
      else
        alert $('.deleteSlideItem').data('deletemessage')

     # Image upload
    $(document).on 'click', '#imageSlideUpload', ->
      $('#imageSlideUpload').fileupload
        dataType: 'json'
        add: (e, data) ->
          version_id = $('#version_id').val()
          section_id = $('#course_section_id').val()
          text_image_slide_id = $('.ui-tabs-active a').attr('data-id')
          inter_slide_id = $('#inter_slide_id').val()
          image_slide_id = $('.selected').attr('data-id')
          if !!image_slide_id
            data.url = Routes.course_creation_version_course_section_interactive_slide_image_slide_path(version_id, section_id,inter_slide_id, image_slide_id)
            data.method = 'put'
          else
            data.url = Routes.course_creation_version_course_section_interactive_slide_image_slides_path(version_id, section_id,inter_slide_id)
          data.submit()
          loader true
          return
        done: (e, data) ->

          if data.result.status
            $('.selected').attr('data-id', data.result.inter_slide.id)
            changeImageTemplate = _.template $('#changeImageSlideId').html()
            $('.selected').html(changeImageTemplate({image: data.result.inter_slide.image.url}))
            $('.selected').attr('data-imageurl', data.result.inter_slide.image.url)
          else
            alert $('#imageSlideUpload').attr('data-uploadfailmessage')
          loader false
          return
      return

$ ->
  if $('#uploadImageSlideId').length
    image_router = new ImageSlideRouter()
