//= require 'course_creation/interactive_slides/models/model'
class TextInteractiveSlide
  constructor: ->
    @slide = new lms.model.interSlide()

  # this method is callback after saving interactive slide.
  show_title:(data) =>
    $('.update-heading').find('h4').show()
    $('.edit-heading.hidden-area').hide()
    $('.edit-heading').find('#title').val('')
    $('.update-heading h4').show().text(data.inter_slide.title)
    $('.add-section-container').find('.selected').text(data.inter_slide.title)
    $('.selected').attr('data-id', data.inter_slide.id)
    $('.selected').attr('data-title', data.inter_slide.title)

  # this method is callback after updating interactive slide.
  show_content:(data) =>
    $('.edit-slide-content').hide()
    $('.edit-slide-content span').val()
    $('.text-area-container').find('.is-slide-desc').show().html(data.inter_slide.description)
    tinyMCE.activeEditor.setContent('')
    $('.selected').attr('data-description', data.inter_slide.description)
    $('.update-heading h4').show().text(data.inter_slide.title).next().hide()
    $('.update-heading').removeClass('active-heading')
    $('.update-heading').attr('data-slide-id', data.inter_slide)

  # generating Url for ajax call
   formatUrl = ->
    version_id = $('#version_id').val()
    section_id = $('#course_section_id').val()
    inter_slide_id = $('#inter_slide_id').val()
    Routes.course_creation_version_course_section_interactive_slide_interactive_slides_informations_path(
      version_id, section_id, inter_slide_id)

  # Edit Url for ajax call
  editUrl = ->
    version_id = $('#version_id').val()
    section_id = $('#course_section_id').val()
    inter_slide_id = $('#inter_slide_id').val()
    text_slide_id = $('.selected').attr('data-id')
    Routes.course_creation_version_course_section_interactive_slide_interactive_slides_information_path(
        version_id, section_id, inter_slide_id, text_slide_id)


  # this method is callback after deleting the slide
  delete_content:(data, status) =>
    $('.update-text-container').hide()
    location.reload()

  empty_title: =>
    alert($('.edit-heading').attr('data-emptytitle'))
    $('.update-links').show()
    $('.edit-heading').show()

  # collects data needed for saving the slide.
  save_data: ->
    slide_title = $('.edit-heading').find('#title').val()
    if !slide_title
      @empty_title()
    else
      slide_type = 'CourseCreation::TextSlide'
      url = formatUrl()
      slide_title = $('.edit-heading').find('#title').val()
      @slide.save(slide_title, url, slide_type,@show_title)

  # collects data needed for updating the slide.
  edit_data: ->
    if $('.selected').attr('data-id') != '' && tinyMCE.get('description').getContent() != ''
      slide_title = $('.update-heading').find('#title').val()
      url = editUrl()
      slide_content = tinyMCE.get('description').getContent()
      @slide.edit(slide_title, slide_content, url, @show_content)
    else if tinyMCE.get('description').getContent() == ''
      alert($('.edit-slide-content').attr('data-description-content'))
    else
      alert($('.edit-slide-content').attr('data-alert-message'))

  # collects data needed for deleting the slide.
  delete_data: (confirmation) ->
    if confirmation
      url = editUrl()
      @slide.delete_slide(url, @delete_content)

  cancel_title: ->
    $('.edit-heading.hidden-area').hide()
    $('.update-heading').show()

  content_show: ->
    $('.text-area-container .is-slide-desc').click ->
    $(@).hide().next('.edit-slide-content').show()

  #edit title of text-based interactive slide
  edit_title: ->
    slide_title = $('.edit-heading').find('#title').val()
    if !slide_title
      @empty_title()
    else
      if $('.selected').attr('data-id') != ''
        slide_title = $('#title').val()
        url = editUrl()
        data = {'interactive_slides_information': { 'title': slide_title } }
        @slide.edit_slide(url, data, @show_title)
      else
        alert($('.edit-heading').attr('data-emptytitle'))

lms.views.textInterSlide = TextInteractiveSlide
