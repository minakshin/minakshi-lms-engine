//= require 'course_creation/interactive_slides/models/model'
class TextImageInteractiveSlide
  constructor: ->
    @textImageSlide = new lms.model.interSlide()
    _.templateSettings = lms.constant.TEMPLATE_SETTING

  # Render templates
  show_template: ->
    if $('#upload_image_id').length
      @initialize_template()
      @show_data_template()

  # initialize template
  initialize_template: =>
    @imageTemplate = _.template $('#upload_image_id').html()
    @titleTemplate = _.template $('#update_heading_id').html()
    @descriptionTemplate = _.template $('#update_description_id').html()
    @changeImageTemplate = _.template $('#change_image_id').html()

  # pass title and value(if title is already present) to title template
  display_data: (title, description) =>
    value = if title is $('.deleteItem').attr('data-titlemessage') then '' else title
    descriptionValue = if description is $('.deleteItem').data('descmessage') then '' else description
    $('.update-heading h4').show().text(title)
    $('.text-area-container .is-slide-desc').show().html(description)
    $('.edit-heading.hidden-area').hide()
    $('.edit-slide-content.hidden-area').hide()

  display_image: (image) =>
    $('.tab-view-image-upload').html(@changeImageTemplate({image: image}))

  # this method is callback after saving interactive slide.
  show_image_title:(data) =>
    $('.edit-heading.hidden-area').hide()
    $('.edit-heading').find('#title').val('')
    $('.update-heading h4').show().text(data.inter_slide.title)
    $('.ui-tabs-active a').text(data.inter_slide.title)
    if !$('.ui-tabs-active a').attr('data-id')
      $('.ui-tabs-active a').attr('data-id',data.inter_slide.id)
    $('.ui-tabs-active a').attr('data-title',data.inter_slide.title)

  # Navigation for tabs
  tab_navigation:(selectedTab) ->
    @display_data(selectedTab.dataset.title, selectedTab.dataset.description)
    if !!selectedTab.dataset.imageUrl
      @display_image(selectedTab.dataset.imageUrl)
    else if !selectedTab.dataset.imageUrl
      $('.tab-view-image-upload').html @imageTemplate

  # Enter data template
  show_data_template: ->
    $('.tab-view-image-upload').html(@imageTemplate)
    imageUrl = $('.ui-tabs-active a').attr('data-image-url')
    if !imageUrl
      $('.tab-view-image-upload').html @imageTemplate
    else
      @display_image imageUrl
    @display_data($('.ui-tabs-active a').data('title'),
      $('.ui-tabs-active a').data('description'))

  # paremeters for url
  url_params: ->
    @inter_slide_id = $('#inter_slide_id').val()
    @version_id = $('#version_id').val()
    @section_id = $('#course_section_id').val()

  # url to save data(post)
  create_url: ->
    @url_params()
    Routes.course_creation_version_course_section_interactive_slide_interactive_slides_informations_path(
      @version_id, @section_id, @inter_slide_id)

  # url for edit and delete
  edit_delete_url: ->
    @url_params()
    text_image_slide_id = $('.ui-tabs-active a').attr('data-id')
    Routes.course_creation_version_course_section_interactive_slide_interactive_slides_information_path(@version_id, @section_id, @inter_slide_id, text_image_slide_id)


  # url for image delete
  delete_url: =>
    @url_params()
    image_slide_id = $('.ui-tabs-active a').attr('data-id')
    Routes.course_creation_delete_slide_image_version_course_section_interactive_slide_interactive_slides_information_path(@version_id, @section_id, @inter_slide_id, image_slide_id)

  # collects and save data needed for saving the slide.
  save_data: ->
    object_id = $('.ui-tabs-active a').attr('data-id')
    @slide_type = 'CourseCreation::TextImageSlide'
    @slide_title = $('.edit-heading').find('#text_image_slide_title').val()
    if !@slide_title
      alert $('#saveTitleImageBased').data('emptytitle')
      $('#saveTitleImageBased').show()
    else
      url = @create_url()
      @textImageSlide.save(@slide_title, url, @slide_type,@show_image_title)

  # Update title record
  edit_title: ->
    url = @edit_delete_url()
    slide_title = $('.edit-heading').find('#text_image_slide_title').val()
    slide_content = $('#text_image_slide_description').val()
    if !slide_title
      alert $('#saveTitleImageBased').data('emptytitle')
    else
      @textImageSlide.edit(slide_title, slide_content, url, @show_image_title)

  # this method is callback after updating interactive slide.
  show_content:(data) =>
    $('.ui-tabs-active a').attr('data-description',data.inter_slide.description)
    $('.edit-slide-content').hide()
    $('.text-area-container').find('.is-slide-desc:first').show().html(data.inter_slide.description)
    $('.update-heading h4').show().text(data.inter_slide.title).next().hide()
    $('.update-heading').removeClass('active-heading')
    $('.update-heading').attr('data-slide-id', data.inter_slide)

  # collects data needed for updating the slide description.
  edit_data: ->
    text_image_slide_id = $('.ui-tabs-active a').attr('data-id')
    slide_content = tinyMCE.get('text_image_slide_description').getContent()
    if !slide_content
      alert $('#savedDescriptionImageBased').data('emptydesc')
    else
      if !!text_image_slide_id
        slide_title = $('.update-heading h4').text()
        url = @edit_delete_url()
        slide_content = tinyMCE.get('text_image_slide_description').getContent()
        @textImageSlide.edit(slide_title, slide_content, url, @show_content)
      else
        alert $('#savedDescriptionImageBased').data('emptytitle')

  # this method is callback after deleting the slide
  delete_content:(data, status) =>
    $('.update-text-container').hide()
    location.reload()

  # Cancel title
  cancel_title: ->
    $('.edit-heading.hidden-area').hide()
    $('.update-heading').show()

  # collects data needed for deleting the slide.
  delete_data: ->
    $('.tab-view-image-upload').html(@imageTemplate)
    url = @edit_delete_url()
    @textImageSlide.delete_slide(url, @delete_content)

  show_image_delete: ->
    $('.ui-tabs-active a').attr('data-image-url', '')

  # cancel image
  cancel_image: ->
    url = @delete_url()
    $('.tab-view-image-upload').html @imageTemplate
    data = {'interactive_slides_information': { 'image': '' } }
    @textImageSlide.edit_slide(url, data, @show_image_delete)

lms.views.textImageInterSlide = TextImageInteractiveSlide
