//= require 'course_creation/interactive_slides/models/model'
class ImageInteractiveSlide
  constructor: ->
    @imageSlide = new lms.model.interSlide()
    _.templateSettings = lms.constant.TEMPLATE_SETTING

  # template intialization
  initialize_template: =>
    @imageTemplate = _.template $('#uploadImageSlideId').html()
    @fileUploadImagetemplate = _.template $('#imageSlideId').html()
    @textContainerTemplate = _.template $('#text_container_id').html()

  # this method is callback after saving interactive slide.
  show_template: ->
    if $('#uploadImageSlideId').length
      @initialize_template()
      imageUrl = $('.selected').attr('data-imageurl')
      $('.image-slide-upload').html(@imageTemplate({image: imageUrl}))

  # Dedault apply selected class and file uplaod
  default_select: ->
    $('.add-section').first().addClass('selected')
    title = $('.selected').attr('data-title')
    description = $('.selected').attr('data-description')
    @data_display(title, description)
    $('.selected').find('.upload-course-image').prepend @fileUploadImagetemplate if $('.selected').find('.image-upload-form').length == 0 && !$('.selected').attr('data-imageurl')

  # this method is callback after saving interactive slide.
  show_image_title:(data) =>
    $('.update-heading').find('h4').show()
    $('.edit-heading.hidden-area').hide()
    $('.edit-heading').find('#image_slide_title').val('')
    $('.update-heading h4').show().text(data.inter_slide.title)
    if $('.selected').attr('data-id') is ''
       $('.selected').attr('data-id', data.inter_slide.id)
    $('.selected').attr('data-title', data.inter_slide.title)

  navigation:(element) ->
    $('.add-section').removeClass('selected');
    $(element).addClass('selected')
    $('.custom-upload').removeClass('selected')
    $(element).find('.upload-course-image').prepend @fileUploadImagetemplate if $(element).find('.image-upload-form').length == 0 && !$('.selected').attr('data-imageurl')
    $(element).unbind('click')
    $('.update-heading h4').text($(element).attr('data-title'))
    $('.text-area-container').find('.is-slide-desc').show().text($(element).attr('data-description'))
    @data_display($(element).attr('data-title'), $(element).attr('data-description'))

   data_display: (title, description) =>
    textValue = if title is $('.deleteSlideItem').attr('data-titlemessage') then '' else title
    descriptionValue = if description is $('.deleteSlideItem').data('descmessage') then '' else description
    $('.update-text-container').show()
    $('.update-heading h4').show().text($('.add-section.selected').attr('data-title'))
    $('.text-area-container').find('.is-slide-desc').show().html($('.add-section.selected').attr('data-description'))
    $('.edit-heading.hidden-area').hide()
    $('.edit-slide-content.hidden-area').hide()

  # this method is callback after updating interactive slide.
  show_content:(data) =>
    $('.edit-slide-content').hide()
    $('.edit-slide-content span').val()
    $('.text-area-container').find('.is-slide-desc').show().html(data.inter_slide.description)
    $('.selected').attr('data-description', data.inter_slide.description)
    $('.update-heading h4').show().text(data.inter_slide.title).next().hide()
    $('.update-heading').removeClass('active-heading')

  cancel_title: ->
    $('.edit-heading.hidden-area').hide()
    $('.update-heading').show()

  # Parameters required for URL
  url_parameters: =>
    @version_id = $('#version_id').val()
    @section_id = $('#course_section_id').val()
    @inter_slide_id = $('#inter_slide_id').val()

  # Edit/update/delete URL
  edit_delete_url: =>
    @url_parameters()
    image_slide_id = $('.selected').attr('data-id')
    Routes.course_creation_version_course_section_interactive_slide_interactive_slides_information_path(@version_id, @section_id, @inter_slide_id, image_slide_id)

  # url for image delete
  delete_url: =>
    @url_parameters()
    image_slide_id = $('.selected').attr('data-id')
    Routes.course_creation_delete_slide_image_version_course_section_interactive_slide_interactive_slides_information_path(@version_id, @section_id, @inter_slide_id, image_slide_id)

  # create record url
  create_url: =>
    @url_parameters()
    Routes.course_creation_version_course_section_interactive_slide_interactive_slides_informations_path(@version_id, @section_id, @inter_slide_id)

  # Saves title of image based slide
  save_title: =>
    @slide_type = 'CourseCreation::ImageSlide'
    @slide_title = $('.edit-heading').find('#image_slide_title').val()
    if !@slide_title
      @empty_title()
    else
      @create_data()

  # Save record
  create_data: ->
    url = @create_url()
    @imageSlide.save(@slide_title, url, @slide_type, @show_image_title)

  # called if title is empty
  empty_title: =>
    alert($('#saveImageSlideTitle').attr('data-emptytitle'))
    $('.update-heading').show()
    $('.edit-heading').show()

  #edit title of text-based interactive slide
  edit_title: ->
    slide_title = $('.edit-heading').find('#image_slide_title').val()
    if !slide_title
      @empty_title()
    else
      url = @edit_delete_url()
      data = {'interactive_slides_information': { 'title': slide_title } }
      @imageSlide.edit_slide(url, data, @show_image_title)

  # Update database record if alreay exists
  update_data: ->
    url = @edit_delete_url()
    slide_content = $('#image_slide_description').val()
    @imageSlide.edit(@slide_title, slide_content, url, @show_image_title)

  # collects data needed for updating the slide description.
  edit_data: ->
    if $('.update-heading h4').text() == $('.edit-heading.hidden-area input').attr('placeholder')
      alert $('#saveImageSlideDesc').data('emptytitle')
    else if tinyMCE.get('image_slide_description').getContent() == ''
      alert($('.edit-slide-content').attr('data-description-content'))
    else
      slide_content = tinyMCE.get('image_slide_description').getContent()
      url = @edit_delete_url()
      @imageSlide.edit( @image_slide_title, slide_content, url, @show_content)

  # Delete content callback
  delete_content:(data, status) =>
    location.reload()

  # collects data needed for deleting the slide.
  delete_data: ->
    url = @edit_delete_url()
    @imageSlide.delete_slide(url, @delete_content)

  show_image_delete:(data) ->
    if !data.slide.title && !data.slide.description
      $('.selected').attr('data-id', '')
    $('.selected').attr('data-imageurl', '')

  # cancel image
  cancel_image: ->
    url = @delete_url()
    data = {'interactive_slides_information': { 'image': '' } }
    @imageSlide.edit_slide(url, data, @show_image_delete)

lms.views.imageInterSlide = ImageInteractiveSlide
