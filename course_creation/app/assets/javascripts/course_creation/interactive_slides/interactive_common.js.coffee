#common js for interactive slides
class CommonInteractiveSlide
  toggleClasses: (showclass,hideclass)->
    $('.'+showclass).show();
    $('.'+hideclass).hide();

  reverseToogleClasses: (hideclass, showclass)->
    $('.'+hideclass).hide();
    $('.'+showclass).show();


lms.views.interactive = CommonInteractiveSlide
