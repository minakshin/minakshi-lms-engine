//= require 'course_creation/interactive_slides/views/text_image_interactive/text_image_interactive'
//= require 'course_creation/interactive_slides/interactive_common'
class TextImageRouter
  constructor: ->
    textImageSlide = new lms.views.textImageInterSlide()
    commonInteractive = new lms.views.interactive()
    # tab navigation jquery
    $('.tabs').tabs()
    textImageSlide.show_template()

    # slide title
    $('.update-heading h4').on 'click', ->
      if $('.update-heading h4').text() == $('.edit-heading.hidden-area input').attr('placeholder')
        $('.edit-heading').show()
        $('.edit-heading input[type=text]').val('')
      else
        $('.edit-heading > #text_image_slide_title').val($('.update-heading h4').text())

    # Save title for text image based interactive slide
    $(document).on 'click', '.update-links #saveTitleImageBased', (e) ->
      object_id = $('.ui-tabs-active a').attr('data-id')
      if !object_id
        $('#saveTitleImageBased').hide()
        textImageSlide.save_data()
      else
        textImageSlide.edit_title()

    #Description
    $('#text_image_description span:first').on 'click', ->
      if $(@).text() == $('#text_image_slide_description').attr('placeholder')
        tinyMCE.activeEditor.setContent('')
      else
        $(@).hide()
        $('.edit-slide-content').show()
        tinyMCE.activeEditor.setContent($(@).text())

    # Description cancel
    $(document).on 'click', '.update-links .cancel', (event) ->
      commonInteractive.reverseToogleClasses('edit-slide-content', 'text-area-container .is-slide-desc')

    # Save description for text image based interactive slide
    $(document).on 'click', '.edit-slide-content #savedDescriptionImageBased', (e) ->
      textImageSlide.edit_data()

    $(document).on 'click', '.update-heading .cancel', (event) ->
      textImageSlide.cancel_title()

    # Get selected tab
    $('.tabs li a').click (event) ->
      textImageSlide.tab_navigation(event.target)

    # Image upload
    $(document).on 'click', '#image', ->
      $('#image').fileupload
        dataType: 'json'
        add: (e, data) ->
          version_id = $('#version_id').val()
          section_id = $('#course_section_id').val()
          text_image_slide_id = $('.ui-tabs-active a').attr('data-id')
          inter_slide_id = $('#inter_slide_id').val()
          text_image_slide_id = $('.ui-tabs-active a').attr('data-id')
          if !!text_image_slide_id
            data.url = Routes.course_creation_version_course_section_interactive_slide_text_image_slide_path(version_id, section_id,inter_slide_id, text_image_slide_id)
            data.method = 'put'
          data.submit()
          loader true
          return
        done: (e, data) ->
          if data.result.status
            imageTemplate = _.template $('#change_image_id').html()
            $('.tab-view-image-upload').html(imageTemplate({image: data.result.inter_slide.image.url}))
            $('.ui-tabs-active a').attr('data-id', data.result.inter_slide.id)
            $('.ui-tabs-active a').attr('data-image-url', data.result.inter_slide.image.url)
          else
            alert $('.imageUpload').data('uploadmessage')
          loader false
          return
      return

    # update image
    $(document).on 'click', '#deleteImage', ->
      if confirm $('.deleteItem').data('message')
        textImageSlide.cancel_image()

    # delete slide
    $(document).on 'click', '.deleteItem', (e) ->
      if confirm($('.deleteItem').data('message'))
        textImageSlide.delete_data()

$ ->
  image_router = new TextImageRouter()
