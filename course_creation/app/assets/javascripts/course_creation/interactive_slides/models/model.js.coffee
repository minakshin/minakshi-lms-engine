# this class contains CRUD methods for interctive slides
class InteractiveSlide
  # addition of text interactive slide
  save: (slide_title, url, slide_type, show_title_callback) ->
    $.ajax
      type: 'post',
      url:  url,
      data: {'interactive_slides_information': { 'title': slide_title, 'type': slide_type} },
      complete: $(this).data('requestRunning', false),
      success: (data, status, inter_slide) =>
        show_title_callback(data,status,inter_slide)
        $('.update-links').show()
        $('#saveImageSlideTitle').show()
        $('#saveTitleImageBased').show()

  # editing text interactive slide
  edit: (slide_title, slide_content, url, show_content_callback) ->
    $.ajax
      type: 'put'
      url: url
      data: {'interactive_slides_information': { 'description': slide_content, 'title': slide_title } },
      success: (data, status, inter_slide) =>
        show_content_callback(data, status, inter_slide)

  # deleting text interactive slide
  delete_slide: (url, delete_slide_callback) ->
    $.ajax
      type: 'delete'
      url: url
      success: (data, status) =>
        delete_slide_callback(data, status)

  #Todo ajax put edit interactive slide
  edit_slide: (url, params, show_edit_callback) ->
    $.ajax
      type: 'put'
      dataType: 'json'
      url: url
      data: params,
      complete: $(this).data('requestRunning', false),
      success: (data, status) ->
        show_edit_callback(data)
        $('.update-links').show()

lms.model.interSlide = InteractiveSlide
