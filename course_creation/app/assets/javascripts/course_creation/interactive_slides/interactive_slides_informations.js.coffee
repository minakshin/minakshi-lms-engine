//= require 'course_creation/interactive_slides/models/model'
$ ->
  slide = new lms.model.interSlide

  toggleClasses = (showclass,hideclass)->
    $('.'+showclass).show();
    $('.'+hideclass).hide();

  reverseToogleClasses = (hideclass, showclass)->
    $('.'+hideclass).hide();
    $('.'+showclass).show();

  showEditSlideName = ->
    toggleClasses('editSlideName','quiz-name');

  hideEditSlideName = ->
    reverseToogleClasses('editSlideName','quiz-name')

  showEditSlideDescription = ->
    toggleClasses('editSlideDescription','quiz-description');

  hideEditSlideDescription = ->
    tinyMCE.get('slide-description').setContent($('.interactive-slide-desc').text())
    reverseToogleClasses('editSlideDescription','quiz-description')

  showNameCallBack = (data)->
    $('.quiz-name h3').text(data.interactive_slide.name)
    hideEditSlideName()

  showDescriptionCallBack = (data)->
    $('.quiz-description .interactive-slide-desc').html(data.interactive_slide.description)
    hideEditSlideDescription()

  ajaxAttributes = ->
    id = $('.quiz-name h3').prop('id')
    version_id = $('#version_id').val()
    section_id = $('#course_section_id').val()
    url = Routes.course_creation_version_course_section_interactive_slide_path(version_id, section_id, id)

  $('.quiz-name .edit-icon').click ->
    $('.editSlideName input').val($('.quiz-name h3').text().trim())
    showEditSlideName()

  $('.editSlideName .cancel').click ->
    hideEditSlideName()

  $('.editSlideName .save').click ->
    if !$('.editSlideName input').val()
      alert($('.quiz-name h3').attr('data-emptytitle'))
    else
      slide_name = $('.editSlideName input').val()
      data = { interactive_slide: name: slide_name }
      slide.edit_slide(ajaxAttributes(), data, showNameCallBack(data))


  $('.quiz-description .edit-icon').click ->
    showEditSlideDescription()

  $('#slide-cancel').click ->
    hideEditSlideDescription()

  $('#slide-save').click ->
    if tinyMCE.get('slide-description').getContent() != ''
      slide_description = tinyMCE.get('slide-description').getContent()
      data = { interactive_slide: description: slide_description }
      slide.edit_slide(ajaxAttributes(), data, showDescriptionCallBack(data))
      $('#slide-description').text(slide_description)
    else
      alert($('#slide-description').attr('data-slide-description'))

