$ ->
  $('#category_name').focusin ->
    $('.error_flash').fadeOut(1000)

  setTimeout (->
    $('.mce-tinymce').find('iframe').css('width','99%')
    return
  ), 1000

  $('.left-nav > li.submenu > a').click ->
    $(@).toggleClass('active').next().slideToggle()
    return

  $('.add-assessment-content').closest('.chapter-name-wrap').css('background-color','#345e41')

  if $('.alert') .length > 0
    $('.alert').delay(6000).fadeOut()

  footerMargin = ->
    if $('.wrapper').height() >= window.innerHeight
      $('.wrapper').css('margin-bottom', '0')
    return

  footerMargin()

  full_path = window.location.pathname
  nav_active = (url) ->
    return true if url is full_path
    resources = ['documents', 'videos', 'quotes', 'notes']
    for resource in resources
      return true if (full_path.indexOf(resource) > -1) and (url.indexOf(resource) > -1)

  $('.left-nav li').each ->
    url = $('a', @).attr('href')
    if nav_active(url)
      $('a', @).addClass 'active'
      if($(@).parent().parent().hasClass('submenu'))
        $(@).parent().parent().find('a:first').toggleClass('active').next().slideToggle()
  return
window.loader = (param) ->
  $('.loader').toggle param
