$ ->
  if $('#new_evaluation_question').length
    $('#new_evaluation_question').validate()
    $('#evaluation_question_content').rules 'add',
      required: true
      messages: required: $('.submit-evaluation').data('message')

  if $('#new_quote, #new_note').length
    $('#new_quote, #new_note').validate()
    $('#quote_title, #note_title').rules 'add',
      required: true
      messages: required: $('.submit-resource').data('message')

    $('.submit-resource').on 'click', ->
      resource_type = $('.submit-resource').data('type').toLowerCase()
      content = tinyMCE.get(resource_type + '_content').getContent()
      if content is ''
        $('.description-error').html($('.submit-resource').data('message'))

  # Client side validation for presentation
  if $('#new_presentation').length
    $('#new_presentation').validate()
    $('#presentation_title').rules 'add',
      required: true
      messages: required: $('#presentation_title').data('message')

  # Client side validation for Ineractive slide
  $('.submit-interactive-slide').on 'click', ->
    set_error = false
    text_content = $('#interactive_slide_name').val()
    if text_content is ''
      set_error = true
      $('.name-error').html($('.submit-interactive-slide').data('emptyTitle'))

    content = tinyMCE.get('interactive_slide_description').getContent()
    if content is ''
      set_error = true
      $('.description-error').html($('.submit-interactive-slide').data('emptyDescription'))

    if set_error
      return false

  $('#interactive_slide_name').focusin ->
    $('.name-error').fadeOut(1000)

  # Client side validation for certificate
  if $('#new_certificate').length
    $('#new_certificate, .edit_certificate').validate()
    $('#certificate_title').rules 'add',
      required: true
      messages: required: $('.submit-certificate').data('empty-title')

    $('#certificate_file').rules 'add',
      required :true
      extension: 'pdf'
      messages:
        required: $('.submit-certificate').data('empty-file')
        extension: $('.submit-certificate').data('invalid-file-format')

  # Client side validation for custom content
  if $('#new_custom_content').length
    $('#new_custom_content').validate submitHandler: (form) ->
      loader true
    $('#custom_content_name').rules 'add',
      required: true
      messages: required: $('.save-custom-content').data('name-message')

    $('#custom_content_zip').rules 'add',
      required: true
      extension: 'zip'
      messages:
        required: $('.save-custom-content').data('upload-zip')
        extension: $('.save-custom-content').data('invalid-content')

  if $('#new_category, .edit_category').length
    $('#new_category, .edit_category').validate()
    $('#category_name').rules 'add',
      required: true
      messages: required: $('#category_name').data('message')
  return
