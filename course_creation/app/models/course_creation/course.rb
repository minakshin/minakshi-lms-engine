module CourseCreation
  # Course Model
  class Course < ActiveRecord::Base
    has_many :versions, dependent: :destroy
    belongs_to :categories
    belongs_to :certificate
    accepts_nested_attributes_for :versions
    validates :title, presence: { message: I18n.t(
                'course_creation.courses.blank_title_message') }, uniqueness: { message: I18n.t(
                'course_creation.courses.duplicate_title_message') }

    class << self
      def published_versions
        CourseCreation::Course.joins(
          :versions).where(course_creation_versions: { published: true })
      end
    end
  end
end
