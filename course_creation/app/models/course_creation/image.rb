module CourseCreation
  # Image Model
  class Image < Resource
    mount_uploader :file, ImageUploader
    validates :file, presence: true
    validates :title,
              length: {
                minimum: 4,
                maximum: 255,
                too_long: "%{count} characters is the maximum allowed",
                too_short: "Must have at most %{count} characters"
              }
  end
end
