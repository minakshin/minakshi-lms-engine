module CourseCreation
  # roles model
  class Role < ActiveRecord::Base
    has_many :version_roles
    has_many :versions, through: :version_roles
  end
end
