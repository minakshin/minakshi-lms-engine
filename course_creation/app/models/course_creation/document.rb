module CourseCreation
  # Documenet Model
  class Document < Resource
    mount_uploader :file, DocumentUploader
    belongs_to :course_section
    belongs_to :content_section, class_name: 'CourseSection'
    validates :file, presence: true
    include CourseCreation::VersionValidator
    validate :version_published, on: :create
    before_destroy :version_editable
    include CourseContent
  end
end
