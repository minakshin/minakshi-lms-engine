module CourseCreation
  module CourseTaking
    # User course evaluation questions model
    class UserCourseEvaluationQuestion < ActiveRecord::Base
      belongs_to :user_course_evaluation,
                 class_name: CourseCreation::CourseTaking::UserCourseEvaluation
    end
  end
end
