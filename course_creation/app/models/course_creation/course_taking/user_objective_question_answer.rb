module CourseCreation
  module CourseTaking
    # User objective model
    class UserObjectiveQuestionAnswer < ActiveRecord::Base
      validates :answer_id, numericality: { only_integer: true }
      belongs_to :user, class_name: CourseCreation.user_class
      belongs_to :user_question
      belongs_to :answer
      delegate :position, to: :answer
    end
  end
end
