module CourseCreation
  module CourseTaking
    # User questions
    class UserQuestion < ActiveRecord::Base
      validates :assessment_id, :question_id,
                :user_course_id, :question_type_id,
                numericality: { only_integer: true }
      has_many :user_descriptives, class_name: UserDescriptive,
                                   dependent: :destroy
      has_many :objective_answers, class_name: UserObjectiveQuestionAnswer,
                                   dependent: :destroy
      accepts_nested_attributes_for :objective_answers, :user_descriptives
      belongs_to :assessment
      belongs_to :question
      belongs_to :user_course,
                 class_name: CourseCreation::CourseTaking::UserCourse
      after_create :evaluate_question
      # Question status {1 : correct answer, 0: false answer}
      STATUS = [1, 0]

      def evaluate_question
        answer = Question.find(question_id).answers.where(
          correct_answer: true).pluck(:id)
        user_answer = objective_answers.pluck(:answer_id)
        self.passing_status =
          answer.uniq.sort == user_answer.uniq.sort ? STATUS.first : STATUS.last
        save!
      end

      def delete_answers
        user_descriptives.delete_all
        objective_answers.delete_all
      end
    end
  end
end
