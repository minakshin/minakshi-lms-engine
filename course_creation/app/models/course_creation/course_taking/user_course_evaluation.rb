module CourseCreation
  module CourseTaking
    # User course evaluation model
    class UserCourseEvaluation < ActiveRecord::Base
      belongs_to :course_evaluation,
                 class_name: CourseCreation::EvaluationQuestion
      belongs_to :user_course
      has_many :evaluation_questions,
                class_name: CourseCreation::CourseTaking::UserCourseEvaluationQuestion,
                dependent: :destroy, foreign_key: 'user_course_evaluation_id'
      accepts_nested_attributes_for :evaluation_questions
      after_create do |user_course_evaluation|
        user_course_evaluation.user_course.complete!
      end
    end
  end
end
