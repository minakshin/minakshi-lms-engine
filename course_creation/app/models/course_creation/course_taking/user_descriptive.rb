module CourseCreation
  module CourseTaking
    # User descriptive model
    class UserDescriptive < ActiveRecord::Base
      belongs_to :user, class_name: CourseCreation.user_class
      belongs_to :user_question
      belongs_to :answer
      delegate :position, to: :answer
    end
  end
end
