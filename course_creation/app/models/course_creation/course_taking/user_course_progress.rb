module CourseCreation
  module CourseTaking
    # User course progress model
    class UserCourseProgress < ActiveRecord::Base
      belongs_to :user_course
      belongs_to :section, class_name: CourseCreation::CourseSection,
                           foreign_key: :content_id
      before_save :validate_version
      after_save :update_course_progress
      validates :user_course_id, :content_id,
                :content_type, presence: true
      ATTEMPT = 1
      def save_status(status_code)
        self.status = status_code
      end

      def user_attempts
        self.attempt_to = attempt_to.nil? ? ATTEMPT : attempt_to + ATTEMPT
      end

      def self.update_course_progress(user_id, content_id, content_type,
                                      status)
        content_type =
          CourseSection.find(content_id).parent_id.nil? ? 'Chapter' : 'Section' if content_type.nil?
        progress = CourseTaking::UserCourseProgress.find_or_initialize_by(
          user_course_id: user_id,
          content_id: content_id,
          content_type: content_type
        )
        progress.status = status
        progress.save
      end

      def validate_version
        !user_course.version.editable
      end

      def update_course_progress
        return unless section.valid_progress_content? && valid_progress?
        user_course.update_progress_percentage
      end

      def valid_progress?
        const_status = CourseCreation::ApiStatuses::STATUS[2]
        (user_course.course_progresses.where(
          "content_id = #{content_id} AND status = #{const_status}"
        ).count == 1 &&
        status == const_status)
      end
    end
  end
end
