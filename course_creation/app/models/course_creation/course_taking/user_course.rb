module CourseCreation
  module CourseTaking
    # User course model
    class UserCourse < ActiveRecord::Base
      include CourseCreation::CoursePasser
      belongs_to :user, class_name: CourseCreation.user_class
      belongs_to :version
      has_many :course_assessments, class_name: UserCourseAssessment,
                                    dependent: :destroy
      has_many :course_progresses, class_name: UserCourseProgress,
                                   dependent: :destroy
      has_many :user_questions, dependent: :destroy
      has_one :user_course_evaluation, dependent: :destroy
      COURSE_COMPLETE_STATUS = 2
      COURSE_INITIAL_STATUS = 1

      def self.preview_user_id
        CourseCreation.preview_user_id || CourseCreation.user_class.find_by(
          :email, CourseCreation.admin_email).id
      end

      def complete!
        update_attributes(course_status: COURSE_COMPLETE_STATUS)
      end

      def update_progress_percentage
        progress = progress_percentage.to_f + (version.contents_count * 100.0)
        update_attributes(progress_percentage: progress)
      end

      def update_version
        version.update_attributes(editable: false) if version.editable
      end

      def update_user_course_status
        update_attributes(course_status: COURSE_INITIAL_STATUS)
      end

      def update_course_content
        update_attributes(content_complete: true)
      end
    end
  end
end
