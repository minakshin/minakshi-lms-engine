module CourseCreation
  # Text Image as a Interactive slide model
  class TextImageSlide < InteractiveSlidesInformation
    mount_uploader :image, ImageUploader
  end
end
