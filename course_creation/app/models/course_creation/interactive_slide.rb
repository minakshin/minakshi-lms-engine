module CourseCreation
  # Interactive slide base model
  class InteractiveSlide < ActiveRecord::Base
    include CourseContent
    TEXTSLIDE = 'Text Based'
    IMAGESLIDE = 'Image Based'
    TEXTIMAGESLIDE = 'Text Image Based'
    TEXTROUTE = 'text'
    IMAGEROUTE = 'image'
    TEXTIMAGEROUTE = 'text_image'
    belongs_to :course
    belongs_to :course_section
    has_many :interactive_slides_informations, dependent: :destroy
    accepts_nested_attributes_for :interactive_slides_informations
    validates :name, presence: true
    validates :description, presence: true
    validates :interactive_slide_type, presence: true
  end
end
