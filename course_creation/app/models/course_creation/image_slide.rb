module CourseCreation
  # Image as a Interactive slide model
  class ImageSlide < InteractiveSlidesInformation
    mount_uploader :image, ImageUploader
  end
end
