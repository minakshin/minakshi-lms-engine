module CourseCreation
  # class EvaluationQuestion
  class EvaluationQuestion < ActiveRecord::Base
    belongs_to :version
    validates :content, presence: true

    include RankedModel

    ranks :course_order,
          with_same: :version_id
  end
end
