module CourseCreation
  # Note Model
  class Note < Resource
    validates :version_id, :content, presence: true
  end
end
