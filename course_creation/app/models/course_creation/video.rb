module CourseCreation
  # Video Model
  class Video < Resource
    include CourseContent
    mount_uploader :file, VideoUploader
    belongs_to :course_section
    belongs_to :content_section, class_name: 'CourseSection'
    validates :file, presence: true
    include CourseCreation::VersionValidator
    validate :version_published, on: :create
    before_destroy :version_editable
  end
end
