module CourseCreation
  # slide setting form
  class SlideSetting < ActiveRecord::Base
    belongs_to :slide
    mount_uploader :background_img, ImageUploader
  end
end
