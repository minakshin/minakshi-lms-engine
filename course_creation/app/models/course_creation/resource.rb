module CourseCreation
  # Resource model
  class Resource < ActiveRecord::Base
    belongs_to :version
    validates :title, presence: true,
              length: {
                maximum: 255,
                too_long: "must have no more than %{count} characters"
              }
    TYPES = { video: 'CourseCreation::Video',
              document: 'CourseCreation::Document',
              quote: 'CourseCreation::Quote',
              note: 'CourseCreation::Note'
    }
    include RankedModel
    include ResourceCollection

    ranks :chapter_order,
          with_same: :course_section_id

    ranks :version_order,
          with_same: :version_id

    def self.valid_type?(type)
      return false if type.nil?
      ResourceCollection::valid_types.include?(type)
    end

    def self.upload?(type)
      return false if type.nil?
      %w(Video Document).include?(type)
    end

    def text_resource?
      [TYPES[:quote], TYPES[:note]].include?(type)
    end
  end
end
