module CourseCreation
  # interactive slide information
  class InteractiveSlidesInformation < ActiveRecord::Base
    TYPES = %w(TextSlide ImageSlide TextImageSlide)
    INTERACTIVESLIDESINFORMATION = 'InteractiveSlidesInformation'
    MIN_TEXT_SLIDE_VALUE = 0
    MAX_TEXT_SLIDE_VALUE = 8
    TEXT_SLIDE_UI_ITERATION = 7
    belongs_to :interactive_slide
  end
end
