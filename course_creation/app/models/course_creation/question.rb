module CourseCreation
  # Course Model
  class Question < ActiveRecord::Base
    has_many :answers, -> { order(position: :asc) }, dependent: :destroy
    belongs_to :assessment
    belongs_to :question_category
    accepts_nested_attributes_for :answers, allow_destroy: true
    validates :question_category_id, presence: true
    include RankedModel
    ranks :question_order, with_same: :assessment_id
    QUESTION_CATEGOTY_TYPE = %w(sorting match_the_pairs multiple_matching)
    include CourseCreation::VersionValidator
    validate :version_published, on: :create
    before_destroy :version_editable

    def parse_option
      QUESTION_CATEGOTY_TYPE.include?(question_category.name)
    end
  end
end
