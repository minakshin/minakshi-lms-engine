module CourseCreation
  # Quote Model
  class Quote < Resource
    validates :version_id, :content, presence: true
  end
end
