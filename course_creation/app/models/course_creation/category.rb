module CourseCreation
  # Course Categories Model
  class Category < ActiveRecord::Base
    has_many :versions, dependent: :destroy
    validates :name,
              presence: { message: I18n.t(
                'course_creation.categories.blank_name_message') },
              uniqueness: { message: I18n.t(
                'course_creation.categories.duplicate_name_message') }
  end
end
