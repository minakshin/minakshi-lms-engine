module CourseCreation
  # CustomContent Model
  class CustomContent < ActiveRecord::Base
    include Custom
    include CourseContent
    # validations
    validates :name, presence: true
    mount_uploader :zip, ZipUploader
    validates :zip, presence: true
    CONTENT_TYPE = 'application/zip'
    belongs_to :course_section
    before_destroy :delete_aws_directory

    def unzip(params)
      dest = File.dirname(zip.path)
      Zip::File.open(zip.path) do |zip_file|
        zip_file.each do |f|
          f_path = File.join(dest, f.name)
          FileUtils.mkdir_p(File.dirname(f_path))
          zip_file.extract(f, f_path)
        end
      end
      check_file(dest, params)
    end

    def check_file(dest, params)
      return unless
        File.file?("#{dest}/#{params[:zip].original_filename[0..-5]}/index.html")
      evaluate_zip(self, params[:zip])
      S3_BUCKET && upload_aws(dest)
      true
    end

    def checkzip(zip)
      return true if zip.nil?
      zip.content_type == CONTENT_TYPE && passing_zip(self, zip, "#{id}")
    end

    def bucket_path
      "uploads/course_creation/custom_content/zip/#{id}"
    end

    def delete_aws_directory
      return unless S3_BUCKET
      s3 = Aws::S3::Resource.new
      bucket = s3.bucket("#{S3_BUCKET}")
      bucket.objects(prefix: bucket_path).batch_delete!
      true
    end

    def upload_aws(dest)
      S3Uploader.upload_directory(dest, S3_BUCKET,
                                  { :s3_key => Rails.application.secrets.aws_access_key_id,
                                    :s3_secret => Rails.application.secrets.aws_secret_access_key,
                                    :destination_dir => bucket_path,
                                    :threads => 4,
                                    :metadata => { 'Cache-Control' => 'max-age=315576000' }
                                  })
      FileUtils.rm_rf(dest)
    end
  end
end
