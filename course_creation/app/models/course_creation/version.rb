module CourseCreation
  # Course Version Model
  class Version < ActiveRecord::Base
    has_many :course_sections, dependent: :destroy
    has_many :evaluation_questions, dependent: :destroy
    has_many :resources, dependent: :destroy
    has_many :version_roles, dependent: :destroy
    has_many :roles, through: :version_roles
    has_many :user_courses, class_name: 'CourseTaking::UserCourse'
    belongs_to :course
    belongs_to :category
    mount_uploader :image, CourseImageUploader
    mount_uploader :video, CourseVideoUploader
    validates :description,
              presence: { message: I18n.t(
                'course_creation.courses.blank_description_message') }
    validates :expiry, presence: true
    validates :price, inclusion: [true, false]

    include ResourceCollection

    def self.list_courses(page = nil)
      includes(:course, :category)
        .order('course_creation_versions.id DESC')
        .page(page).per(10)
    end

    def self.search_courses(page = nil, keyword = nil)
      includes(:course, :category).where(
        'LOWER(title) LIKE LOWER(?)', '%' + keyword.to_s + '%'
      ).order('course_creation_versions.id DESC').references(
        :course).page(page).per(10)
    end

    def chapters
      course_sections.chapters.rank(:course_order)
    end

    def publishable?
      return false if course_sections.chapters.size.zero? ||
                      validate_evaluation_questions?
      course_sections.chapters.each do |chapter|
        return false unless chapter.contents.any? && chapter.section_present?
      end
      true
    end

    def editable?
      return true if editable && !published
      errors[:published] << I18n.t(
        'course_creation.edit_content_err_msg'
      ) if published
      errors[:purchased] << I18n.t(
        'edit_content_purchased_course'
      ) unless editable
      false
    end

    def update_roles(version_user_roles)
      version_roles.destroy_all && version_roles.create(version_user_roles)
    end

    def validate_evaluation_questions?
      evaluation_questions.each do |question|
        return false if question.active
      end
      true
    end

    def contents_count
      count = course_sections.where(
        'content = true OR is_assessment= true').count.to_i
      count > 0 ? (1.0 / count).round(3) : 0.0
    end
  end
end
