module CourseCreation
  # Certificate model
  class Certificate < ActiveRecord::Base
    has_many :courses
    validates :title, presence: true
    COURSE_COMPLETE_STATUS = 2

    # Return array of courses objects asociated with certificate
    def associated_course_versions
      courses.includes(:versions)
    end

    # Return array of courses ids asociated with certificate
    def associated_course_ids
      course_ids
    end

    def update_certificate
      update_attributes(editable: false)
    end

    def certification_taken?
      associated_course_versions.map do |course|
        return true unless course.versions.first.editable
      end
      false
    end

    def user_certification_completed?(user_id)
      return false if course_ids.nil?
      user_course_ids = CourseCreation::CourseTaking::UserCourse.where(
        version_id: course_ids, user_id: user_id,
        course_status: COURSE_COMPLETE_STATUS).count
      user_course_ids == course_ids.count
    end
  end
end
