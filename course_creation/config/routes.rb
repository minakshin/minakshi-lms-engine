CourseCreation::Engine.routes.draw do
  resources :certificates, except: [:show]
  resources :versions do
    member do
      put :publish
      get :preview
      get :syllabus, action: :show, version_view: 'Syllabus'
    end
    resources :course_sections, except: [:index, :show] do
      resources :resources do
        put :update_details
      end
      resources :videos, controller: 'resources', type: 'Video' do
        get :content, on: :new, action: :new, content: true
      end
      resources :presentations do
        resources :slides do
          member do
            put :update_settings
            put :update_ppt_title
            put :delete_bg_img
            put :update_title
            put :update_contents
            delete :destroy_column
            post :slide_clone
            put :apply_seetings
          end
        end
      end
      resources :section_assessments, controller: :assessments, type: 'section', only: [:new]
      resources :assessments
      resources :custom_contents
      resources :documents, controller: 'resources', type: 'Document' do
        get :content, on: :new, action: :new, content: true
      end
      resources :interactive_slides do
        resources :interactive_slides_informations do
          member do
            put :delete_slide_image
          end
        end
        resources :text_slides, controller:
          'interactive_slides_informations', type: 'TextSlide'
        resources :image_slides, controller:
          'interactive_slides_informations', type: 'ImageSlide'
        resources :text_image_slides, controller:
          'interactive_slides_informations', type: 'TextImageSlide'
      end
    end
    resources :evaluation_questions, except: [:show, :new]
    resources :resources
    get :documents, controller: 'resources', action: :index, type: 'Document'
    get :videos, controller: 'resources', action: :index, type: 'Video'
    resources :quotes, controller: 'resources', type: 'Quote'
    resources :notes, controller: 'resources', type: 'Note'
  end
  resources :categories
  resources :courses
  resources :assessments do
    resources :questions, except: [:show] do
      put :update_position
      resources :answers, only: [:destroy]
    end
  end
  namespace :api do
    namespace :v1 do
      resources :courses, only: [:show]
      resources :versions, only: [:show]
      resources :resources
      resources :course_sections, only: [:show]
      resources :roles, only: [] do
        member do
          get :versions
        end
      end
      resources :assessments, only: [:show]
      resources :user_courses, only: [:show] do
        resources :user_course_sections, controller: 'user_courses',
                                         action: 'sections', type: 'section'
        member do
          get :assessments
        end
        resources :user_course_assessments, only: [:show]
      end
      resources :chapters, only: [:show]
      resources :chapter_resources, controller: 'chapters',
                                    only: [:show], type: 'Resource'
      resources :user_course_assessments, only: [:create]
      resources :user_course_progresses, only: [:create, :show]
      resources :user_questions, only: [:create]
      resources :evaluation_questions, only: :create
    end
  end
  namespace :course_taking do
    resources :templates, only: :show
    get '/courses/:course_id/preview', to: 'templates#index', as: 'preview'
    get '/courses/:course_id', to: 'templates#index'
    get '/*path', to: 'templates#index'
  end
  match '*path', to: 'application#routing_error', via: :all
end
