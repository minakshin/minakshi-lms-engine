module CourseCreation
  # Engine namespace
  class Engine < ::Rails::Engine
    isolate_namespace CourseCreation
    require 'jquery-ui-rails'
    require 'ranked-model'
    require 'haml'
    require 'coffee-rails'
    require 'custom.rb'
    require 'simple_form'
    require 'js-routes'
    require 'nested_form'
    require 'tinymce-rails'
    require 'kaminari'
    require 'deep_cloneable'
    require 'underscore-rails'
    require 'remotipart'
    require 'active_model_serializers'
    require 'acts_as_list'
    config.generators do |g|
      g.test_framework :rspec, fixture: false
      g.fixture_replacement :factory_girl, dir: 'spec/factories'
      g.assets false
      g.helper false
    end
    config.before_initialize do
      config.i18n.load_path += Dir["#{config.root}/config/locales/**/*.yml"]
    end
    config.autoload_paths += %W(#{config.root}/lib/modules)
    config.assets.precompile = ['*.js', '*.css', '**/*.js', '**/*.css', '*.jpg',
                                '*.png', '*.ico', '*.gif', '*.woff2', '*.eot',
                                '*.woff', '*.ttf', '*.svg']
    config.to_prepare do
      ApplicationController.helper(CommonHelper)
    end
  end
end
