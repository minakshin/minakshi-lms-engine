module CourseCreation
  module Generators
    # install generator to copy files to host application
    class InstallGenerator < Rails::Generators::Base
      source_root "#{CourseCreation::Engine.root}/config"

      def copy_initializer
        directory "#{CourseCreation::Engine.root}/lib/generators/" \
        'course_creation/install/initializers', "#{Rails.root}/config/" \
        'initializers/course_creation'
      end

      def copy_locale
        directory 'locales', 'config/locales/course_creation'
      end

      def copy_jwplayer
        directory "#{CourseCreation::Engine.root}/lib/generators/" \
        'course_creation/install/jwplayer', "#{Rails.root}/public/jwplayer"
      end
    end
  end
end
