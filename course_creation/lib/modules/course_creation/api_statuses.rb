module CourseCreation
  # Methods for API status
  module ApiStatuses
    extend ActiveSupport::Concern
    # Course Section progress
    # 0 : User yet to start
    # 1 : User current section
    # 2 : User completed section
    STATUS = [0, 1, 2]
    def success(message = 'Success')
      { code: 200, message: message }
    end

    def record_not_found(message = 'Record not found')
      { code: 404, message: message }
    end

    def failure(message = 'Failure')
      { code: 400, message: message }
    end

    def params_missing(message = 'Missing required fields')
      { code: 402, message: message }
    end

    def foreign_key_constraint(message = 'ForeignKey constraint')
      { code: 402, message: message }
    end
  end
end
