module CourseCreation
  # Admin force pass user
  module CoursePasser
    COURSE_COMPLETE_STATUS = 2
    EVALUTION_USER_RESPONSE = 5
    USER_QUESTION_PASSING_STATUS = 1
    def force_pass_user
      version.course_sections.each do |course_section|
        content = content_type(course_section)
        CourseTaking::UserCourseProgress.update_course_progress(
          id, course_section.id, content_type(course_section),
          COURSE_COMPLETE_STATUS)
        user_course_assessment(course_section.assessment) if content.eql?(
          'Assessment')
      end
      create_user_evaluation
      complete!
    end

    private

    def create_user_evaluation
      params = { 'user_course_id' => id,
                 'evaluation_questions_attributes' => eval_questions_params
                }
      return if user_course_evaluation.present?
      CourseTaking::UserCourseEvaluation.create(params)
    end

    def eval_questions_params
      user_eval_questions = []
      version.evaluation_questions.each do |eval_question|
        user_eval_questions << { 'answer_id' => EVALUTION_USER_RESPONSE,
                                 'evaluation_question_id' => eval_question.id
                               }
      end
      user_eval_questions
    end

    def user_course_assessment(assessment)
      create_user_assessment(assessment)
      user_questions = displayed_user_questions(assessment)
      user_questions.each do |question|
        params = { 'question_id' => question.id,
                   'question_type_id' => question.question_category_id,
                   'passing_status' => USER_QUESTION_PASSING_STATUS,
                   'assessment_id' => assessment.id,
                   'user_course_id' => id,
                   'objective_answers_attributes' => answers_hash(question)
                 }
        CourseTaking::UserQuestion.create(params)
      end
    end

    def displayed_user_questions(assessment)
      if assessment.no_of_displayed_questions.present?
        assessment.questions.take(assessment.no_of_displayed_questions)
      else
        assessment.questions
      end
    end

    def create_user_assessment(assessment)
      user_assessment = CourseTaking::UserCourseAssessment.find_or_create_by(
        assessment_id: assessment.id, user_course_id: id)
      user_assessment.status = USER_QUESTION_PASSING_STATUS
      user_assessment.assessment_percentage = 100
      user_assessment.save
    end

    def answers_hash(question)
      answers = []
      question.answers.where(correct_answer: true).pluck(:id).each do |id|
        answers << { 'answer_id' => id }
      end
      answers
    end

    def content_type(section)
      return nil unless section.content
      section.name.split(':').first if section.content
    end
  end
end
