module ResourceCollection
  mattr_reader :valid_types

  @@valid_types = RESOURCE_TYPES.map { |type| type.sub('CourseCreation::', '') }

  RESOURCE_TYPES.each do |type|
    define_method(type.sub('CourseCreation::', '').downcase.pluralize) do
      self.resources.where(type: type)
    end
  end

  def resources_type(resource_type)
    self.send(resource_type.downcase.pluralize.to_sym)
  end

  def order_key(resource_type)
    case resource_type
    when 'Quote', 'Note'
      :version_order
    else
      :chapter_order
    end
  end

  def resources_order(resource_type)
    list = resources_type(resource_type)
    list.any? ? list.rank(order_key(resource_type)) : list
  end
end
