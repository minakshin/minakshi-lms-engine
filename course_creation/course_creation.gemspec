$LOAD_PATH.push File.expand_path('../lib', __FILE__)

# Maintain your gem's version:
require 'course_creation/version'

# Describe your gem and declare its dependencies:
Gem::Specification.new do |s|
  s.name        = 'course_creation'
  s.version     = CourseCreation::VERSION
  s.authors     = ['weboniselab']
  s.email       = ['lfe@weboniselab.com']
  s.homepage    = 'http://weboniselab.com'
  s.summary     = 'CourseCreation'
  s.description = 'Course Creation is Rails engine which provides course
                   creation functionalities '
  s.files = Dir['{app,config,db,lib}/**/*', 'Rakefile', 'README.rdoc']
  s.test_files = Dir['spec/**/*']

  s.add_dependency 'rails', '~> 4.2.1'
  s.add_dependency 'carrierwave', '0.10.0'
  s.add_dependency 'haml'
  s.add_dependency 'coffee-rails'
  s.add_dependency 'jquery-rails'
  s.add_dependency 'jquery-ui-rails'
  s.add_dependency 'ranked-model'
  s.add_dependency 'nested_form', '0.3.2'
  s.add_dependency 'tinymce-rails', '4.2.2'
  s.add_dependency 'simple_form', '3.1.0'
  s.add_dependency 'js-routes', '1.1.0'
  s.add_dependency 'jwplayer-rails', '1.0.1'
  s.add_dependency 'kaminari', '0.16.3'
  s.add_dependency 'deep_cloneable', '2.1.1'
  s.add_dependency 'underscore-rails', '1.8.3'
  s.add_dependency 'rubyzip', '1.1.7'
  s.add_dependency 'remotipart', '1.2'
  s.add_dependency 'active_model_serializers', '0.9.3'
  s.add_dependency 'carrierwave-video-thumbnailer', '0.1.4'
  s.add_dependency 'acts_as_list', '0.7.2'
end
