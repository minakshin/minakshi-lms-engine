Rails.application.routes.draw do
  mount CourseCreation::Engine => '/course_creation'
  root 'course_creation/versions#index'
end
