# encoding: UTF-8
# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# Note that this schema.rb definition is the authoritative source for your
# database schema. If you need to create the application database on another
# system, you should be using db:schema:load, not running all the migrations
# from scratch. The latter is a flawed and unsustainable approach (the more migrations
# you'll amass, the slower it'll run and the greater likelihood for issues).
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema.define(version: 20160205101708) do

  # These are extensions that must be enabled in order to support this database
  enable_extension "plpgsql"

  create_table "course_creation_answers", force: :cascade do |t|
    t.string   "option"
    t.string   "image"
    t.datetime "created_at",                     null: false
    t.datetime "updated_at",                     null: false
    t.boolean  "correct_answer", default: false
    t.integer  "question_id"
    t.integer  "position"
  end

  add_index "course_creation_answers", ["question_id"], name: "index_course_creation_answers_on_question_id", using: :btree

  create_table "course_creation_assessments", force: :cascade do |t|
    t.string   "name",                                    null: false
    t.text     "description"
    t.string   "assessment_type",                         null: false
    t.boolean  "passing_criteria"
    t.float    "passing_percentage",        default: 0.0
    t.integer  "no_of_displayed_questions"
    t.integer  "course_section_id"
    t.boolean  "upfront"
    t.boolean  "additional_text"
    t.boolean  "details_page"
    t.boolean  "randomize"
    t.datetime "created_at",                              null: false
    t.datetime "updated_at",                              null: false
  end

  add_index "course_creation_assessments", ["course_section_id"], name: "index_course_creation_assessments_on_course_section_id", using: :btree

  create_table "course_creation_categories", force: :cascade do |t|
    t.string   "name"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "course_creation_certificates", force: :cascade do |t|
    t.string   "title"
    t.string   "file"
    t.text     "description"
    t.datetime "created_at",                 null: false
    t.datetime "updated_at",                 null: false
    t.boolean  "editable",    default: true
  end

  create_table "course_creation_course_sections", force: :cascade do |t|
    t.integer  "parent_id"
    t.string   "name"
    t.integer  "version_id"
    t.datetime "created_at",                    null: false
    t.datetime "updated_at",                    null: false
    t.integer  "course_order"
    t.integer  "chapter_order"
    t.boolean  "content",       default: false
    t.boolean  "is_assessment", default: false
  end

  create_table "course_creation_courses", force: :cascade do |t|
    t.string   "title"
    t.datetime "created_at",     null: false
    t.datetime "updated_at",     null: false
    t.integer  "certificate_id"
  end

  add_index "course_creation_courses", ["certificate_id"], name: "index_course_creation_courses_on_certificate_id", using: :btree

  create_table "course_creation_custom_contents", force: :cascade do |t|
    t.string   "name"
    t.string   "zip"
    t.integer  "course_section_id"
    t.datetime "created_at",        null: false
    t.datetime "updated_at",        null: false
  end

  add_index "course_creation_custom_contents", ["course_section_id"], name: "index_course_creation_custom_contents_on_course_section_id", using: :btree

  create_table "course_creation_evaluation_questions", force: :cascade do |t|
    t.string   "content"
    t.boolean  "active"
    t.datetime "created_at",   null: false
    t.datetime "updated_at",   null: false
    t.integer  "version_id"
    t.integer  "course_order"
  end

  add_index "course_creation_evaluation_questions", ["version_id"], name: "index_course_creation_evaluation_questions_on_version_id", using: :btree

  create_table "course_creation_interactive_slides", force: :cascade do |t|
    t.string   "name"
    t.text     "description"
    t.integer  "version_id"
    t.integer  "course_section_id"
    t.datetime "created_at",             null: false
    t.datetime "updated_at",             null: false
    t.string   "interactive_slide_type"
  end

  add_index "course_creation_interactive_slides", ["course_section_id"], name: "index_course_creation_interactive_slides_on_course_section_id", using: :btree
  add_index "course_creation_interactive_slides", ["version_id"], name: "index_course_creation_interactive_slides_on_version_id", using: :btree

  create_table "course_creation_interactive_slides_informations", force: :cascade do |t|
    t.string   "image"
    t.string   "title"
    t.text     "description"
    t.integer  "interactive_slide_id"
    t.datetime "created_at",           null: false
    t.datetime "updated_at",           null: false
    t.string   "type",                 null: false
  end

  add_index "course_creation_interactive_slides_informations", ["interactive_slide_id"], name: "interactive_slide_id", using: :btree

  create_table "course_creation_presentations", force: :cascade do |t|
    t.string   "title"
    t.integer  "course_section_id"
    t.datetime "created_at",        null: false
    t.datetime "updated_at",        null: false
  end

  create_table "course_creation_question_categories", force: :cascade do |t|
    t.string "name"
  end

  create_table "course_creation_questions", force: :cascade do |t|
    t.text     "title"
    t.text     "additional_text"
    t.integer  "question_category_id"
    t.datetime "created_at",                null: false
    t.datetime "updated_at",                null: false
    t.integer  "test_reference_section_id"
    t.integer  "test_reference_page_id"
    t.integer  "assessment_id"
    t.integer  "question_order"
  end

  add_index "course_creation_questions", ["assessment_id"], name: "index_course_creation_questions_on_assessment_id", using: :btree
  add_index "course_creation_questions", ["question_category_id"], name: "index_course_creation_questions_on_question_category_id", using: :btree
  add_index "course_creation_questions", ["test_reference_page_id"], name: "index_course_creation_questions_on_test_reference_page_id", using: :btree
  add_index "course_creation_questions", ["test_reference_section_id"], name: "index_course_creation_questions_on_test_reference_section_id", using: :btree

  create_table "course_creation_resources", force: :cascade do |t|
    t.string   "title"
    t.string   "file"
    t.string   "type"
    t.integer  "course_section_id"
    t.datetime "created_at",         null: false
    t.datetime "updated_at",         null: false
    t.text     "content"
    t.integer  "version_id"
    t.integer  "chapter_order"
    t.string   "description"
    t.integer  "content_section_id"
    t.integer  "version_order"
  end

  add_index "course_creation_resources", ["content_section_id"], name: "index_course_creation_resources_on_content_section_id", using: :btree
  add_index "course_creation_resources", ["course_section_id"], name: "index_course_creation_resources_on_course_section_id", using: :btree
  add_index "course_creation_resources", ["version_id"], name: "index_course_creation_resources_on_version_id", using: :btree

  create_table "course_creation_roles", force: :cascade do |t|
    t.string   "name"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "course_creation_slide_contents", force: :cascade do |t|
    t.integer  "slide_id"
    t.text     "content"
    t.string   "orientation"
    t.datetime "created_at",  null: false
    t.datetime "updated_at",  null: false
    t.string   "file_url"
    t.string   "type"
  end

  add_index "course_creation_slide_contents", ["slide_id"], name: "index_course_creation_slide_contents_on_slide_id", using: :btree

  create_table "course_creation_slide_settings", force: :cascade do |t|
    t.string   "background_color", default: "#ffffff"
    t.string   "background_img"
    t.string   "transition"
    t.integer  "slide_id",                             null: false
    t.datetime "created_at",                           null: false
    t.datetime "updated_at",                           null: false
  end

  add_index "course_creation_slide_settings", ["slide_id"], name: "index_course_creation_slide_settings_on_slide_id", using: :btree

  create_table "course_creation_slides", force: :cascade do |t|
    t.string   "title"
    t.integer  "presentation_id"
    t.integer  "number_of_columns", limit: 2
    t.datetime "created_at",                  null: false
    t.datetime "updated_at",                  null: false
    t.integer  "slides_order"
  end

  add_index "course_creation_slides", ["presentation_id"], name: "index_course_creation_slides_on_presentation_id", using: :btree

  create_table "course_creation_user_course_assessments", force: :cascade do |t|
    t.integer  "user_course_id"
    t.integer  "assessment_id"
    t.integer  "status"
    t.integer  "assessment_percentage"
    t.string   "assessment_type"
    t.datetime "created_at",            null: false
    t.datetime "updated_at",            null: false
  end

  add_index "course_creation_user_course_assessments", ["user_course_id"], name: "index_course_creation_user_course_assessments_on_user_course_id", using: :btree

  create_table "course_creation_user_course_evaluation_questions", force: :cascade do |t|
    t.integer "evaluation_question_id"
    t.integer "answer_id"
    t.integer "user_course_evaluation_id"
  end

  add_index "course_creation_user_course_evaluation_questions", ["user_course_evaluation_id"], name: "evaluation_id", using: :btree

  create_table "course_creation_user_course_evaluations", force: :cascade do |t|
    t.integer "version_id"
    t.integer "user_course_id"
  end

  add_index "course_creation_user_course_evaluations", ["user_course_id"], name: "index_course_creation_user_course_evaluations_on_user_course_id", using: :btree

  create_table "course_creation_user_course_progresses", force: :cascade do |t|
    t.integer  "content_id"
    t.integer  "user_course_id"
    t.integer  "status"
    t.string   "content_type"
    t.integer  "attempt_to"
    t.datetime "created_at",     null: false
    t.datetime "updated_at",     null: false
  end

  add_index "course_creation_user_course_progresses", ["user_course_id"], name: "index_course_creation_user_course_progresses_on_user_course_id", using: :btree

  create_table "course_creation_user_courses", force: :cascade do |t|
    t.integer  "user_id"
    t.integer  "version_id"
    t.datetime "created_at",                                                  null: false
    t.datetime "updated_at",                                                  null: false
    t.integer  "course_status"
    t.string   "last_item_id"
    t.string   "last_item_type"
    t.decimal  "progress_percentage", precision: 5, scale: 2
    t.integer  "course_score"
    t.boolean  "content_complete",                            default: false
  end

  add_index "course_creation_user_courses", ["version_id"], name: "index_course_creation_user_courses_on_version_id", using: :btree

  create_table "course_creation_user_descriptives", force: :cascade do |t|
    t.text     "answer"
    t.integer  "user_question_id"
    t.integer  "question_id"
    t.integer  "user_id"
    t.boolean  "is_correct_answer"
    t.datetime "created_at",        null: false
    t.datetime "updated_at",        null: false
  end

  add_index "course_creation_user_descriptives", ["question_id"], name: "index_course_creation_user_descriptives_on_question_id", using: :btree
  add_index "course_creation_user_descriptives", ["user_question_id"], name: "index_course_creation_user_descriptives_on_user_question_id", using: :btree

  create_table "course_creation_user_objective_question_answers", force: :cascade do |t|
    t.integer  "answer_id"
    t.integer  "user_question_id"
    t.integer  "question_id"
    t.integer  "user_id"
    t.boolean  "is_correct_answer"
    t.datetime "created_at",        null: false
    t.datetime "updated_at",        null: false
  end

  create_table "course_creation_user_questions", force: :cascade do |t|
    t.integer  "assessment_id"
    t.integer  "user_course_id"
    t.integer  "question_id"
    t.integer  "passing_status"
    t.integer  "attempt_no"
    t.integer  "question_type_id"
    t.string   "assessment_type"
    t.datetime "created_at",       null: false
    t.datetime "updated_at",       null: false
  end

  add_index "course_creation_user_questions", ["assessment_id"], name: "index_course_creation_user_questions_on_assessment_id", using: :btree
  add_index "course_creation_user_questions", ["question_id"], name: "index_course_creation_user_questions_on_question_id", using: :btree
  add_index "course_creation_user_questions", ["question_type_id"], name: "index_course_creation_user_questions_on_question_type_id", using: :btree
  add_index "course_creation_user_questions", ["user_course_id"], name: "index_course_creation_user_questions_on_user_course_id", using: :btree

  create_table "course_creation_version_roles", force: :cascade do |t|
    t.integer  "version_id"
    t.integer  "role_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  add_index "course_creation_version_roles", ["role_id"], name: "index_course_creation_version_roles_on_role_id", using: :btree
  add_index "course_creation_version_roles", ["version_id"], name: "index_course_creation_version_roles_on_version_id", using: :btree

  create_table "course_creation_versions", force: :cascade do |t|
    t.string   "version",       default: "0"
    t.text     "description",                   null: false
    t.string   "image"
    t.string   "video"
    t.integer  "expiry",                        null: false
    t.boolean  "price",                         null: false
    t.string   "default_image"
    t.integer  "prerequisite"
    t.integer  "course_id"
    t.integer  "category_id"
    t.datetime "created_at",                    null: false
    t.datetime "updated_at",                    null: false
    t.boolean  "published",     default: false
    t.boolean  "editable",      default: true
  end

  add_index "course_creation_versions", ["category_id"], name: "index_course_creation_versions_on_category_id", using: :btree
  add_index "course_creation_versions", ["course_id"], name: "index_course_creation_versions_on_course_id", using: :btree

  add_foreign_key "course_creation_answers", "course_creation_questions", column: "question_id"
  add_foreign_key "course_creation_assessments", "course_creation_course_sections", column: "course_section_id"
  add_foreign_key "course_creation_custom_contents", "course_creation_course_sections", column: "course_section_id"
  add_foreign_key "course_creation_evaluation_questions", "course_creation_versions", column: "version_id"
  add_foreign_key "course_creation_interactive_slides", "course_creation_course_sections", column: "course_section_id"
  add_foreign_key "course_creation_interactive_slides", "course_creation_versions", column: "version_id"
  add_foreign_key "course_creation_interactive_slides_informations", "course_creation_interactive_slides", column: "interactive_slide_id"
  add_foreign_key "course_creation_questions", "course_creation_assessments", column: "assessment_id"
  add_foreign_key "course_creation_questions", "course_creation_question_categories", column: "question_category_id"
  add_foreign_key "course_creation_resources", "course_creation_course_sections", column: "content_section_id"
  add_foreign_key "course_creation_resources", "course_creation_course_sections", column: "course_section_id"
  add_foreign_key "course_creation_resources", "course_creation_versions", column: "version_id"
  add_foreign_key "course_creation_slide_contents", "course_creation_slides", column: "slide_id"
  add_foreign_key "course_creation_slide_settings", "course_creation_slides", column: "slide_id"
  add_foreign_key "course_creation_slides", "course_creation_presentations", column: "presentation_id"
  add_foreign_key "course_creation_user_course_assessments", "course_creation_assessments", column: "assessment_id"
  add_foreign_key "course_creation_user_course_assessments", "course_creation_user_courses", column: "user_course_id"
  add_foreign_key "course_creation_user_course_evaluation_questions", "course_creation_user_course_evaluations", column: "user_course_evaluation_id"
  add_foreign_key "course_creation_user_course_evaluations", "course_creation_user_courses", column: "user_course_id"
  add_foreign_key "course_creation_user_course_progresses", "course_creation_user_courses", column: "user_course_id"
  add_foreign_key "course_creation_user_courses", "course_creation_versions", column: "version_id"
  add_foreign_key "course_creation_user_descriptives", "course_creation_questions", column: "question_id"
  add_foreign_key "course_creation_user_descriptives", "course_creation_user_questions", column: "user_question_id"
  add_foreign_key "course_creation_user_objective_question_answers", "course_creation_questions", column: "question_id"
  add_foreign_key "course_creation_user_objective_question_answers", "course_creation_user_questions", column: "user_question_id"
  add_foreign_key "course_creation_user_questions", "course_creation_assessments", column: "assessment_id"
  add_foreign_key "course_creation_user_questions", "course_creation_question_categories", column: "question_type_id"
  add_foreign_key "course_creation_user_questions", "course_creation_questions", column: "question_id"
  add_foreign_key "course_creation_user_questions", "course_creation_user_courses", column: "user_course_id"
  add_foreign_key "course_creation_version_roles", "course_creation_roles", column: "role_id"
  add_foreign_key "course_creation_version_roles", "course_creation_versions", column: "version_id"
  add_foreign_key "course_creation_versions", "course_creation_categories", column: "category_id"
  add_foreign_key "course_creation_versions", "course_creation_courses", column: "course_id"
end
